package api.utils.gui;

import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.input.InputState;

@Deprecated
public class SimpleGUIHorizontalButtonPane extends GUIAncor {

    private GUITextButton[] buttons;
    private int spacing;

    public SimpleGUIHorizontalButtonPane(InputState state, float width, float height, int spacing) {
        super(state, width, height);
        this.spacing = spacing;
    }

    @Override
    public void update(Timer timer) {
        super.update(timer);
        System.err.println(width );

    }

    @Override
    public void draw() {
        super.draw();
        if(buttons != null) {
            float width = this.getWidth()-spacing-buttons.length;
            for (GUITextButton button : buttons) {
                button.setWidth(width);
            }
        }
    }

    public SimpleGUIHorizontalButtonPane(InputState state, float width, float height) {
        this(state, width, height, 2);
    }

    public void addButton(GUITextButton button) {
        if(buttons == null) {
            button.setWidth((this.getWidth() - (spacing * 2)));
            button.setPos(spacing, spacing, 0);
            button.setMouseUpdateEnabled(true);
            buttons = new GUITextButton[] {button};
            attach(button);
        } else {
            button.setMouseUpdateEnabled(true);
            GUITextButton[] newButtons = new GUITextButton[buttons.length + 1];
            System.arraycopy(buttons, 0, newButtons, 0, buttons.length);
            newButtons[newButtons.length - 1] = button;
            for(int i = 0; i < newButtons.length; i ++) {
                newButtons[i].setWidth((this.getWidth() / newButtons.length) - (spacing * 2));
                newButtons[i].setPos(spacing + ((newButtons[i].getWidth() + spacing) * i), spacing, 0);
            }
            this.buttons = newButtons;
        }
        attach(button);
    }
}