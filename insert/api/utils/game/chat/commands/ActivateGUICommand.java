package api.utils.game.chat.commands;

import api.ModPlayground;
import api.common.GameCommon;
import api.mod.StarMod;
import api.utils.game.PlayerUtils;
import api.utils.game.chat.CommandInterface;
import api.utils.gui.ModGUIHandler;
import org.jetbrains.annotations.Nullable;
import org.schema.game.common.data.player.PlayerState;

/**
 * ActivateGUICommand
 * <Description>
 *
 * @author TheDerpGamer
 * @since 04/14/2021
 */
public class ActivateGUICommand implements CommandInterface {

    @Override
    public String getCommand() {
        return "activate";
    }

    @Override
    public String[] getAliases() {
        return new String[] {
                "activate%SEPARATOR%gui",
                "gui%SEPARATOR%activate"
        };
    }

    @Override
    public String getDescription() {
        return "Activates the specified GUIMenu or GUIDialog for the client.\n" +
                "- %COMMAND% <menu|dialog> <menu_name> [player_name] : Activates a menu or dialog for the specified player. player_name defaults to the command's sender.";
    }

    @Override
    public boolean isAdminOnly() {
        return true;
    }

    @Override
    public boolean onCommand(PlayerState sender, String[] args) {
        if(args.length == 2 || args.length == 3) {
            PlayerState targetPlayer = (args.length == 2) ? sender : GameCommon.getPlayerFromName(args[2]);
            if(targetPlayer != null) {
                if(args[0].equalsIgnoreCase("menu")) {
                    if(ModGUIHandler.getGUIControlManager(args[1]) != null) targetPlayer.activateModControlManager(args[1]);
                    else PlayerUtils.sendMessage(sender, "[ERROR]: Control Manager for " + args[1] + " not found");
                } else if(args[0].equalsIgnoreCase("dialog")) {
                    if(ModGUIHandler.getInputDialog(args[1]) != null) targetPlayer.activateModInputDialog(args[1]);
                    else PlayerUtils.sendMessage(sender, "[ERROR]: Input Dialog for " + args[1] + " not found");
                }
            } else PlayerUtils.sendMessage(sender, "[ERROR]: " + args[2] + " is not online");
            return true;
        } else return false;
    }

    @Override
    public void serverAction(@Nullable PlayerState sender, String[] args) {

    }

    @Override
    public StarMod getMod() {
        return ModPlayground.inst;
    }
}
