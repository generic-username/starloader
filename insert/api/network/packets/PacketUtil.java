package api.network.packets;

import api.common.GameClient;
import api.common.GameServer;
import api.network.Packet;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.client.ClientProcessor;
import org.schema.schine.network.server.ServerProcessor;

public class PacketUtil {
    public static void sendPacketToServer(Packet apiPacket) {
        ClientProcessor processor = (ClientProcessor) GameClient.getClientState().getProcessor();
        processor.getModPacketQueue().add(apiPacket);
    }

    public static ServerProcessor getServerProcessor(RegisteredClientOnServer clientOnServer) {
        return clientOnServer.getProcessor();
    }
    public static ObjectArrayFIFOQueue<ImmutablePair<PlayerState, Packet>> serverPacketQueue = new ObjectArrayFIFOQueue<>();
    public static ObjectArrayFIFOQueue<Packet> clientPacketQueue = new ObjectArrayFIFOQueue<>();

    public static void sendPacket(ServerProcessor processor, Packet apiPacket) {
        processor.getModPacketQueue().add(apiPacket);
    }

    public static void sendPacket(PlayerState player, Packet apiPacket) {
        ServerProcessor processor = getServerProcessor(GameServer.getServerClient(player));
        processor.getModPacketQueue().add(apiPacket);
    }

    public static void registerPacket(Class<? extends Packet> aClass) {
        Packet.registerPacket(aClass);
    }

}
