package api.common;

import api.mod.ModStarter;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.SendableGameState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.schine.network.objects.Sendable;

import javax.annotation.Nullable;
import java.util.Locale;

public class GameCommon {
    @Nullable
    public static SendableGameState getGameState() {
        if (GameServerState.instance != null) {
            return GameServerState.instance.getGameState();
        } else if (GameClientState.instance != null) {
            return GameClientState.instance.getGameState();
        }
        //Probably in the main menu or something
        return null;
    }

    public static Sendable getGameObject(int id){
        return getGameState().getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(id);
    }
    public static boolean isOnSinglePlayer(){
        return ModStarter.lastConnectedToClient && ModStarter.lastConnectedToServer;
    }
    public static boolean isDedicatedServer(){
        return !ModStarter.lastConnectedToClient && ModStarter.lastConnectedToServer;
    }
    public static boolean isClientConnectedToServer(){
        return ModStarter.lastConnectedToClient && !ModStarter.lastConnectedToServer;
    }

    /**
     * A unique id depending on the context the mod is loaded in.
     * Single player/Dedicated Server = Universe Name
     * Client connected to server = Server IP:Port
     */
    public static String getUniqueContextId(){
        return ModStarter.lastConnected;
    }

    public static PlayerState getPlayerFromName(String pName){
        if(GameServer.getServerState() != null) {
            try {
                return GameServer.getServerState().getPlayerFromName(pName);
            } catch (PlayerNotFountException e) {
                return null;
            }
        }
        return GameClient.getClientState().getOnlinePlayersLowerCaseMap().get(pName.toLowerCase(Locale.ENGLISH));
    }

}
