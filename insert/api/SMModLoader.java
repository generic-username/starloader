package api;

import api.listener.Listener;
import api.mod.*;
import api.mod.exception.ModExceptionWindow;
import api.modloader.ProxyJarClassLoader;
import api.smd.SMDUtils;
import api.utils.StarRunnable;
import api.utils.game.chat.CommandInterface;
import org.schema.game.client.view.gui.LoadingScreenDetailed;
import org.schema.game.common.Starter;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Locale;

public class SMModLoader {
    public static final File modFolder = new File("mods");

    public static StarMod loadMod(ModSkeleton skeleton) {
        if (skeleton.isLoaded()) {
            System.err.println("[StarLoader] [SMModLoader] Did not load: " + skeleton.getDebugName() + " because it is already loaded.");
            return skeleton.getRealMod();
        }
        DebugFile.log("Loading Mod: " + skeleton.getDebugName());
        try {
            ProxyJarClassLoader loader = ProxyJarClassLoader.construct(skeleton);
            Class<?> c = loader.loadClass(skeleton.getMainClass());
            Constructor<?> constructor = c.getConstructors()[0];
            Object o = constructor.newInstance();
            DebugFile.log("Creating mod...");
            if (!(o instanceof StarMod)) {
                DebugFile.err("Failed to load mod! not instanceof StarMod.");
                throw new IllegalArgumentException("Main class must be an instance of StarMod");
            } else {
                StarMod sMod = ((StarMod) o);
                DebugFile.log("Loading mod...");
                skeleton.setLoaded(true);
                skeleton.setRealMod(sMod);
                sMod.setSkeleton(skeleton);
                skeleton.setClassLoader(loader);
                sMod.onLoad();
                DebugFile.log("Mod loaded: " + sMod.toString());

                return sMod;
            }
        } catch (InvocationTargetException e) {
            DebugFile.logError(e, null);
            DebugFile.err(" !! InvocationTargetException occurred while loading mod!!");
            DebugFile.err("This error is thrown when the main class itself throws an error while being constructed");
            DebugFile.err("===== The root cause of this error is as follows: =====");
            DebugFile.logError(e.getCause(), null);
            DebugFile.err("===== =========================================== =====");
        } catch (Exception e) {
            DebugFile.err("Error loading mod: " + skeleton.getDebugName());
            DebugFile.logError(e, null);
        }
        return null;
    }

    public static void loadModSkeletons() {
        File[] modList = modFolder.listFiles();
        if (modList == null) throw new NullPointerException("Could not list files in mods folder");
        for (File file : modList) {
            if (file.getName().endsWith(".jar")) {
                loadModSkeleton(file);
            }
        }
    }

    /**
     * Loads a ModSkeleton from a file
     */
    public static ModSkeleton loadModSkeleton(File f) {
        try {
            ModSkeleton skeleton = ModSkeleton.fromJarFile(f);
            StarLoader.starMods.add(skeleton);
            return skeleton;
        } catch (Exception e) {
            e.printStackTrace();
            boolean shouldContinue = ModExceptionWindow.display(ModPlayground.inst.getSkeleton(), e);
            if (shouldContinue) {
                return null;
            }
        }
        throw new RuntimeException();
    }

    //    public static void loadAllModsInModsFolder(){
//        DebugFile.log("Loading Mods...");
//        for (File file : modFolder.listFiles()) {
//            try {
//                JarFile jf = new JarFile(file);
//                loadMod(jf);
//                jf.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//                DebugFile.log("MOD LOAD FAILED::::");
//                DebugFile.logError(e, null);
//            }
//        }
//    }
    //Uplinking is when the client must restart to apply class transformers
    public static boolean shouldUplink = false;
    public static ArrayList<Integer> uplinkMods = new ArrayList<>();
    public static String uplinkServerHost;
    public static int uplinkServerPort;
    public static String[] uplinkArgs;
    public static boolean runningAsServer = false;

    public static void main(String[] args) throws ClassNotFoundException {
        DebugFile.clear();
        if (!modFolder.exists()) {
            modFolder.mkdir();
        }
        DebugFile.info("Starting StarLoader with args:");
        for (String arg : args) {
            DebugFile.info(arg);
        }
        DebugFile.info("=====================");
        //Fixes some errors with java 7 not connecting to modern sites properly
        SSLContext ssl = null;
        try {
            ssl = SSLContext.getInstance("TLSv1.2");
            ssl.init(null, null, new SecureRandom());
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }
        System.setProperty("https.protocols", "TLSv1.2");

        System.err.println("Loading default mod");
        StarLoader.starMods.add(ModSkeleton.getVirtualMod("StarLoader", "internal", "StarLoader components", StarLoader.version, true, new ModPlayground()));

        DebugFile.log("Starting starloader...");

        loadModSkeletons();

        DebugFile.log("Checking for mod updates...");
        ModUpdater.checkUpdateAll();
        DebugFile.log("Done.");

        //Store launch arguments incase we need to restart the game
        uplinkArgs = args;

        //If we should load mods immediately, so they can apply class transformers (-uplink or -server)
        boolean loadModsImmediately = false;

        for (int i = 0; i < args.length; i++) {
            String arg = args[i].toLowerCase(Locale.ENGLISH);
            if (arg.equals("-download")) {
                String[] modSerialized = args[i + 1].split(",");
                int resId = Integer.parseInt(args[i + 1]);
                DebugFile.log("Downloading mod: " + resId + " v" + modSerialized[1]);
                try {
                    SMDUtils.downloadMod(new ModIdentifier(resId, modSerialized[1]));
                    DebugFile.log("Done. Exiting program");
                    System.exit(0);
                } catch (NullPointerException e) {
                    DebugFile.log("Mod not found, use the resource id + version on starmade dock [-download 1234123,1.0]");
                } catch (IOException e) {
                    DebugFile.log("IOException while trying to download, heres the error:");
                    e.printStackTrace();
                }
            } else if (arg.equals("-uplink")) {
                // -uplink host.name 4242 1,2,3,44,55,66,12345
                System.err.println("Running with -uplink, should connect to server on first splash screen draw");
                shouldUplink = true;
                loadModsImmediately = true;
                uplinkServerHost = args[i + 1];
                uplinkServerPort = Integer.parseInt(args[i + 2]);
                for (String str : args[i + 3].split(",")) {
                    uplinkMods.add(Integer.parseInt(str));
                }
                System.err.println("Uplink info set successfully");
            } else if (arg.equals("-autoupdatemods")) {
                System.err.println("[SMModLoader] Checking for mod updates.");
                ArrayList<ModSkeleton> outdatedMods = new ArrayList<>();
                for (ModSkeleton starMod : StarLoader.starMods) {
                    SMDModInfo mod = SMDModData.getInstance().getModData(starMod.getSmdResourceId());
                    if (mod != null) {
                        System.err.println("[SMModLoader] Checked: " + starMod.getDebugName() + ", remote: " + mod.getLatestDownloadVersion());
                        if (!starMod.getModVersion().equals(mod.getLatestDownloadVersion())) {
                            outdatedMods.add(starMod);
                        }
                    }
                }
                System.err.println("[SMModLoader] Downloading all outdated mods...");
                for (ModSkeleton mod : outdatedMods) {
                    SMDModInfo modInfo = SMDModData.getInstance().getModData(mod.getSmdResourceId());
                    try {
                        ModIdentifier modId = new ModIdentifier(modInfo.getResourceId(), modInfo.getLatestDownloadVersion());
                        System.err.println("[SMModLoader] Downloading: " + modId);
                        ModUpdater.downloadAndLoadMod(modId, null);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    deleteMod(mod);
                }
                System.err.println("[SMModLoader] Done updating.");
            } else if (arg.equals("-server")) {
                loadModsImmediately = true;
                //When running with -server, assume we should "-uplink all_mods"
                runningAsServer = true;
                for (ModSkeleton modSkeleton : StarLoader.starMods) {
                    uplinkMods.add(modSkeleton.getSmdResourceId());
                }
            }
        }
        if (loadModsImmediately) {
            DebugFile.log("Loading -uplink/-server mods right away");
            LoadingScreenDetailed.modMainStatus = "Init Stage [ModLoad]: Loading -uplink mods";
            //If started with uplink/server, just load the mods right away so their class transformers are applied properly
            for (Integer modId : uplinkMods) {
                ModSkeleton mod = StarLoader.getModFromId(modId);
                if (mod == null) throw new IllegalArgumentException("Mod not found with -uplink: " + modId);
                if (modId <= 0) {
                    System.err.println("Not loading virtual mod: " + modId);
                } else {
                    LoadingScreenDetailed.modSecondaryStatus = "Loading: " + mod.getDebugName();
                    SMModLoader.loadMod(mod);
                }

                //Subscribe mod to transform event
                if (mod.isCoreMod()) {
                    StarLoader.registerCoreMod(mod);
                }
            }
            LoadingScreenDetailed.modMainStatus = "Done Mod Load.";
        }
        //DebugFile.log("Download blueprint data");
//        SMDEntryUtils.fetchDataOnThread();
        //Have it only do this when Browse tab is activated
        try {
            DebugFile.log("Starting StarMade...");
            Starter.main(args);
        } catch (IOException e) {
            e.printStackTrace();
            DebugFile.logError(e, null);
        }
    }

    /**
     * Disables an individual mod, this is mostly a debug tool, as we cannot be sure deeper modifications to the game are erased.
     */
    public static void unloadMod(ModSkeleton mod) {
        DebugFile.warn("Hard unloading mod: " + mod.getName());
        if (mod.isEnabled()) {
            //Only disable if not already enabled (like on the title screen)
            ModStarter.disableMod(mod);
        }
        //Clear Listeners of self:
        for (ArrayList<Listener> value : StarLoader.listeners.values()) {
            ArrayList<Listener> removeQueue = new ArrayList<>();
            for (Listener listener : value) {
                if (listener.getMod() == mod.getRealMod()) {
                    removeQueue.add(listener);
                }
            }
            for (Listener listener : removeQueue) {
                value.remove(listener);
            }
        }
        //StarRunnables
        StarRunnable.deleteAllFromMod(mod);

        //Commands
        ArrayList<CommandInterface> removeQueue = new ArrayList<>();
        for(CommandInterface cmd : StarLoader.getAllCommands()) {
            if(cmd.getMod() == mod.getRealMod()) removeQueue.add(cmd);
        }
        for(CommandInterface ch : removeQueue) StarLoader.getAllCommands().remove(ch);

        StarLoader.starMods.remove(mod);
    }

    /**
     * Unloads and deletes a mod jar
     */
    public static void deleteMod(ModSkeleton f) {
        SinglePlayerModData.getInstance().setClientEnabled(ModIdentifier.fromMod(f), false);
        SMModLoader.unloadMod(f);
        boolean good = f.getJarFile().delete();
        if (!good) {
            System.err.println("!!! WARNING !!! COULD NOT DELETE MOD");
        }
    }
}
