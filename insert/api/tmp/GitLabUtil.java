/*
MIT License

Copyright (c) 2020 JakeV

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package api.tmp;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Scanner;

public class GitLabUtil {
    // ===================== GitLabUtil Configuration ========================
    //The Project ID on gitlab
    public static final int GITLAB_PROJECT_ID = 16765877;

    //Where to access the API from
    public static final String GITLAB_COORDINATOR_URL = "https://gitlab.com/api/v4/projects/";

    //The path+name of the file to store job info in
    public static final String JOB_INFO_FILE = "gitlab_job_info.txt";

    //Where to download the jar
    public static final String FILE_DESTINATION = "starloader.jar";

    //Where the jar is inside of the zip
    public static final String JAR_IN_JOB_ARTIFACT = "build/libs/starloader.jar";

    //What branch to listen to
    public static final String RELEASE_BRANCH = "master";

    //Private key to a dummy account to work around a gitlab bug where you cannot read pipelines without an account.
    //The private key can be made public and distributed with no consequences, as it has no relation or permissions to any project.
    //Only needs to be changed if gitlab.com is not your coordinator url.
    public static final String PRIVATE_TOKEN = "gjdT7wVFvJ1ZaiJB1Xbb";
    // =======================================================================


    public static HttpURLConnection GET(String request) throws IOException {
        URL url = new URL(GITLAB_COORDINATOR_URL + GITLAB_PROJECT_ID + "/" + request);
        HttpURLConnection openConnection = (HttpURLConnection) url.openConnection();
        openConnection.setRequestMethod("GET");
        openConnection.setRequestProperty("PRIVATE-TOKEN", PRIVATE_TOKEN);

        System.out.println("RCode: " + openConnection.getResponseCode());
        System.out.println(openConnection.getResponseMessage());
        return openConnection;
    }

    public static int getCurrentInstalledJID() {
        try {
            Scanner scanner = new Scanner(new File(JOB_INFO_FILE));
            return Integer.parseInt(scanner.nextLine());
        } catch (Exception e) {
            return -1;
        }
    }

    public static JsonArray getJsonArray(String raw) {
        return new JsonParser().parse(raw).getAsJsonArray();
    }

    public static ArrayList<GitLabJob> jobs = new ArrayList<GitLabJob>();

    public static void refreshJobs() throws IOException {
        HttpURLConnection get = GET("jobs/");
        String raw = IOUtils.toString(get.getInputStream(), "UTF-8");
        System.out.println(raw);
        JsonArray resources = getJsonArray(raw);
        jobs.clear();
        for (JsonElement resource : resources) {
            JsonObject obj = resource.getAsJsonObject();
            GitLabJob job = new GitLabJob(obj);
            jobs.add(job);
        }
    }

    public static GitLabJob getLatestJob() {
        assert !jobs.isEmpty();
        for (GitLabJob job : jobs) {
            if (job.status.equals("success") && job.branch.equals(RELEASE_BRANCH)) {
                return job;
            }
        }
        throw new RuntimeException("No gitlab job found!");
    }

    public static void downloadJob(GitLabJob job, File destination) throws IOException {
        int jId = job.id;
        //jobs/:job_id/artifacts/*artifact_path
        HttpURLConnection get = GET("jobs/" + jId + "/artifacts/" + JAR_IN_JOB_ARTIFACT);
        FileUtils.copyInputStreamToFile(get.getInputStream(), destination);
        FileWriter writer = new FileWriter(JOB_INFO_FILE);
        writer.write(jId + "\n");
        writer.write(job.toString());
        writer.close();
    }

    //TEST METHOD & EXAMPLES
    public static void main(String[] args) throws IOException {
        //Must be called at the start - grabs all jobs
        refreshJobs();

        //Print out latest jobs
        for (GitLabJob job : jobs) {
            System.out.println(job.toString());
        }
        //Print out absolute latest job
        System.out.println("Latest Job:::");
        System.out.println(getLatestJob().toString());

        //Print installed job
        System.out.println(getCurrentInstalledJID());

        try {
            //If an update is available, download it
            boolean success = checkAndDownloadUpdates();
            if (success) {
                System.out.println("Downloaded job!");
            } else {
                System.out.println("Already up to date!");
            }
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }
    }

    public static boolean checkAndDownloadUpdates() throws IOException, NoSuchAlgorithmException, KeyManagementException {
        //Fixes some errors with java 7 not downloading properly
        SSLContext ssl = SSLContext.getInstance("TLSv1.2");
        ssl.init(null, null, new SecureRandom());
        System.setProperty("https.protocols", "TLSv1.2");
        System.err.println("Looking for GitLab jobs...");
        refreshJobs();
        GitLabJob latestJob = getLatestJob();
        int installedJob = getCurrentInstalledJID();
        System.err.println("Latest GitLab job: " + latestJob.toString());
        System.err.println("Installed JID: " + installedJob);
        if (installedJob == -1 || latestJob.id != installedJob) {
            downloadJob(latestJob, new File(FILE_DESTINATION));
            return true;
        }
        return false;
    }
}

class GitLabJob {
    int id;
    String status;
    String shortId;
    String timeStamp;
    String author;
    String commitMessage;
    String web_url;
    String branch;

    public GitLabJob(JsonObject json) {
        this.id = json.get("id").getAsInt();
        this.status = json.get("status").getAsString();
        branch = json.get("ref").getAsString();
        JsonObject cInfo = json.get("commit").getAsJsonObject();
        shortId = cInfo.get("short_id").getAsString();
        timeStamp = cInfo.get("created_at").getAsString();
        author = cInfo.get("author_name").getAsString();
        commitMessage = cInfo.get("title").getAsString();
        web_url = cInfo.get("web_url").getAsString();

    }

    @Override
    public String toString() {
        return "GitLabJob{" +
                "\nid=" + id +
                "\n, status='" + status + '\'' +
                "\n, shortId='" + shortId + '\'' +
                "\n, timeStamp='" + timeStamp + '\'' +
                "\n, author='" + author + '\'' +
                "\n, commitMessage='" + commitMessage + '\'' +
                "\n, web_url='" + web_url + '\'' +
                "\n, branch='" + branch + '\'' +
                "\n}";
    }
}