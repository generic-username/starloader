package api.listener.events.calculate;

import api.listener.events.Event;
import org.schema.game.common.controller.damage.effects.InterEffectHandler;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;

public class ApplyAddEffectConfigEvent extends Event {

    private ConfigEntityManager cfm;
    private StatusEffectType statusType;
    private InterEffectHandler.InterEffectType effectType;
    private float result;
    private float[] strength;

    public ApplyAddEffectConfigEvent() {

    }

    public void setParams(ConfigEntityManager cfm, StatusEffectType statusType, InterEffectHandler.InterEffectType effectType, float result, float[] strength) {
        this.cfm = cfm;
        this.statusType = statusType;
        this.effectType = effectType;
        this.result = result;
        this.strength = strength;
    }

    public ConfigEntityManager getConfigEntityManager() {
        return cfm;
    }

    public StatusEffectType getStatusEffectType() {
        return statusType;
    }

    public InterEffectHandler.InterEffectType getEffectType() {
        return effectType;
    }

    public float getResultValue() {
        return result;
    }

    public void setResultValue(float result) {
        this.result = result;
    }

    public float[] getInitialEffectStrength() {
        return strength;
    }
}
