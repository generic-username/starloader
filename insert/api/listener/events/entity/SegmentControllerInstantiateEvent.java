package api.listener.events.entity;

import api.listener.events.Event;
import org.schema.game.common.controller.SegmentController;

/**
 * Event fires when the constructor of SegmentController.class is called.
 * Use it for detecting new created SegmentControllers: Shipcreation, loading in ships etc.
 * !! might fire for docked segmentcontrollers as well !!
 *
 * class code is directly stolen from sc.spawnevent
 * @author IR0NSIGHT
 */
public class SegmentControllerInstantiateEvent extends Event {
    private final SegmentController controller;
    private final long timeCreated;
    /**
     * constructor
     * @param entity SegmentController
     * @param timeCreated systemtime (millis) at call
     */
    public SegmentControllerInstantiateEvent(/*Vector3i sector, */SegmentController entity, long timeCreated) {
        controller = entity;
        this.timeCreated = timeCreated;
    }

    /**
     * get reference to segmentcontroller
     * @return SegmentController that was created
     */
    public SegmentController getController() {
        return controller;
    }

    /**
     * Get the time the constructor for segmentcontroller was called. Usually time difference between creation and event is 1 tick, lag can (in theory) change that because the event is suspended until the segmentcontroller is fully created.
     * @return System.Currentmillis at time of creation
     */
    public long getTimeCreated() {
        return timeCreated;
    }

}



