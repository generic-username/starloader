package api.modloader;

import api.mod.ModSkeleton;
import api.mod.StarLoader;

import java.lang.instrument.ClassFileTransformer;
import java.security.ProtectionDomain;
import java.util.HashMap;


/**
 * Created by Jake on 10/8/2020.
 * Our main class transformer
 *
 * Dont import anything from the main starmade jar in here.
 */
public class StarClassTransformer implements ClassFileTransformer {

    @Override
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] byteCode) {
        //Mark class as loaded (Currently unused, in the future it may be used to determine if a core mod can be loaded without game restart.
        String classFilePath = className + ".class";
        HashMap<String, byte[]> starLoaderClasses = StarAgent.getStarLoaderClasses();
        byte[] classBytes = starLoaderClasses.get(classFilePath);
        //If a starloader class is available (inside the starloader.jar), then use it instead of the vanilla class
        if (classBytes != null) {
            if(!ProxyJarClassLoader.modForcedLoadedClasses.contains(classFilePath)){
                System.err.println("> Loaded StarLoader Class: " + classFilePath + "[" + byteCode.length + "/" + classBytes.length + "]");
                byteCode = classBytes;
            }else{
                System.err.println("> Not loading StarLoader class: " + classFilePath);
            }
        }

        for (ModSkeleton coreMod : StarLoader.getCoreMods()) {
            byteCode = coreMod.getRealMod().onClassTransform(loader, className, classBeingRedefined, protectionDomain, byteCode);
        }


//        byteCode = AccessTransformerHandler.handleTransform(loader, className, byteCode);
        return byteCode;
    }

}