//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.schema.game.server.data.blueprint;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.controller.EntityAlreadyExistsException;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.BlueprintInterface;
import org.schema.game.server.data.EntityRequest;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.game.server.data.blueprintnw.BlueprintType;
import org.schema.game.server.data.simulation.npc.geo.NPCEntityContingent.NPCEntitySpecification;
import org.schema.game.server.data.simulation.npc.geo.NPCSystem;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.resource.tag.Tag;

import javax.vecmath.Vector3f;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

@Deprecated
public abstract class SegmentControllerOutline<E extends SegmentController> {
    public String uniqueIdentifier;
    public String realName;
    public BlueprintInterface en;
    public Vector3i min;
    public Vector3i max;
    public Vector3i spawnSectorId;
    public String playerUID;
    public String dockTo;
    public BluePrintController removeAfterSpawn;
    public boolean scrap;
    public boolean shop;
    protected GameServerState state;
    protected float[] mat;
    protected ArrayList<SegmentControllerOutline<?>> childs;
    protected boolean activeAI;
    protected Tag railTag;
    protected int factionId;
    private ChildStats stats;
    public String blueprintFolder;
    public String blueprintUID;
    public NPCSystem npcSystem;
    public NPCEntitySpecification npcSpec;
    public boolean tradeNode;
    public ElementCountMap itemsToSpawnWith;
    public long parentDbId = -1L;
    public long rootDb = -1L;
    public boolean checkProspectedBlockCount;
    public SegmentController parent;
    public SegmentControllerOutline<?> parentOutline;
    public SegmentPiece railToSpawnOn;

    public static final ConcurrentLinkedQueue<SegmentControllerOutline> shipUidsToConvert = new ConcurrentLinkedQueue<>();
    public SegmentControllerOutline(GameServerState var1, BlueprintInterface var2, String var3, String var4, float[] var5, Vector3i var6, Vector3i var7, String var8, boolean var9, Vector3i var10, ChildStats var11) {
        this.en = var2;
        this.activeAI = var9;
        this.state = var1;
        this.uniqueIdentifier = var3;
        this.realName = var4;
        this.mat = var5;
        this.min = var6;
        this.max = var7;
        this.spawnSectorId = var10;
        this.stats = var11;
        if (var8.startsWith("ENTITY_PLAYERSTATE_")) {
            var8 = var8.substring(19);
        }
        //INSERTED CODE
        //Queue this ship to be converted on load
        if(this.en instanceof BlueprintEntry) {
            //Only check new blueprints, not SegmentControllerBluePrintEntryOld
            shipUidsToConvert.add(this);
        }
        ///

        this.playerUID = var8;
    }

    private String getChildName(ChildStats var1, String var2) {
        var2 = this.realName;
        return var2 + var1.childCounter++;
    }

    public void checkForChilds(int var1) {
        if (this.en instanceof BlueprintEntry) {
            BlueprintEntry var2;
            if ((var2 = (BlueprintEntry)this.en).rootRead) {
                this.stats.rootName = this.realName;
            }

            if (var2.getChilds() != null) {
                this.childs = new ArrayList();
                Iterator var3 = var2.getChilds().iterator();

                while(var3.hasNext()) {
                    BlueprintEntry var4;
                    if ((var4 = (BlueprintEntry)var3.next()).getEntityType() == BlueprintType.SPACE_STATION && (Boolean)ServerConfig.OVERRIDE_INVALID_BLUEPRINT_TYPE.getCurrentState()) {
                        var4.setEntityType(BlueprintType.SHIP);
                    }

                    Transform var5;
                    String var6;
                    SegmentControllerOutline var13;
                    if (var4.railDock) {
                        (var5 = new Transform()).setFromOpenGLMatrix(this.mat);
                        var6 = this.getChildName(this.stats, "r");

                        try {
                            var13 = var2.getBbController().loadBluePrint(this.state, var2.metaVersionRead >= 2 ? this.stats.rootName : var6, var5, -1, var1, var4, this.spawnSectorId, (List)null, this.playerUID, PlayerState.buffer, this.activeAI, (SegmentPiece)null, this.stats);

                            assert var4.railTag != null;

                            var13.railTag = var4.railTag;
                            this.childs.add(var13);
                        } catch (EntityNotFountException var10) {
                            var10.printStackTrace();
                        } catch (IOException var11) {
                            var11.printStackTrace();
                        } catch (EntityAlreadyExistsException var12) {
                            var12.printStackTrace();
                        }
                    } else {
                        (var5 = new Transform()).setFromOpenGLMatrix(this.mat);
                        Vector3f var10000 = var5.origin;
                        var10000.x += (float)(var4.getDockingPos().x - 16);
                        var10000 = var5.origin;
                        var10000.y += (float)(var4.getDockingPos().y - 16);
                        var10000 = var5.origin;
                        var10000.z += (float)(var4.getDockingPos().z - 16);
                        if (var4.getDockingStyle() == 7) {
                            var6 = this.getChildName(this.stats, "turret");
                        } else {
                            var6 = this.getChildName(this.stats, "dock");
                        }

                        try {
                            var13 = var2.getBbController().loadBluePrint(this.state, var6, var5, -1, var1, var4, this.spawnSectorId, (List)null, this.playerUID, PlayerState.buffer, this.activeAI, (SegmentPiece)null, this.stats);
                            this.childs.add(var13);
                        } catch (EntityNotFountException var7) {
                            var7.printStackTrace();
                        } catch (IOException var8) {
                            var8.printStackTrace();
                        } catch (EntityAlreadyExistsException var9) {
                            var9.printStackTrace();
                        }
                    }
                }
            }
        }

    }

    public abstract E spawn(Vector3i var1, boolean var2, ChildStats var3, SegmentControllerSpawnCallback var4) throws EntityAlreadyExistsException, StateParameterNotFoundException;

    public abstract long spawnInDatabase(Vector3i var1, GameServerState var2, int var3, ObjectArrayList<String> var4, ChildStats var5, boolean var6) throws SQLException, EntityAlreadyExistsException, StateParameterNotFoundException, IOException;

    public void checkOkName() throws EntityAlreadyExistsException {
        EntityRequest.existsIdentifier(this.state, this.uniqueIdentifier);
        if (this.childs != null) {
            Iterator var1 = this.childs.iterator();

            while(var1.hasNext()) {
                ((SegmentControllerOutline)var1.next()).checkOkName();
            }
        }

    }

    public int getFactionId() {
        return this.factionId;
    }

    public boolean hasOldDocking() {
        return ((BlueprintEntry)this.en).hasOldDocking();
    }
}
