//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.schema.game.server.data;

import api.StarLoaderHooks;
import api.listener.events.Event;
import api.listener.events.world.ServerSendableAddEvent;
import api.listener.events.world.ServerSendableRemoveEvent;
import api.mod.StarLoader;
import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import org.lwjgl.BufferUtils;
import org.schema.common.LogUtil;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.data.SegmentManagerInterface;
import org.schema.game.common.PercentCallbackInterface;
import org.schema.game.common.Starter;
import org.schema.game.common.ThreadedSegmentWriter;
import org.schema.game.common.api.NewSessionCallback;
import org.schema.game.common.api.OldSessionCallback;
import org.schema.game.common.controller.*;
import org.schema.game.common.controller.activities.RaceManager;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.controller.database.DatabaseIndex;
import org.schema.game.common.controller.elements.scanner.ScannerCollectionManager;
import org.schema.game.common.controller.elements.scanner.ScannerElementManager;
import org.schema.game.common.controller.gamemodes.GameModes;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.ScanData;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.SendableGameState;
import org.schema.game.common.data.blockeffects.config.ConfigPool;
import org.schema.game.common.data.blockeffects.config.ConfigPoolProvider;
import org.schema.game.common.data.chat.AllChannel;
import org.schema.game.common.data.chat.ChannelRouter;
import org.schema.game.common.data.chat.ChatChannel;
import org.schema.game.common.data.element.ControlElementMapOptimizer;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.explosion.ExplosionRunnable;
import org.schema.game.common.data.fleet.FleetManager;
import org.schema.game.common.data.fleet.FleetStateInterface;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.catalog.CatalogManager;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.playermessage.ServerPlayerMessager;
import org.schema.game.common.data.player.tech.Technology;
import org.schema.game.common.data.world.*;
import org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType;
import org.schema.game.common.data.world.Universe.SystemOwnershipType;
import org.schema.game.common.data.world.migration.Chunk32Migration;
import org.schema.game.common.gui.PreparingFilesJFrame;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.common.util.FolderZipper;
import org.schema.game.common.util.FolderZipper.ZipCallback;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.game.common.version.Version;
import org.schema.game.network.objects.ChatMessage;
import org.schema.game.network.objects.ChatMessage.ChatMessageType;
import org.schema.game.network.objects.remote.RemoteScanData;
import org.schema.game.server.controller.*;
import org.schema.game.server.controller.gameConfig.GameConfig;
import org.schema.game.server.controller.pathfinding.AbstractPathFindingHandler;
import org.schema.game.server.controller.world.factory.regions.UsableRegion;
import org.schema.game.server.controller.world.factory.regions.hooks.RegionHook;
import org.schema.game.server.data.admin.AdminCommandQueueElement;
import org.schema.game.server.data.admin.AdminCommands;
import org.schema.game.server.data.blueprint.ChildStats;
import org.schema.game.server.data.blueprint.SegmentControllerOutline;
import org.schema.game.server.data.simulation.SimulationManager;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.game.server.data.simulation.npc.NPCFactionPresetManager;
import org.schema.schine.auth.SessionCallback;
import org.schema.schine.common.DebugTimer;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.LoadingScreen;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.network.*;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.AdminLocalClient;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.network.server.ServerState;
import org.schema.schine.resource.DiskWritable;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.ResourceMap;
import org.schema.schine.resource.UniqueInterface;
import org.w3c.dom.Document;

import javax.vecmath.Vector3f;
import java.io.*;
import java.nio.ByteBuffer;
import java.sql.SQLException;
import java.util.*;

@Deprecated
public class GameServerState extends ServerState implements GameStateInterface, SegmentManagerInterface, RaceManagerState, MetaObjectState, ConfigPoolProvider, FleetStateInterface, GravityStateInterface, CatalogState, FactionState {
    public static final ByteArrayOutputStream SEGMENT_BYTE_ARRAY_BUFFER = new ByteArrayOutputStream(102400);
    public static final int NT_SEGMENT_OCTREE_COMPRESSION_RATE = 8192;
    public static String SERVER_DATABASE;
    public static String DATABASE_PATH;
    public static String ENTITY_DATABASE_PATH;
    public static String ENTITY_BLUEPRINT_PATH;
    public static String ENTITY_TEMP_BLUEPRINT_PATH;
    public static String ENTITY_BLUEPRINT_PATH_DEFAULT;
    public static String SEGMENT_DATA_BLUEPRINT_PATH;
    public static String SEGMENT_DATA_TEMP_BLUEPRINT_PATH;
    public static String SEGMENT_DATA_BLUEPRINT_PATH_DEFAULT;
    public static String ENTITY_BLUEPRINT_PATH_STATIONS_NEUTRAL;
    public static String SEGMENT_DATA_BLUEPRINT_PATH_STATIONS_NEUTRAL;
    public static String ENTITY_BLUEPRINT_PATH_STATIONS_PIRATE;
    public static String SEGMENT_DATA_BLUEPRINT_PATH_STATIONS_PIRATE;
    public static String ENTITY_BLUEPRINT_PATH_STATIONS_TRADING_GUILD;
    public static String SEGMENT_DATA_BLUEPRINT_PATH_STATIONS_TRADING_GUILD;
    public static String SEGMENT_DATA_DATABASE_PATH;
    public static int axisSweepsInMemory;
    public static Long dataReceived;
    public static final int DATABASE_VERSION = 2;
    public static int allocatedSegmentData;
    public static int lastAllocatedSegmentData;
    public static int lastFreeSegmentData;
    public static boolean updateAllShopPricesFlag;
    public static int collectionUpdates;
    public static float dayTime;
    public static int itemIds;
    public static int debugObj;
    public static GameServerState instance;
    public static int totalSectorCountTmp;
    public static int activeSectorCountTmp;
    public static int totalSectorCount;
    public static int activeSectorCount;
    public static long lastBCMessage;
    public static int segmentRequestQueue;
    public static int buffersInUse;
    public static int totalDockingChecks;
    private static int DB_VERSION_READ;
    public final IntOpenHashSet activeSectors = new IntOpenHashSet();
    public static final HashMap<String, Object> fileLocks;
    public final ArrayList<ShipSpawnWave> waves = new ArrayList();
    public final ObjectArrayFIFOQueue<AbstractPathFindingHandler<?, ?>> pathFindingCallbacks = new ObjectArrayFIFOQueue();
    public final ObjectArrayFIFOQueue<CreatureSpawn> creatureSpawns = new ObjectArrayFIFOQueue();
    private final Short2ObjectOpenHashMap<Technology> techs = new Short2ObjectOpenHashMap();
    private final MetaObjectManager metaObjectManager;
    private final ServerPlayerMessager serverPlayerMessager;
    private final AdminLocalClient adminLocalClient = new AdminLocalClient();
    private final ObjectArrayList<SimpleTransformableSendableObject> currentGravitySources = new ObjectArrayList();
    private final Map<String, Admin> admins = new Object2ObjectOpenHashMap();
    private final HashMap<String, ProtectedUplinkName> protectedNames = new HashMap();
    private final Universe universe;
    private final List<GameServerState.FileRequest> activeFileRequests = new ObjectArrayList();
    private final List<GameServerState.FileRequest> fileRequests = new ObjectArrayList();
    private final List<EntityRequest> entityRequests = new ObjectArrayList();
    private final List<String> chatLog = new ObjectArrayList();
    private final List<SectorSwitch> sectorSwitches = new ObjectArrayList();
    private final ObjectArrayFIFOQueue<ServerSegmentRequest> segmentRequests = new ObjectArrayFIFOQueue();
    private final ObjectArrayFIFOQueue<ServerSegmentRequest> segmentRequestsLoaded = new ObjectArrayFIFOQueue();
    private final GameConfig gameConfig;
    private final Long2ObjectOpenHashMap<String> playerDbIdToUID = new Long2ObjectOpenHashMap();
    private final SimulationManager simulationManager;
    private final ThreadedSegmentWriter threadedSegmentWriter = new ThreadedSegmentWriter("SERVER");
    private final ChannelRouter channelRouter;
    private final ObjectArrayFIFOQueue<ServerExecutionJob> serverExecutionJobs = new ObjectArrayFIFOQueue();
    private final Map<String, SegmentController> segmentControllersByName = new Object2ObjectOpenHashMap();
    private final Map<String, SegmentController> segmentControllersByNameLowerCase = new Object2ObjectOpenHashMap();
    private final Map<String, PlayerState> playerStatesByName = new Object2ObjectOpenHashMap();
    private final Map<String, PlayerState> playerStatesByNameLowerCase = new Object2ObjectOpenHashMap();
    private final Map<Integer, PlayerState> playerStatesByClientId = new Object2ObjectOpenHashMap();
    private final List<SegmentControllerOutline> bluePrintsToSpawn = new ObjectArrayList();
    private final PlayerAccountEntrySet blackListedIps = new PlayerAccountEntrySet();
    private final PlayerAccountEntrySet whiteListedIps = new PlayerAccountEntrySet();
    private final PlayerAccountEntrySet blackListedNames = new PlayerAccountEntrySet();
    private final PlayerAccountEntrySet blackListedAccounts = new PlayerAccountEntrySet();
    private final PlayerAccountEntrySet whiteListedNames = new PlayerAccountEntrySet();
    private final PlayerAccountEntrySet whiteListedAccounts = new PlayerAccountEntrySet();
    private final ObjectArrayFIFOQueue<ExplosionRunnable> explosionOrdersFinished = new ObjectArrayFIFOQueue();
    private final ObjectArrayList<ExplosionRunnable> explosionOrdersQueued = new ObjectArrayList();
    private final Set<PlayerState> spawnRequests = new ObjectOpenHashSet();
    private final Set<PlayerState> spawnRequestsReady = new ObjectOpenHashSet();
    private final List<Sendable> scheduledUpdates = new ObjectArrayList();
    private final DatabaseIndex databaseIndex;
    private final GameServerState.MobSpawnThread mobSpawnThread;
    private final GameMapProvider gameMapProvider;
    private final ObjectArrayList<RegionHook<?>> creatorHooks = new ObjectArrayList();
    private final ObjectArrayFIFOQueue<ByteBuffer> bufferPool = new ObjectArrayFIFOQueue(5);
    public ObjectArrayFIFOQueue<Vector3i> toLoadSectorsQueue;
    public String[] logbookEntries;
    public long delayAutosave;
    public long udpateTime;
    byte[] buffer = new byte[1048576];
    private Document blockBehaviorConfig;
    private ChatSystem chat;
    private SegmentDataManager segmentDataManager;
    private GameModes gameMode;
    private ArrayList<AdminCommandQueueElement> adminCommands = new ArrayList();
    private HashSet<Sendable> flaggedAddedObjects = new HashSet();
    private HashSet<Sendable> flaggedRemovedObjects = new HashSet();
    private HashSet<Sendable> needsNotifyObjects = new HashSet();
    private SendableGameState gameState;
    private long serverStartTime;
    private long serverTimeMod;
    private long timedShutdownStart;
    private int timedShutdownSeconds;
    private long timedMessageStart;
    private int timedMessageSeconds;
    private String timedMessage;
    private boolean factionReinstitudeFlag;
    private String configCheckSum;
    private String factionConfigCheckSum;
    private String configPropertiesCheckSum;
    private byte[] blockConfigFile;
    private byte[] factionConfigFile;
    private byte[] blockPropertiesFile;
    private ResourceMap resourceMap;
    private byte[] blockBehaviorBytes;
    private String blockBehaviorChecksum;
    private String customTexturesChecksum;
    private byte[] customTextureFile;
    private boolean synched;
    private ControlElementMapOptimizer controlOptimizer = new ControlElementMapOptimizer();
    private final FleetManager fleetManager;
    private int bkmaxFile;
    private int bkfile;
    private final Long2ObjectOpenHashMap<PlayerState> playerStatesByDbId = new Long2ObjectOpenHashMap();
    public final DebugServerController debugController;
    private Exception currentE;
    private final DebugTimer debugTimer = new DebugTimer();

    public GameServerState() throws SQLException {
        FileExt var1;
        if (!(var1 = new FileExt(DATABASE_PATH + "version")).exists()) {
            Galaxy.USE_GALAXY = false;
        } else {
            try {
                BufferedReader var2;
                DB_VERSION_READ = Integer.parseInt((var2 = new BufferedReader(new FileReader(var1))).readLine());
                var2.close();
            } catch (Exception var10) {
                var10.printStackTrace();
            }
        }

        if (DB_VERSION_READ > 0) {
            Galaxy.USE_GALAXY = true;
        }

        for(int var12 = 0; var12 < 10; ++var12) {
            ByteBuffer var3 = BufferUtils.createByteBuffer(1048576);
            this.bufferPool.enqueue(var3);
        }

        if (DB_VERSION_READ == 1 || this.isOldDb()) {
            try {
                this.doChunk32Migration();
            } catch (IOException var9) {
                var9.printStackTrace();
                throw new SQLException(var9);
            }
        }

        if (DB_VERSION_READ > 0 && DB_VERSION_READ < 2) {
            try {
                var1.delete();
                BufferedWriter var13 = new BufferedWriter(new FileWriter(var1));
                Galaxy.USE_GALAXY = true;
                var13.append("2");
                var13.close();
            } catch (IOException var8) {
                var8.printStackTrace();
            }
        }

        this.serverStartTime = System.currentTimeMillis();
        LogUtil.log().fine("STARMADE SERVER VERSION: " + Version.VERSION + "; Build(" + Version.build + ")");
        LogUtil.log().fine("STARMADE SERVER STARTED: " + new Date(System.currentTimeMillis()));
        ServerConfig.read();

        try {
            ServerConfig.write();
        } catch (IOException var7) {
            var7.printStackTrace();
        }

        File var14 = new File(ENTITY_DATABASE_PATH + "lockPWS");
        if (ServerConfig.ALLOW_OLD_POWER_SYSTEM.isOn()) {
            if (var14.exists()) {
                System.err.println("[SERVER] forcing disallow of old power system as it was set once already");
                ServerConfig.ALLOW_OLD_POWER_SYSTEM.setCurrentState(false);
            }
        } else if (!var14.exists()) {
            try {
                var14.createNewFile();
            } catch (IOException var6) {
                var6.printStackTrace();
            }
        }

        this.gameConfig = new GameConfig(this);

        try {
            this.gameConfig.parse();
        } catch (Exception var5) {
            var5.printStackTrace();
            throw new RuntimeException(var5);
        }

        this.debugController = new DebugServerController(this);
        this.mobSpawnThread = new GameServerState.MobSpawnThread();
        this.getMobSpawnThread().setPriority(3);
        this.getMobSpawnThread().start();
        this.gameMode = GameModes.SANDBOX;
        this.segmentDataManager = new SegmentDataManager(this);
        this.gameMapProvider = new GameMapProvider(this);
        this.universe = new Universe(this);
        boolean var11 = DatabaseIndex.existsDB();
        if (ENTITY_DATABASE_PATH != null) {
            this.databaseIndex = new DatabaseIndex();
            if (!var11) {
                this.databaseIndex.createDatabase();
            } else {
                this.databaseIndex.getTableManager().migrate();

                try {
                    this.databaseIndex.migrateFleets((PercentCallbackInterface)null);
                    this.databaseIndex.migrateAddFields();
                    this.databaseIndex.migrateSectorsAndSystems((PercentCallbackInterface)null);
                    this.databaseIndex.getTableManager().getFTLTable().migrateFTL((PercentCallbackInterface)null);
                    this.databaseIndex.migrateMessageSystem((PercentCallbackInterface)null);
                    this.databaseIndex.migrateTrade((PercentCallbackInterface)null);
                    this.databaseIndex.migrateEffects((PercentCallbackInterface)null);
                    this.databaseIndex.migrateVisibilityAndPlayers((PercentCallbackInterface)null);
                    this.databaseIndex.migrateNPCFactionStats();
                } catch (IOException var4) {
                    var4.printStackTrace();
                }

                this.databaseIndex.migrateAddSectorAsteroidsTouched();
                this.databaseIndex.migrateUIDFieldSize();
                this.databaseIndex.getTableManager().getSystemTable().migrateSystemsResourcesField();
            }

            NPCFactionPresetManager.importPresets(ENTITY_DATABASE_PATH);
            this.universe.getGalaxyManager().initializeOnServer();
        } else {
            System.err.println("NO DATABASE CONNECTION");
            this.databaseIndex = null;
        }

        this.simulationManager = new SimulationManager(this);
        this.metaObjectManager = new MetaObjectManager(this);
        this.serverPlayerMessager = new ServerPlayerMessager(this);
        this.channelRouter = new ChannelRouter(this);
        this.fleetManager = new FleetManager(this);
        instance = this;
    }

    private boolean isOldDb() {
        if (SEGMENT_DATA_DATABASE_PATH == null) {
            return false;
        } else {
            FileExt var1;
            if (DB_VERSION_READ == 0 && (var1 = new FileExt(SEGMENT_DATA_DATABASE_PATH)).exists()) {
                String[] var2;
                if ((var2 = var1.list()).length > 0 && var2[0].endsWith(".smd2")) {
                    System.err.println("[SERVER] Old chunk16 database detected. MIGRATION NEEDED");
                    return true;
                }

                if (var2.length > 1 && var2[1].endsWith(".smd2")) {
                    System.err.println("[SERVER] Old chunk16 database detected. MIGRATION NEEDED");
                    return true;
                }

                if (var2.length > 2 && var2[2].endsWith(".smd2")) {
                    System.err.println("[SERVER] Old chunk16 database detected. MIGRATION NEEDED");
                    return true;
                }

                if (var2.length > 0 && var2[var2.length - 1].endsWith(".smd2")) {
                    System.err.println("[SERVER] Old chunk16 database detected. MIGRATION NEEDED");
                    return true;
                }
            }

            return false;
        }
    }

    private void doChunk32Migration() throws IOException {
        System.err.println("[SERVER] DATABASE VERSION < 2. SERVER DATABASE DATA IS NOW MIGRATING TO CHUNK32");
        ZipCallback var1;
        if (ServerConfig.BACKUP_WORLD_ON_MIGRATION.isOn()) {
            this.bkmaxFile = FileUtil.countFilesRecusrively(DATABASE_PATH);
            var1 = new ZipCallback() {
                public void update(File var1) {
                    LoadingScreen.serverMessage = StringTools.format(Lng.ORG_SCHEMA_GAME_SERVER_DATA_GAMESERVERSTATE_2, new Object[]{GameServerState.this.bkfile, GameServerState.this.bkmaxFile});
                    GameServerState.this.bkfile++;
                }
            };
            FileExt var2;
            FolderZipper.zipFolder((var2 = new FileExt(DATABASE_PATH)).getAbsolutePath(), "CHUNK16BACKUP_" + var2.getName() + ".zip", "backup-StarMade-", var1, "", (FileFilter)null, true);
        }

        this.bkmaxFile = FileUtil.countFilesRecusrively(SEGMENT_DATA_DATABASE_PATH);
        this.bkfile = 0;
        var1 = new ZipCallback() {
            public void update(File var1) {
                LoadingScreen.serverMessage = StringTools.format(Lng.ORG_SCHEMA_GAME_SERVER_DATA_GAMESERVERSTATE_3, new Object[]{GameServerState.this.bkfile, GameServerState.this.bkmaxFile});
                GameServerState.this.bkfile++;
            }
        };
        Chunk32Migration.processFolder(SEGMENT_DATA_DATABASE_PATH, true, var1, false);
    }

    public static void initPaths(boolean var0, int var1) {
        ++var1;
        ENTITY_DATABASE_PATH = DATABASE_PATH;
        ENTITY_BLUEPRINT_PATH = "." + File.separator + "blueprints" + File.separator;
        ENTITY_TEMP_BLUEPRINT_PATH = "." + File.separator + "tmpBB" + File.separator;
        ENTITY_BLUEPRINT_PATH_DEFAULT = "." + File.separator + "blueprints-default" + File.separator;
        SEGMENT_DATA_BLUEPRINT_PATH = "." + File.separator + "blueprints" + File.separator + "DATA" + File.separator;
        SEGMENT_DATA_TEMP_BLUEPRINT_PATH = "." + File.separator + "tmpBB" + File.separator + "DATA" + File.separator;
        SEGMENT_DATA_BLUEPRINT_PATH_DEFAULT = "." + File.separator + "blueprints-default" + File.separator + "DATA" + File.separator;
        ENTITY_BLUEPRINT_PATH_STATIONS_NEUTRAL = "." + File.separator + "blueprints-stations" + File.separator + "neutral" + File.separator;
        SEGMENT_DATA_BLUEPRINT_PATH_STATIONS_NEUTRAL = "." + File.separator + "blueprints-stations" + File.separator + "neutral" + File.separator + "DATA" + File.separator;
        ENTITY_BLUEPRINT_PATH_STATIONS_PIRATE = "." + File.separator + "blueprints-stations" + File.separator + "pirate" + File.separator;
        SEGMENT_DATA_BLUEPRINT_PATH_STATIONS_PIRATE = "." + File.separator + "blueprints-stations" + File.separator + "pirate" + File.separator + "DATA" + File.separator;
        ENTITY_BLUEPRINT_PATH_STATIONS_TRADING_GUILD = "." + File.separator + "blueprints-stations" + File.separator + "trading-guild" + File.separator;
        SEGMENT_DATA_BLUEPRINT_PATH_STATIONS_TRADING_GUILD = "." + File.separator + "blueprints-stations" + File.separator + "trading-guild" + File.separator + "DATA" + File.separator;
        SEGMENT_DATA_DATABASE_PATH = DATABASE_PATH + File.separator + "DATA" + File.separator;
        boolean var2 = (new FileExt(SEGMENT_DATA_DATABASE_PATH)).mkdirs();
        System.err.println("[INIT] Segment Database Path: " + (new FileExt(SEGMENT_DATA_DATABASE_PATH)).getAbsolutePath() + "; exists? " + (new FileExt(SEGMENT_DATA_DATABASE_PATH)).exists() + "; is Dir? " + (new FileExt(SEGMENT_DATA_DATABASE_PATH)).isDirectory() + "; creating dir successful (gives false if exists)? " + var2);
        if (!(new FileExt(SEGMENT_DATA_DATABASE_PATH)).exists()) {
            if (var1 < 4) {
                System.err.println("[ERROR] Exception creating directories. retrying in one sec");

                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException var5) {
                    var5.printStackTrace();
                }

                initPaths(var0, var1);
            } else {
                GuiErrorHandler.processErrorDialogException(new FileNotFoundException("Cannot create database directories in game installation dir.\nPlease make sure you have the right to do so.\nPlease go to help.star-made.org for additional help."));
            }
        }

        (new FileExt(ENTITY_BLUEPRINT_PATH_STATIONS_NEUTRAL)).mkdirs();
        (new FileExt(SEGMENT_DATA_BLUEPRINT_PATH_STATIONS_NEUTRAL)).mkdirs();
        (new FileExt(ENTITY_BLUEPRINT_PATH_STATIONS_PIRATE)).mkdirs();
        (new FileExt(SEGMENT_DATA_BLUEPRINT_PATH_STATIONS_PIRATE)).mkdirs();
        (new FileExt(ENTITY_BLUEPRINT_PATH_STATIONS_TRADING_GUILD)).mkdirs();
        (new FileExt(SEGMENT_DATA_BLUEPRINT_PATH_STATIONS_TRADING_GUILD)).mkdirs();
        if ((new FileExt(ENTITY_BLUEPRINT_PATH_STATIONS_NEUTRAL)).list().length == 1 && (new FileExt(ENTITY_BLUEPRINT_PATH_STATIONS_PIRATE)).list().length == 1 && (new FileExt(ENTITY_BLUEPRINT_PATH_STATIONS_TRADING_GUILD)).list().length == 1) {
            PreparingFilesJFrame var6 = null;
            if (var0) {
                try {
                    (var6 = new PreparingFilesJFrame()).setVisible(true);
                } catch (Exception var4) {
                    var4.printStackTrace();
                }
            }

            try {
                LoadingScreen.serverMessage = Lng.ORG_SCHEMA_GAME_SERVER_DATA_GAMESERVERSTATE_4;
                FileUtil.extract(new FileExt("." + File.separator + "data" + File.separator + "prefabBlueprints" + File.separator + "defaultStations.zip"), "." + File.separator + "blueprints-stations");
            } catch (IOException var3) {
                var3.printStackTrace();
            }

            if (var6 != null) {
                var6.dispose();
            }
        }

    }

    public static synchronized int getItemId() {
        return itemIds++;
    }

    public static void readDatabasePosition(boolean var0) throws IOException {
        ServerConfig.read();
        if (ServerConfig.WORLD.getCurrentState().toString().toLowerCase(Locale.ENGLISH).equals("unset")) {
            if ((new FileExt("." + File.separator + "server-database" + File.separator + "index" + File.separator)).exists()) {
                ServerConfig.WORLD.setCurrentState("old");
            } else {
                ServerConfig.WORLD.setCurrentState("world0");
            }
        }

        if (!ServerConfig.WORLD.getCurrentState().toString().toLowerCase(Locale.ENGLISH).equals("old")) {
            String var1;
            DATABASE_PATH = var1 = "." + File.separator + "server-database" + File.separator + ServerConfig.WORLD.getCurrentState().toString().toLowerCase(Locale.ENGLISH) + File.separator;
            System.out.println("[SERVER] using world: " + ServerConfig.WORLD.getCurrentState().toString() + "; " + var1);
            initPaths(var0, 0);
        } else {
            DATABASE_PATH = "." + File.separator + "server-database" + File.separator;
            initPaths(var0, 0);
            System.out.println("[SERVER] using old world (std server-database)");
        }

        ServerConfig.write();
    }

    public void addCountdownMessage(int var1, String var2) {
        if (var1 >= 0) {
            this.setTimedMessageStart(System.currentTimeMillis());
            this.setTimedMessageSeconds(var1);
            this.setTimedMessage(var2);
        } else {
            this.timedShutdownStart = -1L;
            this.getGameState().getNetworkObject().serverCountdownTime.set(-1.0F);
            this.getGameState().getNetworkObject().serverCountdownMessage.set("");
        }
    }

    public void addServerFileRequest(ClientChannel var1, String var2) {
        synchronized(this.getFileRequests()) {
            this.getFileRequests().add(new GameServerState.FileRequest(var1, var2));
        }
    }

    public void addTimedShutdown(int var1) {
        if (var1 >= 0) {
            this.timedShutdownStart = System.currentTimeMillis();
            this.timedShutdownSeconds = var1;
        } else {
            this.timedShutdownStart = -1L;
            this.getGameState().getNetworkObject().serverShutdown.set(-1.0F);
        }
    }

    public void announceModifiedBlueprintUsage(BlueprintInterface var1, String var2) {
        try {
            PlayerState var10;
            if ((var10 = this.getPlayerFromName(var2)) != null) {
                long var3;
                long var5 = (var3 = ((Integer)ServerConfig.AUTO_BAN_TIME_IN_MINUTES.getCurrentState()).longValue()) > 0L ? System.currentTimeMillis() + var3 * 60000L : -1L;
                if (ServerConfig.AUTO_BAN_ID_MODIFIED_BLUEPRINT_USE.isOn()) {
                    System.err.println("[SERVER] banning name for modified blueprint use " + var10.getName());
                    LogUtil.log().fine("[SERVER] banning name for modified blueprint use " + var10.getName());
                    this.getController().addBannedName("[SYSTEM]", var10.getName(), var5);
                }

                if (ServerConfig.AUTO_BAN_IP_MODIFIED_BLUEPRINT_USE.isOn() && var10.getIp() != null) {
                    System.err.println("[SERVER] banning IP for modified blueprint use " + var10.getIp());

                    try {
                        LogUtil.log().fine("[SERVER] banning IP for modified blueprint use " + var10.getIp() + " (name: " + var10.getName() + ")");
                        this.getController().addBannedIp("[SYSTEM]", var10.getIp().startsWith("/") ? var10.getIp().substring(1) : var10.getIp(), var5);
                    } catch (NoIPException var8) {
                        var8.printStackTrace();
                    }
                }

                if (ServerConfig.AUTO_KICK_MODIFIED_BLUEPRINT_USE.isOn()) {
                    try {
                        System.err.println("[SERVER] kicking for modified blueprint use " + var10.getName());
                        this.getController().sendLogout(var10.getClientId(), "You have been kick for using a modified blueprint");
                    } catch (IOException var7) {
                        var7.printStackTrace();
                    }
                }

                if (ServerConfig.REMOVE_MODIFIED_BLUEPRINTS.isOn()) {
                    System.err.println("[SERVER] removing modified blueprint " + var1.getName());
                    this.getController().broadcastMessageAdmin(new Object[]{472, "blueprints/exported/" + var1.getName() + ".sment"}, 3);
                    var10.sendServerMessage(new ServerMessage(new Object[]{473, "blueprints/exported/" + var1.getName() + ".sment"}, 3, 3));
                    this.getCatalogManager().serverDeletEntry(var1.getName());
                }
            }

        } catch (PlayerNotFountException var9) {
            var9.printStackTrace();
        }
    }

    public void chat(ChatSystem var1, String var2, String var3, boolean var4) {
        this.chatLog.add(var1.getOwnerStateId() + ": " + var2);
    }

    public ChatSystem getChat() {
        return this.chat;
    }

    public String[] getCommandPrefixes() {
        return null;
    }

    public byte[] getDataBuffer() {
        return this.buffer;
    }

    public ByteBuffer getDataByteBuffer() {
        synchronized(this.bufferPool) {
            ++buffersInUse;

            while(this.bufferPool.isEmpty()) {
                try {
                    this.bufferPool.wait(20000L);
                } catch (InterruptedException var3) {
                    var3.printStackTrace();
                }
            }

            --buffersInUse;
            return (ByteBuffer)this.bufferPool.dequeue();
        }
    }

    public String getVersion() {
        return Version.VERSION;
    }

    public void needsNotify(Sendable var1) {
        synchronized(this.needsNotifyObjects) {
            this.needsNotifyObjects.add(var1);
        }
    }

    public void notifyOfAddedObject(Sendable var1) {
        SegmentController var2;
        if (var1 instanceof SegmentController && (var2 = (SegmentController)var1).getCreatorThread() == null) {
            var2.startCreatorThread();
        }

        synchronized(this.flaggedAddedObjects) {
            this.flaggedAddedObjects.add(var1);
        }
    }

    public void notifyOfRemovedObject(Sendable var1) {
        synchronized(this.flaggedRemovedObjects) {
            this.flaggedRemovedObjects.add(var1);
        }
    }

    public String onAutoComplete(String var1, TextCallback var2, String var3) {
        System.err.println("NO AUTOCOMPLETE ON SERVER");
        return var1;
    }

    public boolean onChatTextEnterHook(ChatSystem var1, String var2, boolean var3) {
        return false;
    }

    public void onStringCommand(String var1, TextCallback var2, String var3) {
        throw new IllegalArgumentException();
    }

    public void releaseDataByteBuffer(ByteBuffer var1) {
        synchronized(this.bufferPool) {
            this.bufferPool.enqueue(var1);
            this.bufferPool.notify();
        }
    }

    public ResourceMap getResourceMap() {
        return this.resourceMap;
    }

    public void setResourceMap(ResourceMap var1) {
        this.resourceMap = var1;
    }

    public long getUpdateTime() {
        return this.udpateTime;
    }

    public void setSynched() {
        assert !this.synched : this.printLast();

        assert this.setLastSynch();

        this.synched = true;
    }

    private String printLast() {
        if (this.currentE != null) {
            System.err.println("CURRENTLY SYNCHED BY:");
            this.currentE.printStackTrace();
        }

        return this.currentE.getMessage();
    }

    private boolean setLastSynch() {
        try {
            throw new Exception("Trace");
        } catch (Exception var2) {
            this.currentE = var2;
            return true;
        }
    }

    public void setUnsynched() {
        assert this.synched;

        this.synched = false;
    }

    public boolean isSynched() {
        return this.synched;
    }

    public long getUploadBlockSize() {
        return 256L;
    }

    public void setChat(ChatSystem var1) {
        this.chat = var1;
    }

    public void executeAdminCommand(String var1, String var2, RegisteredClientInterface var3) {
        if (!(var3 instanceof AdminLocalClient) && !ServerConfig.SUPER_ADMIN_PASSWORD_USE.isOn()) {
            try {
                var3.serverMessage("END; ERROR: super admin not enabled on this server");
            } catch (IOException var6) {
                var1 = null;
                var6.printStackTrace();
            }

            var3.executedAdminCommand();
        } else if (var1 != null && var1.equals(ServerConfig.SUPER_ADMIN_PASSWORD.getCurrentState())) {
            try {
                String[] var13;
                if (var2.startsWith("/chatchannel ")) {
                    if ((var13 = StringTools.splitParameters(var2.substring(12))).length == 2) {
                        ChatChannel var21;
                        if ((var21 = this.channelRouter.getChannel(var13[0])) != null) {
                            ChatMessage var18;
                            (var18 = new ChatMessage()).sender = "[SERVER]";
                            var18.receiver = var21.getUniqueChannelName();
                            var18.receiverType = ChatMessageType.CHANNEL;
                            var18.text = var13[1];
                            var21.send(var18);
                            var3.serverMessage("END; broadcasted as server message: " + var13[1]);
                            var3.executedAdminCommand();
                        } else {
                            var3.serverMessage("END; error: chat channel not found: " + var13[0] + "; note: General channel is \"all\" faction channels are \"Faction<fid>\" (e.g. Faction1001) (all channels are case sensitive)");
                        }
                    } else {
                        var3.serverMessage("END; error: invalid format: use /chatchannel \"channel name\" \"message\"");
                    }
                } else if (var2.startsWith("/chat ")) {
                    AllChannel var17 = this.channelRouter.getAllChannel();
                    ChatMessage var20;
                    (var20 = new ChatMessage()).sender = "[SERVER]";
                    var20.receiver = var17.getUniqueChannelName();
                    var20.receiverType = ChatMessageType.CHANNEL;
                    var20.text = var2.substring(6);
                    var17.send(var20);
                    var3.serverMessage("END; broadcasted as server message: " + var2.substring(6));
                    var3.executedAdminCommand();
                } else if (var2.startsWith("/pm ")) {
                    if ((var13 = var2.split(" ", 3)).length == 3) {
                        String var19 = var13[1].trim();

                        try {
                            PlayerState var16;
                            (var16 = this.getPlayerFromName(var19)).sendServerMessage(new ServerMessage(new Object[]{"[SERVER-PM] " + var13[2]}, 0, var16.getId()));
                            var3.serverMessage("END; send to " + var19 + " as server message: " + var13[2]);
                        } catch (PlayerNotFountException var7) {
                            var7.printStackTrace();
                            var3.serverMessage("END; player not found: '" + var19 + "'");
                        }
                    } else {
                        var3.serverMessage("END; not enough parameters for PM (/pm playername text) ");
                    }

                    var3.executedAdminCommand();
                } else if (var2.startsWith("/")) {
                    try {
                        AdminCommands var4 = AdminCommands.valueOf((var13 = (var2 = var2.substring(1)).split(" "))[0].trim().toUpperCase(Locale.ENGLISH));
                        String[] var14 = StringTools.splitParameters(var2.substring(var13[0].length()));
                        Object[] var15 = AdminCommands.packParameters(var4, var14);
                        this.getController().enqueueAdminCommand(var3, var4, var15);
                    } catch (Exception var8) {
                        var8.printStackTrace();
                        var3.serverMessage("Admin command failed: Error packing parameters");
                        var3.executedAdminCommand();
                    }
                } else {
                    try {
                        var3.serverMessage("END; ERROR: command not regognize (use /chat to broadcast)");
                        var3.executedAdminCommand();
                    } catch (IOException var9) {
                        var1 = null;
                        var9.printStackTrace();
                    }
                }
            } catch (IOException var10) {
                IOException var12 = var10;
                var10.printStackTrace();

                try {
                    var3.serverMessage("END; Error: " + var12.getClass() + ": " + var12.getMessage());
                    var3.executedAdminCommand();
                } catch (IOException var5) {
                    var5.printStackTrace();
                }
            }
        } else {
            try {
                var3.serverMessage("END; ERROR: wrong super password");
            } catch (IOException var11) {
                var1 = null;
                var11.printStackTrace();
            }

            var3.executedAdminCommand();
        }
    }

    public boolean filterJoinMessages() {
        return ServerConfig.FILTER_CONNECTION_MESSAGES.isOn();
    }

    public boolean flushPingImmediately() {
        return ServerConfig.PING_FLUSH.isOn();
    }

    public String getAcceptingIP() {
        return (String)ServerConfig.SERVER_LISTEN_IP.getCurrentState();
    }

    public int getMaxClients() {
        return (Integer)ServerConfig.MAX_CLIENTS.getCurrentState();
    }

    public NetworkProcessor getProcessor(int var1) {
        return ((RegisteredClientOnServer)this.getClients().get(var1)).getProcessor();
    }

    public String getServerDesc() {
        return this.gameState.getServerDescription();
    }

    public String getServerName() {
        return this.gameState.getServerName();
    }

    public int getSocketBufferSize() {
        return (Integer)ServerConfig.SOCKET_BUFFER_SIZE.getCurrentState();
    }

    public long getStartTime() {
        return this.serverStartTime;
    }

    public boolean tcpNoDelay() {
        return ServerConfig.TCP_NODELAY.isOn();
    }

    public boolean useUDP() {
        return ServerConfig.USE_UDP.isOn();
    }

    public SessionCallback getSessionCallBack(String var1, String var2) {
        return (SessionCallback)(Starter.getAuthStyle() == 0 ? new OldSessionCallback(var1, var2) : new NewSessionCallback(var1, var2));
    }

    public void addNTReceivedStatistics(RegisteredClientOnServer var1, int var2, int var3, int var4, int var5, Object[] var6, ObjectArrayList<NetworkObject> var7) {
    }

    public int getNTSpamProtectTimeMs() {
        return (Integer)ServerConfig.NT_SPAM_PROTECT_TIME_MS.getCurrentState();
    }

    public int getNTSpamProtectMaxAttempty() {
        return (Integer)ServerConfig.NT_SPAM_PROTECT_MAX_ATTEMPTS.getCurrentState();
    }

    public String getNTSpamProtectException() {
        return ServerConfig.NT_SPAM_PROTECT_EXCEPTIONS.getCurrentState().toString();
    }

    public boolean isNTSpamCheckActive() {
        return ServerConfig.NT_SPAM_PROTECT_ACTIVE.isOn();
    }

    public boolean announceServer() {
        return ServerConfig.ANNOUNCE_SERVER_TO_SERVERLIST.isOn();
    }

    public String announceHost() {
        return ServerConfig.HOST_NAME_TO_ANNOUNCE_TO_SERVER_LIST.getCurrentState().toString();
    }

    public boolean checkUserAgent(byte var1, String var2) {
        System.err.println("[LOGIN] checking user agent: " + var1 + ": " + var2);
        if (var1 == 1) {
            System.err.println("[LOGIN] checking StarMote access for: " + var2 + ": " + this.isAdmin(var2));
            return this.isAdmin(var2);
        } else {
            return true;
        }
    }

    public List<GameServerState.FileRequest> getActiveFileRequests() {
        return this.activeFileRequests;
    }

    public List<AdminCommandQueueElement> getAdminCommands() {
        return this.adminCommands;
    }

    public void setAdminCommands(ArrayList<AdminCommandQueueElement> var1) {
        this.adminCommands = var1;
    }

    public AdminLocalClient getAdminLocalClient() {
        return this.adminLocalClient;
    }

    public Map<String, Admin> getAdmins() {
        return this.admins;
    }

    public PlayerAccountEntrySet getBlackListedIps() {
        return this.blackListedIps;
    }

    public PlayerAccountEntrySet getBlackListedNames() {
        return this.blackListedNames;
    }

    public PlayerAccountEntrySet getBlackListedAccounts() {
        return this.blackListedAccounts;
    }

    public byte[] getBlockConfigFile() {
        return this.blockConfigFile;
    }

    public void setBlockConfigFile(byte[] var1) {
        this.blockConfigFile = var1;
    }

    public byte[] getBlockPropertiesFile() {
        return this.blockPropertiesFile;
    }

    public void setBlockPropertiesFile(byte[] var1) {
        this.blockPropertiesFile = var1;
    }

    public byte[] getBlockBehaviorFile() {
        return this.blockBehaviorBytes;
    }

    public List<SegmentControllerOutline> getBluePrintsToSpawn() {
        return this.bluePrintsToSpawn;
    }

    public CatalogManager getCatalogManager() {
        return this.gameState.getCatalogManager();
    }

    public String getConfigCheckSum() {
        return this.configCheckSum;
    }

    public void setConfigCheckSum(String var1) {
        this.configCheckSum = var1;
    }

    public String getConfigPropertiesCheckSum() {
        return this.configPropertiesCheckSum;
    }

    public void setConfigPropertiesCheckSum(String var1) {
        this.configPropertiesCheckSum = var1;
    }

    public String getCustomTexturesChecksum() {
        return this.customTexturesChecksum;
    }

    public void setCustomTexturesChecksum(String var1) {
        this.customTexturesChecksum = var1;
    }

    public ObjectArrayList<RegionHook<? extends UsableRegion>> getCreatorHooks() {
        return this.creatorHooks;
    }

    public ObjectArrayList<SimpleTransformableSendableObject> getCurrentGravitySources() {
        return this.currentGravitySources;
    }

    public DatabaseIndex getDatabaseIndex() {
        return this.databaseIndex;
    }

    public List<EntityRequest> getEntityRequests() {
        return this.entityRequests;
    }

    public FactionManager getFactionManager() {
        return this.gameState != null ? this.gameState.getFactionManager() : null;
    }

    public List<GameServerState.FileRequest> getFileRequests() {
        return this.fileRequests;
    }

    public GameMapProvider getGameMapProvider() {
        return this.gameMapProvider;
    }

    public GameModes getGameMode() {
        return this.gameMode;
    }

    public void setGameMode(GameModes var1) {
        this.gameMode = var1;
    }

    public SendableGameState getGameState() {
        return this.gameState;
    }

    public void setGameState(SendableGameState var1) {
        this.gameState = var1;
    }

    public boolean isPhysicalAsteroids() {
        return ServerConfig.ASTEROIDS_ENABLE_DYNAMIC_PHYSICS.isOn();
    }

    public Document getBlockBehaviorConfig() {
        return this.blockBehaviorConfig;
    }

    public void setBlockBehaviorConfig(Document var1) {
        this.blockBehaviorConfig = var1;
    }

    public float getSectorSize() {
        return this.getSectorSizeWithoutMargin() + 300.0F;
    }

    public Short2ObjectOpenHashMap<Technology> getAllTechs() {
        return this.techs;
    }

    public boolean getMaterialPrice() {
        return ServerConfig.USE_DYNAMIC_RECIPE_PRICES.isOn();
    }

    public int getSegmentPieceQueueSize() {
        return !Starter.DEDICATED_SERVER_ARGUMENT ? (Integer)EngineSettings.SEGMENT_PIECE_QUEUE_SINGLEPLAYER.getCurrentState() : (Integer)ServerConfig.NT_BLOCK_QUEUE_SIZE.getCurrentState();
    }

    public ControlElementMapOptimizer getControlOptimizer() {
        return this.controlOptimizer;
    }

    public ChannelRouter getChannelRouter() {
        return this.channelRouter;
    }

    public MetaObjectManager getMetaObjectManager() {
        return this.metaObjectManager;
    }

    public void requestMetaObject(int var1) {
        throw new IllegalArgumentException("This may not be called for server");
    }

    public PlayerState getPlayerFromName(String var1) throws PlayerNotFountException {
        PlayerState var2;
        if ((var2 = (PlayerState)this.playerStatesByName.get(var1)) != null) {
            return var2;
        } else {
            throw new PlayerNotFountException(var1);
        }
    }

    public PlayerState getPlayerFromNameIgnoreCase(String var1) throws PlayerNotFountException {
        PlayerState var2;
        if ((var2 = this.getPlayerFromNameIgnoreCaseWOException(var1)) != null) {
            return var2;
        } else {
            throw new PlayerNotFountException(var1);
        }
    }

    public PlayerState getPlayerFromNameIgnoreCaseWOException(String var1) {
        return (PlayerState)this.playerStatesByNameLowerCase.get(var1.toLowerCase(Locale.ENGLISH));
    }

    public PlayerState getPlayerFromStateId(int var1) throws PlayerNotFountException {
        PlayerState var2;
        if ((var2 = (PlayerState)this.playerStatesByClientId.get(var1)) != null) {
            return var2;
        } else {
            throw new PlayerNotFountException("CLIENT-ID(" + var1 + ") ");
        }
    }

    public Map<String, PlayerState> getPlayerStatesByName() {
        return this.playerStatesByName;
    }

    public HashMap<String, ProtectedUplinkName> getProtectedUsers() {
        return this.protectedNames;
    }

    public List<Sendable> getScheduledUpdates() {
        return this.scheduledUpdates;
    }

    public List<SectorSwitch> getSectorSwitches() {
        return this.sectorSwitches;
    }

    public Map<String, SegmentController> getSegmentControllersByName() {
        return this.segmentControllersByName;
    }

    public SegmentDataManager getSegmentDataManager() {
        return this.segmentDataManager;
    }

    public ObjectArrayFIFOQueue<ServerSegmentRequest> getSegmentRequests() {
        return this.segmentRequests;
    }

    public ObjectArrayFIFOQueue<ServerExecutionJob> getServerExecutionJobs() {
        return this.serverExecutionJobs;
    }

    public long getServerStartTime() {
        return this.serverStartTime;
    }

    public long getServerTimeMod() {
        return this.serverTimeMod;
    }

    public void setServerTimeMod(long var1) {
        this.serverTimeMod = var1;
    }

    public SimulationManager getSimulationManager() {
        return this.simulationManager;
    }

    public Set<PlayerState> getSpawnRequests() {
        return this.spawnRequests;
    }

    public ThreadedSegmentWriter getThreadedSegmentWriter() {
        return this.threadedSegmentWriter;
    }

    public String getTimedMessage() {
        return this.timedMessage;
    }

    public void setTimedMessage(String var1) {
        this.timedMessage = var1;
    }

    public int getTimedMessageSeconds() {
        return this.timedMessageSeconds;
    }

    public void setTimedMessageSeconds(int var1) {
        this.timedMessageSeconds = var1;
    }

    public long getTimedMessageStart() {
        return this.timedMessageStart;
    }

    public void setTimedMessageStart(long var1) {
        this.timedMessageStart = var1;
    }

    public int getTimedShutdownSeconds() {
        return this.timedShutdownSeconds;
    }

    public void setTimedShutdownSeconds(int var1) {
        this.timedShutdownSeconds = var1;
    }

    public long getTimedShutdownStart() {
        return this.timedShutdownStart;
    }

    public void setTimedShutdownStart(long var1) {
        this.timedShutdownStart = var1;
    }

    public Universe getUniverse() {
        return this.universe;
    }

    public PlayerAccountEntrySet getWhiteListedIps() {
        return this.whiteListedIps;
    }

    public PlayerAccountEntrySet getWhiteListedNames() {
        return this.whiteListedNames;
    }

    public PlayerAccountEntrySet getWhiteListedAccounts() {
        return this.whiteListedAccounts;
    }

    public void handleAddedAndRemovedObjects() throws IOException, SQLException {
        long var1 = System.currentTimeMillis();
        HashSet var3;
        if (!this.flaggedAddedObjects.isEmpty()) {
            var3 = new HashSet(this.flaggedAddedObjects.size());
            synchronized(this.flaggedAddedObjects) {
                var3.addAll(this.flaggedAddedObjects);
            }

            Iterator var4 = var3.iterator();

            while(var4.hasNext()) {
                Sendable var9 = (Sendable)var4.next();
                this.onSendableAdded(var9);
                SendableSegmentController var5;
                if (var9 instanceof SendableSegmentProvider && (var5 = (SendableSegmentController)((SendableSegmentProvider)var9).getSegmentController()) != null) {
                    var5.setServerSendableSegmentController((SendableSegmentProvider)var9);
                    ((SendableSegmentProvider)var9).setConnectionReady();
                }

                this.setChanged();
                this.notifyObservers(var9);
                if (var9 instanceof SegmentController) {
                    this.getFleetManager().onAddedEntity((SegmentController)var9);
                }

                if (var9 instanceof PlayerState) {
                    this.getFleetManager().onJoinedPlayer((PlayerState)var9);
                }
            }

            this.flaggedAddedObjects.clear();
        }

        long var10;
        if ((var10 = System.currentTimeMillis() - var1) > 10L) {
            System.err.println("[SERVER][UPDATE] WARNING: handleAddedAndRemovedObjects update took " + var10);
        }

        if (!this.flaggedRemovedObjects.isEmpty()) {
            var3 = new HashSet(this.flaggedRemovedObjects.size());
            synchronized(this.flaggedRemovedObjects) {
                var3.addAll(this.flaggedRemovedObjects);
            }

            Iterator var11 = var3.iterator();

            while(var11.hasNext()) {
                Sendable var8;
                if ((var8 = (Sendable)var11.next()) instanceof DiskWritable && !var8.isWrittenForUnload()) {
                    var8.setWrittenForUnload(true);
                    this.getController().writeEntity((DiskWritable)var8, true);
                }

                this.onSendableRemoved(var8);
                this.setChanged();
                this.notifyObservers(var8);
                if (var8 instanceof PlayerState) {
                    this.getFleetManager().onLeftPlayer((PlayerState)var8);
                }
            }

            this.flaggedRemovedObjects.clear();
        }

    }

    public void handleNeedNotifyObjects() throws IOException, SQLException {
        long var1 = System.currentTimeMillis();
        if (!this.needsNotifyObjects.isEmpty()) {
            HashSet var3 = new HashSet();
            synchronized(this.needsNotifyObjects) {
                var3.addAll(this.needsNotifyObjects);
            }

            Iterator var4 = var3.iterator();

            while(var4.hasNext()) {
                Sendable var6 = (Sendable)var4.next();
                this.setChanged();
                this.notifyObservers(var6);
            }

            this.needsNotifyObjects.clear();
        }

        long var7;
        if ((var7 = System.currentTimeMillis() - var1) > 10L) {
            System.err.println("[SERVER][UPDATE] WARNING: needsNotifyObjects update took " + var7);
        }

    }

    public boolean isAdmin(String var1) {
        return this.admins.isEmpty() || this.admins.containsKey(var1.toLowerCase(Locale.ENGLISH));
    }

    public boolean isFactionReinstitudeFlag() {
        return this.factionReinstitudeFlag;
    }

    public void setFactionReinstitudeFlag(boolean var1) {
        this.factionReinstitudeFlag = var1;
    }

    public boolean isReady() {
        return super.isReady() && !isShutdown();
    }

    public GameServerController getController() {
        return (GameServerController)super.getController();
    }

    public int getClientIdByName(String var1) throws ClientIdNotFoundException {
        try {
            return this.getPlayerFromName(var1).getClientId();
        } catch (PlayerNotFountException var2) {
            throw new ClientIdNotFoundException(var1);
        }
    }

    public String getBuild() {
        return Version.build;
    }

    private void onSendableAdded(Sendable var1) {
        if (var1 instanceof PlayerState) {
            this.playerStatesByName.put(((PlayerState)var1).getName(), (PlayerState)var1);
            this.getPlayerStatesByNameLowerCase().put(((PlayerState)var1).getName().toLowerCase(Locale.ENGLISH), (PlayerState)var1);
            this.playerStatesByClientId.put(((PlayerState)var1).getClientId(), (PlayerState)var1);
            this.playerStatesByDbId.put(((PlayerState)var1).getDbId(), (PlayerState)var1);
            this.getSimulationManager().getPlanner().playerAdded((PlayerState)var1);
        }

        if (var1 instanceof SegmentController) {
            this.getSegmentControllersByName().put(((SegmentController)var1).getUniqueIdentifier(), (SegmentController)var1);
            this.getSegmentControllersByNameLowerCase().put(((SegmentController)var1).getUniqueIdentifier().toLowerCase(Locale.ENGLISH), (SegmentController)var1);
        }

        if (var1 instanceof SimpleTransformableSendableObject && ((SimpleTransformableSendableObject)var1).isGravitySource()) {
            this.getCurrentGravitySources().add((SimpleTransformableSendableObject)var1);
        }

        if (var1 instanceof RemoteSector) {
            this.getUniverse().onAddedSectorSynched(((RemoteSector)var1).getServerSector());
        }
        //INSERTED CODE
        ServerSendableAddEvent event = new ServerSendableAddEvent(this, var1, Event.Condition.POST);
        StarLoader.fireEvent(event, true);
        StarLoaderHooks.onServerSendableAddEvent(event);
        ///

    }

    private void onSendableRemoved(Sendable var1) {
        if (var1 instanceof PlayerState) {
            this.playerStatesByName.remove(((PlayerState)var1).getName());
            this.getPlayerStatesByNameLowerCase().remove(((PlayerState)var1).getName().toLowerCase(Locale.ENGLISH));
            this.playerStatesByClientId.remove(((PlayerState)var1).getClientId());
            this.playerStatesByDbId.remove(((PlayerState)var1).getDbId());
            this.getSimulationManager().getPlanner().playerRemoved((PlayerState)var1);
        }

        if (var1 instanceof SegmentController) {
            this.getSegmentControllersByName().remove(((SegmentController)var1).getUniqueIdentifier());
            this.getSegmentControllersByNameLowerCase().remove(((SegmentController)var1).getUniqueIdentifier().toLowerCase(Locale.ENGLISH));
            this.getFleetManager().onRemovedEntity((SegmentController)var1);
        }

        if (var1 instanceof SimpleTransformableSendableObject && ((SimpleTransformableSendableObject)var1).isGravitySource()) {
            this.getCurrentGravitySources().remove(var1);
        }

        if (var1.isMarkedForPermanentDelete()) {
            if (var1 instanceof UniqueInterface) {
                this.getSimulationManager().removeMemberFromGroups(((UniqueInterface)var1).getUniqueIdentifier());
            }

            var1.destroyPersistent();
        }

        if (var1 instanceof RemoteSector) {
            this.getUniverse().onRemovedSectorSynched(((RemoteSector)var1).getServerSector());
        }

        //INSERTED CODE
        ServerSendableRemoveEvent event = new ServerSendableRemoveEvent(this, var1, Event.Condition.POST);
        StarLoader.fireEvent(event, true);
        ///

    }

    public void scheduleExecutionJob(ServerExecutionJob var1) {
        synchronized(this.getServerExecutionJobs()) {
            this.getServerExecutionJobs().enqueue(var1);
        }
    }

    public void scheduleSectorBulkExport(RegisteredClientInterface var1, String var2) {
        SectorBulkRequest var3 = new SectorBulkRequest(var1, var2, true);
        this.scheduleExecutionJob(var3);
    }

    public void scheduleSectorBulkImport(RegisteredClientInterface var1, String var2) {
        SectorBulkRequest var3 = new SectorBulkRequest(var1, var2, false);
        this.scheduleExecutionJob(var3);
    }

    public void scheduleSectorExport(Vector3i var1, RegisteredClientInterface var2, String var3) {
        SectorExportRequest var4 = new SectorExportRequest(var1, var2, var3);
        this.scheduleExecutionJob(var4);
    }

    public void scheduleSectorImport(Vector3i var1, RegisteredClientInterface var2, String var3) {
        SectorImportRequest var4 = new SectorImportRequest(var1, var2, var3);
        this.scheduleExecutionJob(var4);
    }

    public void scheduleUpdate(Sendable var1) {
        synchronized(this.getScheduledUpdates()) {
            this.getScheduledUpdates().add(var1);
        }
    }

    public void spawnMobs(int var1, String var2, Vector3i var3, Transform var4, int var5, BluePrintController var6) throws EntityNotFountException, IOException, EntityAlreadyExistsException {
        this.getMobSpawnThread().spawnMobs(var1, var2, var3, var4, var5, var6);
    }

    public ObjectArrayFIFOQueue<ServerSegmentRequest> getSegmentRequestsLoaded() {
        return this.segmentRequestsLoaded;
    }

    public void setBlockBehaviorBytes(byte[] var1) {
        this.blockBehaviorBytes = var1;
    }

    public String getBlockBehaviorChecksum() {
        return this.blockBehaviorChecksum;
    }

    public void setBlockBehaviorChecksum(String var1) {
        this.blockBehaviorChecksum = var1;
    }

    public float getSectorSizeWithoutMargin() {
        return ((Integer)ServerConfig.SECTOR_SIZE.getCurrentState()).floatValue();
    }

    public ServerPlayerMessager getServerPlayerMessager() {
        return this.serverPlayerMessager;
    }

    public Map<String, SegmentController> getSegmentControllersByNameLowerCase() {
        return this.segmentControllersByNameLowerCase;
    }

    public byte[] getCustomTexturesFile() {
        return this.customTextureFile;
    }

    public void setCustomTexturesFile(byte[] var1) {
        this.customTextureFile = var1;
    }

    public Map<String, PlayerState> getPlayerStatesByNameLowerCase() {
        return this.playerStatesByNameLowerCase;
    }

    public void scanOnServer(ScannerCollectionManager var1, PlayerState var2) {
        System.err.println("[SERVER][SCAN] processing scan ....");

        try {
            StellarSystem var9 = this.getUniverse().getStellarSystemFromStellarPos(var2.getCurrentSystem());

            assert var9.getPos().equals(var2.getCurrentSystem());

            float var3 = ScannerElementManager.DEFAULT_SCAN_DISTANCE;
            SystemOwnershipType var4 = this.getUniverse().getSystemOwnerShipType(var9, var2.getFactionId());
            System.err.println("[SERVER] performing scan for " + var2 + " in system " + var9 + ": " + var4.name() + ": " + var9.getOwnerUID() + "; " + var9.getOwnerFaction());
            boolean var10 = false;
            switch(var4) {
                case BY_ALLY:
                    var3 *= ScannerElementManager.SLLY_SYSTEM_DISTANCE_MULT;
                    break;
                case BY_ENEMY:
                    var3 *= ScannerElementManager.ENEMY_SYSTEM_DISTANCE_MULT;
                case BY_NEUTRAL:
                case NONE:
                default:
                    break;
                case BY_SELF:
                    var3 *= ScannerElementManager.SLLY_SYSTEM_DISTANCE_MULT;
                    var10 = true;
            }

            ScanData var5;
            (var5 = new ScanData()).origin = new Vector3i(var2.getCurrentSector());
            var5.time = System.currentTimeMillis();
            var5.systemOwnerShipType = var4;
            var5.range = var3;
            Iterator var11 = this.getPlayerStatesByName().values().iterator();

            label59:
            while(true) {
                PlayerState var6;
                do {
                    do {
                        do {
                            if (!var11.hasNext()) {
                                if (var2.getClientChannel() != null) {
                                    var2.getClientChannel().getNetworkObject().scanDataUpdates.add(new RemoteScanData(var5, var2.isOnServer()));
                                } else {
                                    System.err.println("[SERVER] Error: Exception: cannot send scan result to " + var2 + "; client channel is null");
                                }

                                var2.addScanHistory(var5);
                                break label59;
                            }
                        } while((var6 = (PlayerState)var11.next()) == var2);
                    } while(var6.isInvisibilityMode());
                } while(Vector3fTools.length(var6.getCurrentSector(), var2.getCurrentSector()) > var3 && (!var10 || !var6.getCurrentSystem().equals(var2.getCurrentSystem())));

                SimpleTransformableSendableObject var7 = var6.getFirstControlledTransformableWOExc();
                var5.add(var2, var6, var7);
            }
        } catch (IOException var8) {
            var8.printStackTrace();
        }

        System.err.println("[SERVER][SCAN] " + this + " Scannig " + var2.getCurrentSystem());
        var2.getFogOfWar().scan(var2.getCurrentSystem());
    }

    public byte[] getFactionConfigFile() {
        return this.factionConfigFile;
    }

    public void setFactionConfigFile(byte[] var1) {
        this.factionConfigFile = var1;
    }

    public String getFactionConfigCheckSum() {
        return this.factionConfigCheckSum;
    }

    public void setFactionConfigCheckSum(String var1) {
        this.factionConfigCheckSum = var1;
    }

    public GameConfig getGameConfig() {
        return this.gameConfig;
    }

    public ObjectArrayFIFOQueue<ExplosionRunnable> getExplosionOrdersFinished() {
        return this.explosionOrdersFinished;
    }

    public void enqueueExplosion(ExplosionRunnable var1) {
        this.explosionOrdersQueued.add(var1);
    }

    public ObjectArrayList<ExplosionRunnable> getExplosionOrdersQueued() {
        return this.explosionOrdersQueued;
    }

    public Set<PlayerState> getSpawnRequestsReady() {
        return this.spawnRequestsReady;
    }

    public RaceManager getRaceManager() {
        return this.getGameState().getRaceManager();
    }

    public boolean existsEntity(EntityType var1, String var2) {
        if (this.getLocalAndRemoteObjectContainer().getUidObjectMap().containsKey(var1.dbPrefix + var2)) {
            return true;
        } else {
            Iterator var3 = this.getLocalAndRemoteObjectContainer().getUidObjectMap().keySet().iterator();

            do {
                if (!var3.hasNext()) {
                    try {
                        if (this.getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(var2, 1).size() > 0) {
                            return true;
                        }

                        return false;
                    } catch (SQLException var4) {
                        var4.printStackTrace();
                        return true;
                    }
                }
            } while(!((String)var3.next()).toLowerCase(Locale.ENGLISH).equals((var1.dbPrefix + var2).toLowerCase(Locale.ENGLISH)));

            return true;
        }
    }

    public FleetManager getFleetManager() {
        return this.fleetManager;
    }

    public GameServerState.MobSpawnThread getMobSpawnThread() {
        return this.mobSpawnThread;
    }

    public void doDatabaseInsert(Sendable var1) {
        SegmentController var2;
        if (var1 instanceof SegmentController && (var2 = (SegmentController)var1).dbId < 0L && !var2.isVirtualBlueprint()) {
            Sector var3;
            boolean var5;
            if ((var3 = this.getUniverse().getSector(var2.getSectorId())) == null) {
                var5 = var2.transientSector;
            } else {
                var5 = var3.isTransientSector();
            }

            if (!(var2 instanceof TransientSegmentController) || ((TransientSegmentController)var2).needsTagSave() || !var5) {
                System.err.println("[SERVER] Object " + var1 + " didn't have a db entry yet. Creating entry!");

                try {
                    var2.dbId = this.getDatabaseIndex().getTableManager().getEntityTable().updateOrInsertSegmentController(var2);
                    if (var2.npcSystem != null) {
                        assert var2.npcSpec != null;

                        var2.npcSystem.getContingent().spawn(var2.npcSpec, var2.dbId);
                        var2.npcSystem = null;
                        var2.npcSpec = null;
                    }

                    return;
                } catch (SQLException var4) {
                    var4.printStackTrace();
                }
            }
        }

    }

    public void destroyEntity(long var1) {
        Sendable var3;
        if ((var3 = (Sendable)this.getLocalAndRemoteObjectContainer().getDbObjects().get(var1)) != null && var3 instanceof SimpleTransformableSendableObject) {
            ((SimpleTransformableSendableObject)var3).destroy();
        } else {
            try {
                DatabaseEntry var7;
                if ((var7 = this.getDatabaseIndex().getTableManager().getEntityTable().getById(var1)) != null) {
                    Faction var4;
                    if ((var4 = this.getGameState().getFactionManager().getFaction(var7.faction)) != null && var4 instanceof NPCFaction) {
                        ((NPCFaction)var4).onLostEntity(var1, (SegmentController)null, true);
                    }

                    if (var7 != null) {
                        (new File(var7.getEntityFilePath())).delete();
                    }

                    this.getDatabaseIndex().getTableManager().getEntityTable().removeSegmentController(var1);
                    this.getFleetManager().onRemovedEntity(var1);
                } else {
                    try {
                        throw new Exception("Entity not found: " + var1 + ". may already be removed");
                    } catch (Exception var5) {
                        var5.printStackTrace();
                    }
                }
            } catch (SQLException var6) {
                var6.printStackTrace();
            }
        }
    }

    public String getPlayerNameFromDbIdLowerCase(long var1) {
        String var3;
        if ((var3 = (String)this.playerDbIdToUID.get(var1)) == null) {
            try {
                if ((var3 = this.getDatabaseIndex().getTableManager().getPlayerTable().getPlayerName(var1)) != null) {
                    var3 = var3.toLowerCase(Locale.ENGLISH);
                    this.playerDbIdToUID.put(var1, var3);
                }
            } catch (SQLException var4) {
                var4.printStackTrace();
            }
        }

        return var3;
    }

    public Long2ObjectOpenHashMap<PlayerState> getPlayerStatesByDbId() {
        return this.playerStatesByDbId;
    }

    public ConfigPool getConfigPool() {
        return this.getGameState().getConfigPool();
    }

    public DebugTimer getDebugTimer() {
        return this.debugTimer;
    }

    static {
        DATABASE_PATH = SERVER_DATABASE = "." + File.separator + "server-database" + File.separator;
        dataReceived = new Long(0L);
        debugObj = -1;
        buffersInUse = 0;
        fileLocks = new HashMap();
    }

    public class MobSpawnThread extends Thread {
        private ArrayList<GameServerState.MobSpawnThread.MobSpawnRequest> requests = new ArrayList();
        private boolean shutdown;

        public MobSpawnThread() {
            super("MobSpawnThread");
            this.setDaemon(true);
        }

        public void run() {
            ByteBuffer var1 = ByteBuffer.allocate(1024000);
            ObjectArrayList var2 = new ObjectArrayList();

            label70:
            while(true) {
                if (!this.shutdown) {
                    GameServerState.MobSpawnThread.MobSpawnRequest var3;
                    synchronized(this.requests) {
                        while(true) {
                            if (this.requests.isEmpty()) {
                                try {
                                    this.requests.wait();
                                    if (!this.shutdown) {
                                        continue;
                                    }
                                } catch (InterruptedException var16) {
                                    var16.printStackTrace();
                                    continue;
                                }

                                return;
                            }

                            var3 = (GameServerState.MobSpawnThread.MobSpawnRequest)this.requests.remove(0);
                            break;
                        }
                    }

                    Transform var4 = new Transform();
                    List var5 = var3.bbc.readBluePrints();
                    int var6 = 0;

                    while(true) {
                        if (var6 >= var3.count) {
                            continue label70;
                        }

                        var4.set(var3.transform);
                        var4.origin.set(var4.origin.x + (float)(Math.random() - 0.5D) * 256.0F, var4.origin.y + (float)(Math.random() - 0.5D) * 256.0F, var4.origin.z + (float)(Math.random() - 0.5D) * 256.0F);
                        String var7 = "MOB_" + var3.catalogname + "_" + System.currentTimeMillis() + "_" + var6;

                        for(int var8 = var3.catalogname.length() - 1; var7.length() > 64; --var8) {
                            System.err.println("[SERVER] WARNING: MOB NAME LENGTH TOO LONG: " + var7 + " -> " + var7.length() + "/64");
                            var7 = "MOB_" + var3.catalogname.substring(0, var8) + "_" + System.currentTimeMillis() + "_" + var6;
                        }

                        if (EntityRequest.isShipNameValid(var7)) {
                            try {
                                SegmentControllerOutline var18;
                                (var18 = var3.bbc.loadBluePrint(GameServerState.this, var3.catalogname, var7, var4, -1, var3.factionId, var5, var3.sector, var2, "<system>", var1, true, (SegmentPiece)null, new ChildStats(false))).spawnSectorId = new Vector3i(var3.sector);
                                Vector3f var19 = new Vector3f();
                                Vector3f var9 = new Vector3f();
                                Vector3f var10 = new Vector3f((float)(var18.min.x << 5), (float)(var18.min.y << 5), (float)(var18.min.z << 5));
                                Vector3f var11 = new Vector3f((float)(var18.max.x << 5), (float)(var18.max.y << 5), (float)(var18.max.z << 5));
                                AabbUtil2.transformAabb(var10, var11, 40.0F, var4, var19, var9);
                                var2.add(new BoundingBox(var19, var9));
                                synchronized(GameServerState.this.getBluePrintsToSpawn()) {
                                    GameServerState.this.getBluePrintsToSpawn().add(var18);
                                }
                            } catch (EntityNotFountException var13) {
                                var13.printStackTrace();
                            } catch (IOException var14) {
                                var14.printStackTrace();
                            } catch (EntityAlreadyExistsException var15) {
                                var15.printStackTrace();
                            }
                        } else {
                            System.err.println("[ADMIN] ERROR: Not a valid name: " + var7);
                        }

                        ++var6;
                    }
                }

                return;
            }
        }

        public void spawnMobs(int var1, String var2, Vector3i var3, Transform var4, int var5, BluePrintController var6) {
            GameServerState.MobSpawnThread.MobSpawnRequest var8 = new GameServerState.MobSpawnThread.MobSpawnRequest(var1, var2, var3, var4, var5, var6);
            synchronized(this.requests) {
                this.requests.add(var8);
                this.requests.notify();
            }
        }

        public void shutdown() {
            this.shutdown = true;
            synchronized(this.requests) {
                this.requests.notifyAll();
            }
        }

        class MobSpawnRequest {
            final int count;
            final String catalogname;
            final Vector3i sector;
            final Transform transform;
            final int factionId;
            final BluePrintController bbc;

            public MobSpawnRequest(int var2, String var3, Vector3i var4, Transform var5, int var6, BluePrintController var7) {
                this.count = var2;
                this.catalogname = var3;
                this.sector = var4;
                this.transform = var5;
                this.factionId = var6;
                this.bbc = var7;
            }
        }
    }

    public class FileRequest {
        public ClientChannel channel;
        public String req;

        public FileRequest(ClientChannel var2, String var3) {
            this.channel = var2;
            this.req = var3;
        }
    }
}
