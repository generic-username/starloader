//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.schema.game.client.data.gamemap;

import api.listener.events.draw.GameMapClientUpdateEntriesEvent;
import api.mod.StarLoader;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.gamemap.entry.MapEntryInterface;

@Deprecated
public abstract class GameMap {
    public static final int STATUS_UNLOADED = 0;
    public static final int STATUS_LOADING = 1;
    public static final int STATUS_LOADED = 2;
    public static final byte TYPE_REGION = 1;
    public static final byte TYPE_SYSTEM = 2;
    public static final byte TYPE_SECTOR = 3;
    public static final byte TYPE_SYSTEM_SIMPLE = 4;
    protected MapEntryInterface parent;
    protected int loadStatus;
    private Vector3i pos;
    private MapEntryInterface[] entries;

    public GameMap() {
    }

    public MapEntryInterface[] getEntries() {
        return this.entries;
    }

    public void setEntries(MapEntryInterface[] var1) {
        this.entries = var1;
    }

    public Vector3i getPos() {
        return this.pos;
    }

    public void setPos(Vector3i var1) {
        this.pos = var1;
    }

    public void update(MapEntryInterface[] var1) {
        this.entries = var1;
        //INSERTED CODE
        GameMapClientUpdateEntriesEvent event = new GameMapClientUpdateEntriesEvent(this, var1);
        StarLoader.fireEvent(event, false);
        //Update entry list
        this.entries = event.getEntryArray().toArray(new MapEntryInterface[0]);
        ///
    }
}
