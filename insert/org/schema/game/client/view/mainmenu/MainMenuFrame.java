//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.schema.game.client.view.mainmenu;

import api.SMModLoader;
import api.utils.GameRestartHelper;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.schema.common.ParseException;
import org.schema.common.util.data.DataUtil;
import org.schema.game.client.controller.GameMainMenuController;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.controller.tutorial.newtut.TutorialController;
import org.schema.game.common.LanguageManager;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.*;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.movie.MoviePlayer;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderModifyInterface;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.graphicsengine.util.timer.SinusTimerUtil;
import org.schema.schine.input.Keyboard;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.ResourceLoadEntry;
import org.schema.schine.resource.ResourceLoader;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;

@Deprecated
public class MainMenuFrame implements GraphicsFrame {
    private final GameMainMenuController state;
    private MainMenuGUI gui;
    private SinusTimerUtil sinus0 = new SinusTimerUtil(1.2F);
    private SinusTimerUtil sinus1 = new SinusTimerUtil(1.0F);
    private Shader bgShader;
    private int displayListQuad = -1;
    private int bgWidth;
    private int bgHeight;
    private boolean init;
    private MoviePlayer player;
    long started = System.nanoTime();
    long startedLastSecond = System.nanoTime();
    int videoFramesLastSecond = 0;
    int renderFramesLastSecond = 0;

    public MainMenuFrame(GameMainMenuController var1) {
        this.state = var1;
        this.gui = new MainMenuGUI(var1);
    }

    public void switchLanguage(final String var1) {
        Controller.getResLoader().enqueueWithResetForced(new ResourceLoadEntry[]{new ResourceLoadEntry("Purging Fonts", 4) {
            public void load(ResourceLoader var1) throws ResourceException, IOException {
                FontLibrary.purge();
            }
        }});
        Controller.getResLoader().enqueueWithResetForced(new ResourceLoadEntry[]{new ResourceLoadEntry("Load Language", 4) {
            public void load(ResourceLoader var1x) throws ResourceException, IOException {
                LanguageManager.loadLanguage(var1.toLowerCase(Locale.ENGLISH), false);
            }
        }});
        Controller.getResLoader().enqueueWithResetForced(new ResourceLoadEntry[]{new ResourceLoadEntry("Reinitialize Fonts", 4) {
            public void load(ResourceLoader var1) throws ResourceException, IOException {
                FontLibrary.initialize();
            }
        }});
        Controller.getResLoader().enqueueWithResetForced(new ResourceLoadEntry[]{new ResourceLoadEntry("Reload Main Menu", 4) {
            public void load(ResourceLoader var1) throws ResourceException, IOException {
                MainMenuFrame.this.gui.cleanUp();
                MainMenuFrame.this.gui = new MainMenuGUI(MainMenuFrame.this.state);
            }
        }});
    }

    public void doFrameAndUpdate(GraphicsContext var1) throws RuntimeException {
        if (!this.init) {
            this.onInit();
        }

        if (TutorialController.running != null) {
            try {
                TutorialController.running.cleanUp();
            } catch (Exception var2) {
                var2.printStackTrace();
            }

            TutorialController.running = null;
        }

        if (this.player != null && !this.player.isEnded()) {
            if (Keyboard.isKeyDown(1) || Keyboard.isKeyDown(28) || Keyboard.isKeyDown(57)) {
                this.player.setEnded(true);
            }
            //INSERTED CODE
            if (SMModLoader.shouldUplink) {
                System.err.println("Title screen drawn; uplinking game");
                this.player.setEnded(true);
                if(SMModLoader.uplinkServerHost.equals("localhost")) {
                    GameRestartHelper.startLocalWorld();
                }else{
                    GameRestartHelper.startOnlineWorld(SMModLoader.uplinkServerHost, SMModLoader.uplinkServerPort);
                }
            }
            ///

            this.movieTest();
        } else {
            this.drawBg();
            this.gui.draw();
        }

        this.state.update();
    }

    private void onInit() {
        if (EngineSettings.PLAY_INTRO.isOn()) {
            this.movieInit();
        }

        this.init = true;
    }

    public void movieInit() {
        FileExt var1 = new FileExt("./data/video/SchineSplashScreen.mp4");

        try {
            this.player = new MoviePlayer(var1);
            if (!EngineSettings.S_SOUND_SYS_ENABLED.isOn() || !EngineSettings.USE_OPEN_AL_SOUND.isOn()) {
                this.player.movie.onMissingAudio();
            }

        } catch (IOException var2) {
            var2.printStackTrace();
        }
    }

    public void movieTest() {
        this.player.tick();
        GL11.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
        GL11.glClear(16384);
        GL11.glMatrixMode(5889);
        GL11.glLoadIdentity();
        GL11.glOrtho(0.0D, (double) GLFrame.getWidth(), (double) GLFrame.getHeight(), 0.0D, -1.0D, 1.0D);
        GL11.glMatrixMode(5888);
        GL11.glLoadIdentity();
        GlUtil.glEnable(3553);
        if (this.player.syncTexture(5, this.state.graphicsContext.timer)) {
            System.nanoTime();
            GlUtil.glEnable(3042);
            GlUtil.glBlendFunc(770, 771);
            this.player.movie.height();
            this.player.movie.width();
            GL11.glPushMatrix();
            GL11.glBegin(7);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            float var1 = (float) GLFrame.getWidth() / (float) this.player.movie.width();
            float var2 = (float) GLFrame.getHeight() / (float) this.player.movie.height();
            var1 = Math.min(var1, var2);
            var2 = (float) this.player.movie.width() * var1;
            var1 = (float) this.player.movie.height() * var1;
            float var3 = ((float) GLFrame.getWidth() - var2) * 0.5F;
            float var4 = ((float) GLFrame.getHeight() - var1) * 0.5F;
            GL11.glTexCoord2f(0.0F, 0.0F);
            GL11.glVertex3f(var3 + var2 * 0.0F, var4 + var1 * 0.0F, 0.0F);
            GL11.glTexCoord2f(1.0F, 0.0F);
            GL11.glVertex3f(var3 + var2, var4 + var1 * 0.0F, 0.0F);
            GL11.glTexCoord2f(1.0F, 1.0F);
            GL11.glVertex3f(var3 + var2, var4 + var1, 0.0F);
            GL11.glTexCoord2f(0.0F, 1.0F);
            GL11.glVertex3f(var3 + var2 * 0.0F, var4 + var1, 0.0F);
            GL11.glEnd();
            GlUtil.glDisable(3042);
            GlUtil.glDisable(3553);
            GL11.glPopMatrix();
            System.nanoTime();
            ++this.renderFramesLastSecond;
            if (System.nanoTime() > this.startedLastSecond + 1000000000L) {
                this.startedLastSecond += 1000000000L;
                this.renderFramesLastSecond = 0;
                this.videoFramesLastSecond = this.player.textureUpdateTook.addCount();
            }

        }
    }

    private void drawBg() {
        if (this.bgShader != null) {
            GlUtil.glDisable(2884);
            GlUtil.glMatrixMode(5889);
            GlUtil.glPushMatrix();
            GlUtil.glLoadIdentity();
            GLU.gluOrtho2D(-1.0F, 1.0F, 1.0F, -1.0F);
            GlUtil.glMatrixMode(5888);
            GlUtil.glPushMatrix();
            GlUtil.glLoadIdentity();
            GlUtil.glEnable(3553);
            this.bgShader.load();
            GlUtil.glPushMatrix();
            this.renderQuad();
            GlUtil.glPopMatrix();
            this.bgShader.unload();
            GlUtil.glPopMatrix();
            GlUtil.glDisable(2903);
            GlUtil.glBindTexture(3553, 0);
            GlUtil.glEnable(2929);
            GlUtil.glMatrixMode(5889);
            GlUtil.glPopMatrix();
            GlUtil.glMatrixMode(5888);
        }
    }

    public void handleException(Exception var1) {
        System.err.println("[GLFRAME] THROWN: " + var1.getClass() + " Now Printing StackTrace");
        var1.printStackTrace();
        this.handleError(var1);
    }

    public void onEndLoop(GraphicsContext var1) {
        try {
            Controller.cleanUpAllBuffers();
            Display.destroy();
            if (Controller.getAudioManager().isLoaded()) {
                Controller.getAudioManager().onCleanUp();
            }

            try {
                throw new Exception("System.exit() called");
            } catch (Exception var2) {
                var2.printStackTrace();
                System.exit(0);
            }
        } catch (Exception var3) {
            var3.printStackTrace();
        }
    }

    public void afterFrame() {
    }

    private void renderQuad() {
        if (this.bgWidth != GLFrame.getWidth() || this.bgHeight != GLFrame.getHeight()) {
            if (this.displayListQuad != -1) {
                GL11.glDeleteLists(this.displayListQuad, 1);
            }

            this.bgWidth = GLFrame.getWidth();
            this.bgHeight = GLFrame.getHeight();
            this.displayListQuad = GL11.glGenLists(1);
            GL11.glNewList(this.displayListQuad, 4864);
            GL11.glBegin(7);
            GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glTexCoord2f(0.0F, 0.0F);
            GL11.glVertex3f(-1.0F, 1.0F, 0.0F);
            GL11.glTexCoord2f(1.0F, 0.0F);
            GL11.glVertex3f(-1.0F, -1.0F, 0.0F);
            GL11.glTexCoord2f(1.0F, 1.0F);
            GL11.glVertex3f(1.0F, -1.0F, 0.0F);
            GL11.glTexCoord2f(0.0F, 1.0F);
            GL11.glVertex3f(1.0F, 1.0F, 0.0F);
            GL11.glEnd();
            GL11.glEndList();
        }

        GL11.glCallList(this.displayListQuad);
    }

    public void enqueueFrameResources() throws FileNotFoundException, ResourceException, ParseException, SAXException, IOException, ParserConfigurationException {
        Controller.getResLoader().enqueueWithReset(new ResourceLoadEntry[]{new ResourceLoadEntry("ServerSettings", 4) {
            public void load(ResourceLoader var1) throws ResourceException, IOException {
                ServerConfig.read();
            }
        }});
        Controller.getResLoader().enqueueWithReset(new ResourceLoadEntry[]{new ResourceLoadEntry("MainMenuBackgroundShader", 4) {
            public void load(ResourceLoader var1) throws ResourceException, IOException {
                try {
                    MainMenuFrame.this.bgShader = new Shader(DataUtil.dataPath + "/shader/loadingscreen/bg.vert.glsl", DataUtil.dataPath + "/shader/loadingscreen/bg.frag.glsl", new ShaderModifyInterface[0]);
                    MainMenuFrame.this.bgShader.setShaderInterface(MainMenuFrame.this.new BackgroundShader());
                } catch (Exception var2) {
                    var2.printStackTrace();
                }
            }
        }});
        Controller.getResLoader().enqueueImageResources();
        Controller.getResLoader().enqueueConfigResources("GuiConfigMainMenu.xml", false);
    }

    public void update(Timer var1) {
        this.gui.update(var1);
        this.sinus0.update(var1);
        this.sinus1.update(var1);
    }

    public void setFinishedFrame(boolean var1) {
        GraphicsContext.setFinished(true);
    }

    public void handleError(Exception var1) {
        String var2 = var1.getMessage() != null && var1.getMessage().length() > 0 ? var1.getMessage() : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_MAINMENUFRAME_0;
        var2 = var2 + "\n\n";
        if (!var1.getClass().getSimpleName().contains("Disconnect") && !var2.contains("you are banned from this server")) {
            var2 = var2 + Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_MAINMENUFRAME_3;
        }

        try {
            PlayerOkCancelInput var4;
            (var4 = new PlayerOkCancelInput("ERROR", this.state, 500, 300, var1.getClass().getSimpleName(), var2) {
                public void pressedOK() {
                    System.err.println("[GUI] PRESSED OK ON ERROR");
                    this.deactivate();
                }

                public void onDeactivate() {
                }

                public void cancel() {
                    PlayerOkCancelInput var1 = new PlayerOkCancelInput("CONFIRM", MainMenuFrame.this.state, 200, 100, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_MAINMENUFRAME_1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_MAINMENUFRAME_2) {
                        public void pressedOK() {
                            GLFrame.setFinished(true);
                            this.deactivate();
                        }

                        public void onDeactivate() {
                        }
                    };
                    this.deactivate();
                    var1.activate();
                }
            }).getInputPanel().onInit();
            var4.getInputPanel().setOkButtonText("CONTINUE");
            var4.getInputPanel().setCancelButtonText("TO DESKTOP");
            var4.activate();
        } catch (Exception var3) {
            var3.printStackTrace();
            System.err.println("ORIGINAL EXCEPTION: ");
            var1.printStackTrace();
        }
    }

    public boolean synchByFrame() {
        Display.sync(60);
        return true;
    }

    public void queueException(RuntimeException var1) {
        this.state.handleError(var1);
    }

    class BackgroundShader implements Shaderable {
        private BackgroundShader() {
        }

        public void onExit() {
            Controller.getResLoader().getSprite("loadingscreen-background").getMaterial().getTexture().detach();
        }

        public void updateShader(DrawableScene var1) {
        }

        public void updateShaderParameters(Shader var1) {
            Controller.getResLoader().getSprite("loadingscreen-background").getMaterial().getTexture().attach(0);
            GlUtil.updateShaderInt(var1, "tex", 0);
            GlUtil.updateShaderVector2f(var1, "res", (float) GLFrame.getWidth(), (float) GLFrame.getHeight());
            GlUtil.updateShaderVector2f(var1, "dir", MainMenuFrame.this.sinus0.getTime() * 5.0F, MainMenuFrame.this.sinus1.getTime() * 5.0F);
            GlUtil.updateShaderVector2f(var1, "dir", MainMenuFrame.this.sinus0.getTime() * 5.0F, MainMenuFrame.this.sinus1.getTime() * 5.0F);
        }
    }
}
