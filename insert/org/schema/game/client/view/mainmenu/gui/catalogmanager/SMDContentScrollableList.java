package org.schema.game.client.view.mainmenu.gui.catalogmanager;

import api.smd.SMDUtils;
import api.smd.SMDEntryData;
import api.smd.SMDEntryExtraData;
import api.smd.SMDEntryUtils;
import api.utils.gui.SimplePopup;
import org.apache.commons.io.FileUtils;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.CompareTools;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.controller.ImportFailedException;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.*;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.*;

public class SMDContentScrollableList extends ScrollableTableList<SMDEntryData> {

    private List<SMDEntryData> entries;
    public boolean updated;
    private FilterType currentFilter;

    public SMDContentScrollableList(InputState state, GUIElement element) {
        super(state, 750, 500, element);
        currentFilter = FilterType.ALL;
        updated = false;
        updateEntries();
    }

    @Override
    public void initColumns() {
        new StringComparator();

        this.addColumn("Name", 18.0F, new Comparator<SMDEntryData>() {
            public int compare(SMDEntryData o1, SMDEntryData o2) {
                return o1.title.toLowerCase().compareTo(o2.title.toLowerCase());
            }
        });

        this.addColumn("Author", 7.5F, new Comparator<SMDEntryData>() {
            public int compare(SMDEntryData o1, SMDEntryData o2) {
                return o1.username.toLowerCase().compareTo(o2.username.toLowerCase());
            }
        });

        this.addColumn("Rating", 5.0F, new Comparator<SMDEntryData>() {
            public int compare(SMDEntryData o1, SMDEntryData o2) {
                return CompareTools.compare(o1.rating_average, o2.rating_average);
            }
        });

        this.addColumn("Downloads", 7.0F, new Comparator<SMDEntryData>() {
            public int compare(SMDEntryData o1, SMDEntryData o2) {
                return CompareTools.compare(o1.downloadCount, o2.downloadCount);
            }
        });

        this.addTextFilter(new GUIListFilterText<SMDEntryData>() {
            public boolean isOk(String s, SMDEntryData entry) {
                return entry.title.toLowerCase().contains(s.toLowerCase());
            }
        }, "SEARCH BY NAME", ControllerElement.FilterRowStyle.LEFT);

        this.addTextFilter(new GUIListFilterText<SMDEntryData>() {
            public boolean isOk(String s, SMDEntryData entry) {
                return entry.username.toLowerCase().contains(s.toLowerCase());
            }
        }, "SEARCH BY AUTHOR", ControllerElement.FilterRowStyle.RIGHT);

        this.activeSortColumnIndex = 0;
    }

    @Override
    protected Collection<SMDEntryData> getElementList() {
        if(!updated) updateEntries();
        return entries;
    }

    public void setCurrentFilter(FilterType currentFilter) {
        this.currentFilter = currentFilter;
        updateEntries();
    }

    public void updateEntries() {
        this.entries = new ArrayList<>();
        if(currentFilter.equals(FilterType.ALL)) {
            this.entries.addAll(SMDEntryUtils.getAllBlueprints().values());
            this.entries.addAll(SMDEntryUtils.getAllTemplates().values());
        } else {
            for(SMDEntryData entry : SMDEntryUtils.getAllBlueprints().values()) {
                if(entry.resource_category == currentFilter.num) this.entries.add(entry);
            }
            for(SMDEntryData entry : SMDEntryUtils.getAllTemplates().values()) {
                if(entry.resource_category == currentFilter.num) this.entries.add(entry);
            }
        }

        flagDirty();
        updated = true;
    }


    /**
     * Download a blueprint or template from starmade dock. Cannot download texture packs, mods, etc.
     *
     * @param resId The SMD resource id. Example: https://starmadedock.net/content/gsi-200m-warp-gate.8221/ has id 8221
     */
    public static void downloadSMDContent(int resId) throws IOException, ImportFailedException {
//        InputStream stream = SMDUtils.GET(downloadURL).getInputStream();
//        File downloadsFolder = new File()
//        File downloadFile = new File(downloadsFolder.getAbsolutePath() + "/" + sanitizeName(entry.title) + ".sment");
//        FileUtils.copyInputStreamToFile(stream, downloadFile);
//        BluePrintController.active.importFile(downloadFile, null);
//        CatalogManagerPanel.updateLists();
    }

    @Override
    public void updateListEntries(GUIElementList list, Set<SMDEntryData> set) {
        if(!updated) updateEntries();
        for(final SMDEntryData entry : set) {
            GUITextOverlayTable nameTextElement;
            (nameTextElement = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(entry.title);
            GUIClippedRow nameRowElement;
            (nameRowElement = new GUIClippedRow(this.getState())).attach(nameTextElement);

            GUITextOverlayTable authorTextElement;
            (authorTextElement = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(entry.username);
            GUIClippedRow authorRowElement;
            (authorRowElement = new GUIClippedRow(this.getState())).attach(authorTextElement);

            GUITextOverlayTable ratingTextElement;
            (ratingTextElement = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(StringTools.formatPointZero(entry.rating_average) + " stars");
            GUIClippedRow ratingRowElement;
            (ratingRowElement = new GUIClippedRow(this.getState())).attach(ratingTextElement);

            GUITextOverlayTable downloadsTextElement;
            (downloadsTextElement = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(entry.downloadCount + " total downloads");
            GUIClippedRow downloadsRowElement;
            (downloadsRowElement = new GUIClippedRow(this.getState())).attach(downloadsTextElement);

            final SMDContentListRow entryListRow = new SMDContentListRow(this.getState(), entry, nameRowElement, authorRowElement, ratingRowElement, downloadsRowElement);

            entryListRow.expanded = new GUIElementList(getState());

            GUIAncor buttonPane = new GUIAncor(getState(), 150, 28);

            GUITextButton downloadButton = new GUITextButton(getState(), 150, 24, GUITextButton.ColorPalette.OK, "DOWNLOAD", new GUICallback() {
                @Override
                public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                    if(mouseEvent.pressedLeftMouse()) {
                        if(entryListRow.f != null) {
                            getState().getController().queueUIAudio("0022_menu_ui - enter");
                            PlayerOkCancelInput confirmBox = new PlayerOkCancelInput("CONFIRM", getState(), 300, 150,"Confirm Download", "Do you wish to download " + entry.title + "?") {
                                @Override
                                public void pressedOK() {
                                    final ArrayList<String> names = new ArrayList<>();
                                    final File templatesFolder = new File("templates");
                                    List<BlueprintEntry> blueprints = BluePrintController.active.readBluePrints();

                                    for(BlueprintEntry BP : blueprints) {
                                        names.add(BP.getName());
                                    }
                                    if (!templatesFolder.exists()) templatesFolder.mkdirs();
                                    if (templatesFolder.listFiles() != null) {
                                        for (File templateFile : templatesFolder.listFiles()) {
                                            names.add(templateFile.getName().substring(0, templateFile.getName().indexOf(".smtpl")));
                                        }
                                    }

                                    try {
                                        SMDEntryExtraData extraData = entry.getExtraData();
                                        String downloadURL = extraData.getDownloadURL();
                                        downloadURL = downloadURL.substring("https://starmadedock.net/api/".length());
                                        InputStream stream = SMDUtils.GETFromSMD(downloadURL).getInputStream();
                                        if(FilterType.getContentType(entry.resource_category) == 1) {
                                            if(extraData.getFileName().toLowerCase(Locale.ENGLISH).endsWith(".sment")) {
                                                File downloadsFolder = new File("download-cache");
                                                if (!downloadsFolder.exists()) downloadsFolder.mkdirs();
                                                File downloadFile = new File(downloadsFolder.getAbsolutePath() + "/" + SMDUtils.sanitizeName(entry.title) + ".sment");
                                                FileUtils.copyInputStreamToFile(stream, downloadFile);
                                                BluePrintController.active.importFile(downloadFile, null);
                                                FileUtils.forceDelete(downloadFile);
                                                FileUtils.forceDelete(downloadsFolder);
                                                CatalogManagerPanel.updateLists();
                                                updated = false;
                                                updateEntries();
                                                new SimplePopup(getState(), "Successfully downloaded blueprint.");
                                            }else{
                                                new SimplePopup(getState(), "Error", "Error: this is not an .sment file, and so it cannot be downloaded.");
                                            }
                                        } else if(FilterType.getContentType(entry.resource_category) == 2) {
                                            if(extraData.getFileName().toLowerCase(Locale.ENGLISH).endsWith(".smtpl")) {
                                                File downloadsFolder = new File("download-cache");
                                                if (!downloadsFolder.exists()) downloadsFolder.mkdirs();
                                                final String title =  SMDUtils.sanitizeName(entry.title);
                                                File downloadFile = new File(downloadsFolder.getAbsolutePath() + "/" + title + ".smtpl");
                                                FileUtils.copyInputStreamToFile(stream, downloadFile);
                                                FileUtils.copyFile(downloadFile, new File(templatesFolder.getAbsolutePath() + "/" + title + ".smtpl"));
                                                FileUtils.forceDelete(downloadFile);
                                                FileUtils.forceDelete(downloadsFolder);
                                                new SimplePopup(getState(), "Successfully downloaded template.");
                                                CatalogManagerPanel.updateLists();
                                                updated = false;
                                                updateEntries();
                                            }else{
                                                new SimplePopup(getState(), "Error", "Error: this is not a .smtpl file, and so it cannot be downloaded.");
                                            }
                                        } else {
                                            PlayerOkCancelInput errorBox = new PlayerOkCancelInput("CONFIRM", getState(), 300, 150, "Error", "Could not download " + entry.title + " due to an unexpected error.") {
                                                @Override
                                                public void onDeactivate() {

                                                }

                                                @Override
                                                public void pressedOK() {
                                                    deactivate();
                                                }
                                            };
                                            errorBox.getInputPanel().onInit();
                                            errorBox.getInputPanel().setCancelButton(false);
                                            errorBox.getInputPanel().background.setPos(470.0F, 35.0F, 0.0F);
                                            errorBox.getInputPanel().background.setWidth((float)(GLFrame.getWidth() - 435));
                                            errorBox.getInputPanel().background.setHeight((float)(GLFrame.getHeight() - 70));
                                            errorBox.activate();
                                        }

                                    } catch (IOException | ImportFailedException e) {
                                        e.printStackTrace();

                                        PlayerOkCancelInput errorBox = new PlayerOkCancelInput("CONFIRM", getState(), 300, 150, "Error", "Could not download " + entry.title + " due to an unexpected error.") {
                                            @Override
                                            public void onDeactivate() {

                                            }

                                            @Override
                                            public void pressedOK() {
                                                deactivate();
                                            }
                                        };
                                        errorBox.getInputPanel().onInit();
                                        errorBox.getInputPanel().setCancelButton(false);
                                        errorBox.getInputPanel().background.setPos(470.0F, 35.0F, 0.0F);
                                        errorBox.getInputPanel().background.setWidth((float)(GLFrame.getWidth() - 435));
                                        errorBox.getInputPanel().background.setHeight((float)(GLFrame.getHeight() - 70));
                                        errorBox.activate();
                                    } catch (NullPointerException e){
                                        e.printStackTrace();
                                        new SimplePopup(getState(), "Error", "Could not download, the file attached is not a .sment or .smtpl");
                                    }
                                    updated = false;
                                    updateEntries();
                                    deactivate();
                                }

                                @Override
                                public void onDeactivate() {
                                }
                            };
                            confirmBox.getInputPanel().onInit();
                            confirmBox.getInputPanel().background.setPos(470.0F, 35.0F, 0.0F);
                            confirmBox.getInputPanel().background.setWidth((float)(GLFrame.getWidth() - 435));
                            confirmBox.getInputPanel().background.setHeight((float)(GLFrame.getHeight() - 70));
                            confirmBox.activate();
                        }
                    }
                }

                @Override
                public boolean isOccluded() {
                    return !isActive();
                }
            });
            GUITextButton viewButton = new GUITextButton(getState(), 150, 24, GUITextButton.ColorPalette.OK, "View on Dock", new GUICallback() {
                @Override
                public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                    if (mouseEvent.pressedLeftMouse()) {
                        SMDEntryData data = entryListRow.f;
                        try {
                            Desktop.getDesktop().browse(new URL("http://starmadedock.net/content/"+data.resource_id+"/").toURI());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public boolean isOccluded() {
                    return false;
                }
            });
            buttonPane.attach(viewButton);
            buttonPane.attach(downloadButton);
            downloadButton.setPos(0, 0, 0);
            viewButton.setPos(160, 0, 0);
            buttonPane.setPos(entryListRow.expanded.getPos());
            entryListRow.expanded.add(new GUIListElement(buttonPane, buttonPane, getState()));
            entryListRow.expanded.attach(buttonPane);
            entryListRow.onInit();
            list.addWithoutUpdate(entryListRow);
        }

        list.updateDim();
    }

    public class SMDContentListRow extends ScrollableTableList<SMDEntryData>.Row {
        public SMDContentListRow(InputState inputState, SMDEntryData entry, GUIElement... guiElements) {
            super(inputState, entry, guiElements);
            this.highlightSelect = true;
            this.highlightSelectSimple = true;
            this.setAllwaysOneSelected(true);
        }
    }
}
