//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.schema.game.client.view.gui.shiphud.newhud;

import com.bulletphysics.linearmath.Transform;
import org.lwjgl.opengl.GL11;
import org.schema.common.FastMath;
import org.schema.game.client.controller.manager.ingame.navigation.NavigationControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.MinimapMode;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.util.Iterator;

@Deprecated
public class Radar extends GUIElement {
    private static final float TITLE_DRAWN_OPAQUE = 4.0F;
    private static final float TITLE_DRAWN_BLEND = 1.3F;
    private static final float TITLE_DRAWN_TOTAL = 5.3F;
    Transform t = new Transform();
    private GameClientState state;
    private GUITextOverlay location;
    private float displayed;
    private Transform inv = new Transform();
    private Vector3f c = new Vector3f();
    private GUIOverlay panel;
    private GUIOverlay panelLarge;
    private int listSmall;
    private int listLarge;
    private Vector4f tint = new Vector4f();
    private GUITextOverlay secString;
    //INSERTED CODE
    public GUITextOverlay getLocation() {
        return location;
    }
    ///

    public Radar(InputState var1) {
        super(var1);
        this.state = (GameClientState)var1;
    }

    public void cleanUp() {
    }

    public void draw() {
        if (EngineSettings.MINIMAP_MODE.getCurrentState() != MinimapMode.OFF) {
            GlUtil.glPushMatrix();
            this.transform();
            GUIOverlay var1;
            if (EngineSettings.MINIMAP_MODE.getCurrentState() == MinimapMode.SMALL) {
                this.panel.draw();
                this.drawCircleHUD(false);
                var1 = this.panel;
            } else {
                this.panelLarge.draw();
                this.drawCircleHUD(true);
                var1 = this.panelLarge;
            }

            this.secString.getPos().x = (float)((int)(var1.getWidth() / 2.0F - (float)(this.secString.getMaxLineWidth() / 2)));
            this.secString.getPos().y = var1.getHeight() + -18.0F;
            int var2 = 0;
            if ((float)((int)(var1.getWidth() / 2.0F + (float)(this.location.getMaxLineWidth() / 2))) > var1.getWidth()) {
                var2 = (int)var1.getWidth() - (int)(var1.getWidth() / 2.0F + (float)(this.location.getMaxLineWidth() / 2));
            }

            this.location.getPos().x = (float)((int)(var1.getWidth() / 2.0F - (float)(this.location.getMaxLineWidth() / 2)) + var2);
            this.location.getPos().y = var1.getHeight() + 18.0F + -18.0F;
            if (this.displayed < 5.3F) {
                if (this.displayed > 4.0F) {
                    float var4 = (this.displayed - 4.0F) / 1.3F;
                    this.secString.getColor().a = 1.0F - var4;
                } else {
                    this.secString.getColor().a = 1.0F;
                }

                this.secString.draw();
            } else {
                this.secString.getColor().a = 0.0F;
            }

            Iterator var3 = this.getChilds().iterator();

            while(var3.hasNext()) {
                ((AbstractSceneNode)var3.next()).draw();
            }

            GlUtil.glPopMatrix();
        }
    }

    public void onInit() {
        this.panel = new GUIOverlay(Controller.getResLoader().getSprite("HUD_Map-2x1-gui-"), this.getState());
        this.panelLarge = new GUIOverlay(Controller.getResLoader().getSprite("HUD_MapLarge-2x1-gui-"), this.getState());
        this.secString = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium16(), this.getState());
        this.secString.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_RADAR_0);
        this.location = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium16(), this.getState());
        this.location.setTextSimple(new Object() {
            public String toString() {
                if (((GameClientState)Radar.this.getState()).getPlayer().isInTutorial()) {
                    return "<Tutorial>";
                } else if (((GameClientState)Radar.this.getState()).getPlayer().isInPersonalSector()) {
                    return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_RADAR_1;
                } else {
                    return ((GameClientState)Radar.this.getState()).getPlayer().isInTestSector() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_RADAR_2 : ((GameClientState)Radar.this.getState()).getPlayer().getCurrentSector().toStringPure();
                }
            }
        });
        this.attach(this.secString);
        this.attach(this.location);
    }

    private int createBgCircles(float var1) {
        int var2;
        GL11.glNewList(var2 = GL11.glGenLists(1), 4864);
        GL11.glBegin(3);
        GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 0.5F);

        float var3;
        for(var3 = 0.0F; var3 < 6.2831855F; var3 += 0.1308997F) {
            GL11.glVertex3f(FastMath.cos(var3) * var1, 0.0F, FastMath.sin(var3) * var1);
        }

        GL11.glVertex3f(FastMath.cos(0.0F) * var1, 0.0F, FastMath.sin(0.0F) * var1);
        GL11.glEnd();
        GL11.glBegin(3);
        GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 0.1F);

        for(var3 = 0.0F; var3 < 6.2831855F; var3 += 0.1308997F) {
            GL11.glVertex3f(FastMath.cos(var3) * var1 * 0.85F, 0.0F, FastMath.sin(var3) * var1 * 0.85F);
        }

        GL11.glVertex3f(FastMath.cos(0.0F) * var1 * 0.85F, 0.0F, FastMath.sin(0.0F) * var1 * 0.85F);
        GL11.glEnd();
        GL11.glBegin(3);
        GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 0.1F);

        for(var3 = 0.0F; var3 < 6.2831855F; var3 += 0.1308997F) {
            GL11.glVertex3f(FastMath.cos(var3) * var1 * 0.6F, 0.0F, FastMath.sin(var3) * var1 * 0.6F);
        }

        GL11.glVertex3f(FastMath.cos(0.0F) * var1 * 0.6F, 0.0F, FastMath.sin(0.0F) * var1 * 0.6F);
        GL11.glEnd();
        GL11.glBegin(3);
        GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 0.1F);

        for(var3 = 0.0F; var3 < 6.2831855F; var3 += 0.1308997F) {
            GL11.glVertex3f(FastMath.cos(var3) * var1 * 0.3F, 0.0F, FastMath.sin(var3) * var1 * 0.3F);
        }

        GL11.glVertex3f(FastMath.cos(0.0F) * var1 * 0.3F, 0.0F, FastMath.sin(0.0F) * var1 * 0.3F);
        GL11.glEnd();
        GL11.glEndList();
        return var2;
    }

    private void drawBgCircles(boolean var1) {
        if (var1) {
            if (this.listLarge != 0) {
                GL11.glCallList(this.listLarge);
            } else {
                this.listLarge = this.createBgCircles(120.0F);
            }
        } else if (this.listSmall != 0) {
            GL11.glCallList(this.listSmall);
        } else {
            this.listSmall = this.createBgCircles(60.0F);
        }
    }

    private void drawCircleHUD(boolean var1) {
        if (this.state.getCurrentPlayerObject() != null) {
            GlUtil.glEnable(3042);
            GL11.glLineWidth(1.0F);
            GlUtil.glBlendFunc(770, 771);
            GlUtil.glEnable(2848);
            GL11.glHint(3154, 4354);
            GlUtil.glPushMatrix();
            Transform var2 = new Transform();
            Controller.getMat(Controller.modelviewMatrix, var2);
            GUIElement.enableOrthogonal3d();
            this.t.setIdentity();
            this.t.basis.rotX(0.368F);
            GlUtil.glMultMatrix(var2);
            GlUtil.translateModelview(this.getWidth() / 2.0F, this.getHeight() / 2.0F, 0.0F);
            GlUtil.glMultMatrix(this.t);
            GlUtil.glDisable(2929);
            this.drawBgCircles(var1);
            SimpleTransformableSendableObject var6 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity();
            if (this.state.getCurrentPlayerObject() instanceof PlayerCharacter) {
                this.t.setIdentity();
                ((PlayerCharacter)this.state.getCurrentPlayerObject()).getOwnerState().getWordTransform(this.t);
                this.t.origin.set(this.state.getCurrentPlayerObject().getWorldTransform().origin);
            } else {
                this.t.set(this.state.getCurrentPlayerObject().getWorldTransform());
            }

            this.t.inverse();
            float var5;
            if (var1) {
                var5 = 120.0F;
            } else {
                var5 = 60.0F;
            }

            GL11.glBegin(1);
            Iterator var3 = this.state.getCurrentSectorEntities().values().iterator();

            SimpleTransformableSendableObject var4;
            while(var3.hasNext()) {
                if (NavigationControllerManager.isVisibleRadar(var4 = (SimpleTransformableSendableObject)var3.next())) {
                    this.c.set(var4.getWorldTransformOnClient().origin);
                    this.t.transform(this.c);
                    this.c.scale(0.02F);
                    if (this.c.length() < var5 / 1.2F) {
                        GlUtil.glColor4f(0.3F, 0.3F, 0.7F, 0.5F);
                        GL11.glVertex3f(-this.c.x, -this.c.y, this.c.z);
                        GL11.glVertex3f(-this.c.x, 0.0F, this.c.z);
                        GlUtil.glColor4f(0.5F, 0.5F, 0.5F, 0.5F);
                        GL11.glVertex3f(-this.c.x, 0.0F, this.c.z);
                        GL11.glVertex3f(0.0F, 0.0F, 0.0F);
                    }
                }
            }

            GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glEnd();
            GL11.glPointSize(2.0F);
            GL11.glBegin(0);
            var3 = this.state.getCurrentSectorEntities().values().iterator();

            while(var3.hasNext()) {
                if (NavigationControllerManager.isVisibleRadar(var4 = (SimpleTransformableSendableObject)var3.next())) {
                    HudIndicatorOverlay.getColor(var4, this.tint, var4 == var6, (GameClientState)this.getState());
                    this.tint.w = 0.8F;
                    this.c.set(var4.getWorldTransformOnClient().origin);
                    this.t.transform(this.c);
                    this.c.scale(0.02F);
                    if (this.c.length() < var5 / 1.2F) {
                        GlUtil.glColor4f(this.tint);
                        GL11.glVertex3f(-this.c.x, -this.c.y, this.c.z);
                    }
                }
            }

            GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glEnd();
            GUIElement.disableOrthogonal();
            GlUtil.glPopMatrix();
        }
    }

    public float getHeight() {
        return EngineSettings.MINIMAP_MODE.getCurrentState() == MinimapMode.LARGE ? this.panelLarge.getHeight() : this.panel.getHeight();
    }

    public float getWidth() {
        return EngineSettings.MINIMAP_MODE.getCurrentState() == MinimapMode.LARGE ? this.panelLarge.getWidth() : this.panel.getWidth();
    }

    public boolean isPositionCenter() {
        return false;
    }

    public void update(Timer var1) {
        this.displayed += var1.getDelta();
    }

    public void resetDrawn() {
        this.displayed = 0.0F;
    }
}
