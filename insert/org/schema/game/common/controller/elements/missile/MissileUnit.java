package org.schema.game.common.controller.elements.missile;

import api.listener.events.weapon.AnyWeaponDamageCalculateEvent;
import api.mod.StarLoader;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileElementManager;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.CustomOutputUnit;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

@Deprecated
public abstract class MissileUnit<
        E extends MissileUnit<E, CM, EM>,
        CM extends MissileCollectionManager<E, CM, EM>,
        EM extends MissileElementManager<E, CM, EM>> extends CustomOutputUnit<E, CM, EM> {
    private Vector3i minSig = new Vector3i(0, 0, 0);
    private Vector3i maxSig = new Vector3i(0, 0, 0);

    public int getEffectBonus() {
        return Math.min(this.size(), (int)((double)this.size() / (double)((MissileCollectionManager)this.elementCollectionManager).getTotalSize() * (double)((MissileCollectionManager)this.elementCollectionManager).getEffectTotal()));
    }

    public float getExtraConsume() {
        return 1.0F + (float)Math.max(0, ((MissileCollectionManager)this.elementCollectionManager).getElementCollections().size() - 1) * DumbMissileElementManager.ADDITIONAL_POWER_CONSUMPTION_PER_UNIT_MULT;
    }

    protected DamageDealerType getDamageType() {
        return DamageDealerType.MISSILE;
    }

    public float getBaseConsume() {
        return (float)(this.size() + this.getEffectBonus()) * DumbMissileElementManager.BASE_POWER_CONSUMPTION;
    }

    public float getDamage() {
        float var1 = (float)(this.size() + this.getEffectBonus()) * this.getBaseDamage();

        //INSERTED CODE @...
        float outDamage = this.getConfigManager().apply(StatusEffectType.WEAPON_DAMAGE, this.getDamageType(), var1);
        AnyWeaponDamageCalculateEvent event = new AnyWeaponDamageCalculateEvent(this, outDamage, false);
        StarLoader.fireEvent(event, this.getSegmentController().isOnServer());
        return event.damage;
        ///
    }

    public float getDamageWithoutEffect() {
        return (float)this.size() * this.getBaseDamage();
    }

    public boolean canUse(long var1, boolean var3) {
        if (!((MissileElementManager)((MissileCollectionManager)this.elementCollectionManager).getElementManager()).checkMissileCapacity()) {
            if (var3) {
                this.getSegmentController().popupOwnClientMessage("NO_MISSILE_CAP", Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_MISSILEUNIT_0, 3);
            }

            return false;
        } else {
            return super.canUse(var1, var3);
        }
    }

    public float getSpeed() {
        return ((GameStateInterface)this.getSegmentController().getState()).getGameState().getMaxGalaxySpeed() * DumbMissileElementManager.BASE_SPEED;
    }

    protected void significatorUpdate(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, long var10) {
        long var12 = (long)getPosZ(this.significator);
        long var14 = (long)getPosY(this.significator);
        long var16 = (long)getPosX(this.significator);
        if ((long)var3 > var12) {
            this.minSig.set(var1, var2, var3);
            this.maxSig.set(var1, var2, var3);
            var12 = (long)var9;
        } else if ((long)var1 == var16) {
            this.minSig.set(var1, Math.min(var2, (int)var14), Math.min(var3, (int)var12));
            this.maxSig.set(var1, Math.max(var2, (int)var14), Math.max(var3, (int)var12));
        }

        var14 = (long)(this.maxSig.y - (this.maxSig.y - this.minSig.y) / 2);
        var16 = (long)(this.maxSig.x - (this.maxSig.x - this.minSig.x) / 2);
        this.significator = ElementCollection.getIndex((int)var16, (int)var14, (int)var12);
    }

    public float getBasePowerConsumption() {
        return DumbMissileElementManager.BASE_POWER_CONSUMPTION;
    }

    public float getPowerConsumption() {
        return this.getExtraConsume() * this.getBaseConsume();
    }

    public float getPowerConsumptionWithoutEffect() {
        return (float)this.size() * DumbMissileElementManager.BASE_POWER_CONSUMPTION;
    }

    public float getReloadTimeMs() {
        return DumbMissileElementManager.BASE_RELOAD;
    }

    public float getInitializationTime() {
        return DumbMissileElementManager.BASE_RELOAD;
    }

    public float getDistanceRaw() {
        return DumbMissileElementManager.BASE_DISTANCE * ((GameStateInterface)this.getSegmentController().getState()).getGameState().getWeaponRangeReference();
    }

    public String toString() {
        return "MissileUnit [significator=" + this.significator + "]";
    }

    public float getBaseDamage() {
        return DumbMissileElementManager.BASE_DAMAGE.get(this.getSegmentController().isUsingPowerReactors());
    }

    public float getFiringPower() {
        return this.getDamage();
    }

    public double getPowerConsumedPerSecondRestingPerBlock() {
        double var1 = (double)DumbMissileElementManager.REACTOR_POWER_CONSUMPTION_RESTING;
        return this.getConfigManager().apply(StatusEffectType.WEAPON_TOP_OFF_RATE, this.getDamageType(), var1);
    }

    public double getPowerConsumedPerSecondChargingPerBlock() {
        double var1 = (double)DumbMissileElementManager.REACTOR_POWER_CONSUMPTION_CHARGING;
        return this.getConfigManager().apply(StatusEffectType.WEAPON_CHARGE_RATE, this.getDamageType(), var1);
    }

    public double getPowerConsumedPerSecondResting() {
        return ((MissileElementManager)((MissileCollectionManager)this.elementCollectionManager).getElementManager()).calculatePowerConsumptionCombi(this.getPowerConsumedPerSecondRestingPerBlock(), false, this);
    }

    public double getPowerConsumedPerSecondCharging() {
        return ((MissileElementManager)((MissileCollectionManager)this.elementCollectionManager).getElementManager()).calculatePowerConsumptionCombi(this.getPowerConsumedPerSecondChargingPerBlock(), true, this);
    }

    public void doShot(ControllerStateInterface var1, Timer var2, ShootContainer var3) {
        boolean var4 = ((MissileCollectionManager)this.elementCollectionManager).isInFocusMode();
        var1.getShootingDir(this.getSegmentController(), var3, this.getDistanceFull(), this.getSpeed(), ((MissileCollectionManager)this.elementCollectionManager).getControllerPos(), var4, true);
        if (var3.shootingDirTemp.lengthSquared() > 0.0F) {
            var3.shootingDirTemp.normalize();
            float var5 = this.getSpeed() * ((GameStateInterface)this.getSegmentController().getState()).getGameState().isRelativeProjectiles();
            var3.shootingDirTemp.scale(var5);

            assert !Float.isNaN(var3.shootingDirTemp.x) : var5 + "; " + var3.shootingDirTemp;

            ((MissileElementManager)((MissileCollectionManager)this.elementCollectionManager).getElementManager()).doShot(this, (MissileCollectionManager)this.elementCollectionManager, var3, this.getSpeed(), var1.getAquiredTarget(), var1.getPlayerState(), var2);
        }

    }

    public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
        return PowerConsumer.PowerConsumerCategory.MISSILES;
    }

    public float getAdditionalCapacityUsedPerDamageStatic() {
        return DumbMissileElementManager.ADDITIONAL_CAPACITY_USER_PER_DAMAGE;
    }

    public float AdditionalCapacityUsedPerDamageMultStatic() {
        return DumbMissileElementManager.ADDITIONAL_CAPACITY_USER_PER_DAMAGE_MULT;
    }

    public float percentagePowerUsageCharging() {
        return DumbMissileElementManager.PERCENTAGE_POWER_USAGE_CHARGING;
    }

    public float percentagePowerUsageResting() {
        return DumbMissileElementManager.PERCENTAGE_POWER_USAGE_RESTING;
    }
}
