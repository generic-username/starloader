//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.schema.game.common.controller.elements.combination.modifier;

import api.listener.events.weapon.UnitModifierHandledEvent;
import api.mod.StarLoader;
import org.schema.common.config.ConfigurationElement;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.combination.MissileCombiSettings;
import org.schema.game.common.controller.elements.combination.modifier.tagMod.BasicModifier;
import org.schema.game.common.controller.elements.combination.modifier.tagMod.formula.SetFomula;
import org.schema.game.common.controller.elements.missile.MissileUnit;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileCollectionManager;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileElementManager;

@Deprecated
public class MissileUnitModifier<E extends MissileUnit<?, ?, ?>> extends Modifier<E, MissileCombiSettings> {
    @ConfigurationElement(
            name = "Damage"
    )
    public BasicModifier damageModifier;
    @ConfigurationElement(
            name = "AdditiveDamage"
    )
    public BasicModifier additiveDamage;
    @ConfigurationElement(
            name = "Reload"
    )
    public BasicModifier reloadModifier;
    @ConfigurationElement(
            name = "Distance"
    )
    public BasicModifier distanceModifier;
    @ConfigurationElement(
            name = "Speed"
    )
    public BasicModifier speedModifier;
    @ConfigurationElement(
            name = "Split"
    )
    public BasicModifier splitModifier;
    @ConfigurationElement(
            name = "Mode"
    )
    public BasicModifier modeModifier;
    @ConfigurationElement(
            name = "PowerConsumption"
    )
    public BasicModifier powerConsumption;
    @ConfigurationElement(
            name = "AdditionalCapacityUsedPerDamage"
    )
    public BasicModifier additionalCapacityUsedPerDamageModifier;
    @ConfigurationElement(
            name = "AdditionalCapacityUsedPerDamageMult"
    )
    public BasicModifier additionalCapacityUsedPerDamageMultModifier;
    @ConfigurationElement(
            name = "PercentagePowerUsageResting"
    )
    public BasicModifier percentagePowerUsageRestingModifier;
    @ConfigurationElement(
            name = "PercentagePowerUsageCharging"
    )
    public BasicModifier percentagePowerUsageChargingModifier;
    @ConfigurationElement(
            name = "LockOnTimeSec"
    )
    public BasicModifier lockOnTimeSecModifier;
    @ConfigurationElement(
            name = "PossibleZoom"
    )
    public BasicModifier possibleZoomMod;
    public float outputPowerConsumption;
    public float outputDamage;
    public float outputDistance;
    public float outputSpeed;
    public float outputReload;
    public float outputMode;
    public int outputSplit = 1;
    public float outputAdditionalCapacityUsedPerDamage;
    public float outputAdditionalCapacityUsedPerDamageMult;

    public MissileUnitModifier() {
    }

    public void handle(MissileUnit var1, ControlBlockElementCollectionManager var2, float var3) {
        this.outputDamage = this.damageModifier.getOuput(var1.getBaseDamage(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputDamage += this.additiveDamage.getOuput(DumbMissileElementManager.ADDITIVE_DAMAGE, var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputReload = this.reloadModifier.getOuput(var1.getReloadTimeMs(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        float var4 = this.distanceModifier.getOuput(var1.getDistance(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputDistance = this.distanceModifier.formulas instanceof SetFomula ? var4 * ((GameStateInterface)var1.getSegmentController().getState()).getGameState().getWeaponRangeReference() : var4;
        this.outputSpeed = this.speedModifier.getOuput(var1.getSpeed(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputSplit = (int)this.splitModifier.getOuput(1.0F, var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputAdditionalCapacityUsedPerDamage = this.additionalCapacityUsedPerDamageModifier.getOuput(var1.getAdditionalCapacityUsedPerDamageStatic(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputAdditionalCapacityUsedPerDamageMult = this.additionalCapacityUsedPerDamageMultModifier.getOuput(var1.AdditionalCapacityUsedPerDamageMultStatic(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);

        assert this.modeModifier.formulas instanceof SetFomula : this.modeModifier.formulas;

        this.outputMode = (float)((int)this.modeModifier.getOuput(0.0F, var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3));
        var4 = this.powerConsumption.getOuput(var1.getBasePowerConsumption() * var1.getExtraConsume(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        float var5;
        if (var1.isPowerCharging(var1.getSegmentController().getState().getUpdateTime())) {
            var5 = this.percentagePowerUsageChargingModifier.getOuput(var1.percentagePowerUsageCharging(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        } else {
            var5 = this.percentagePowerUsageRestingModifier.getOuput(var1.percentagePowerUsageResting(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        }

        var4 = (float)((double)var4 + var1.getPowerInterface().getRechargeRatePercentPerSec() * (double)var5);
        this.outputPowerConsumption = var4;

        //INSERTED CODE @...
        UnitModifierHandledEvent event = new UnitModifierHandledEvent(this, var1, var2, var3);
        StarLoader.fireEvent(event, var1.getSegmentController().isOnServer());
        ///
    }

    public double calculatePowerConsumption(double var1, MissileUnit var3, ControlBlockElementCollectionManager<?, ?, ?> var4, float var5) {
        this.outputDamage = this.damageModifier.getOuput(var3.getBaseDamage(), var3.size(), var3.getCombiBonus(var4.getTotalSize()), var3.getEffectBonus(), var5);
        float var6 = this.powerConsumption.getOuput((float)(var1 * (double)var3.getExtraConsume()), var3.size(), var3.getCombiBonus(var4.getTotalSize()), var3.getEffectBonus(), var5);
        float var2;
        if (var3.isPowerCharging(var3.getSegmentController().getState().getUpdateTime())) {
            var2 = this.percentagePowerUsageChargingModifier.getOuput(var3.percentagePowerUsageCharging(), var3.size(), var3.getCombiBonus(var4.getTotalSize()), var3.getEffectBonus(), var5);
        } else {
            var2 = this.percentagePowerUsageRestingModifier.getOuput(var3.percentagePowerUsageResting(), var3.size(), var3.getCombiBonus(var4.getTotalSize()), var3.getEffectBonus(), var5);
        }

        var6 = (float)((double)var6 + var3.getPowerInterface().getRechargeRatePercentPerSec() * (double)var2);
        this.outputPowerConsumption = var6;
        return (double)var6;
    }

    public double calculateReload(MissileUnit var1, ControlBlockElementCollectionManager<?, ?, ?> var2, float var3) {
        return (double)this.reloadModifier.getOuput(var1.getReloadTimeMs(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
    }

    public void calcCombiSettings(MissileCombiSettings var1, ControlBlockElementCollectionManager<?, ?, ?> var2, ControlBlockElementCollectionManager<?, ?, ?> var3, float var4) {
        var1.lockOnTime = this.lockOnTimeSecModifier.getOuput(((DumbMissileCollectionManager)var2).getLockOnTimeRaw(), var2.getTotalSize(), var3.getTotalSize(), 0, var4);
        var1.possibleZoom = this.possibleZoomMod.getOuput(((DumbMissileCollectionManager)var2).getPossibleZoomRaw(), var2.getTotalSize(), var3.getTotalSize(), 0, var4);
    }
}
