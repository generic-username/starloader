//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.schema.game.common.controller.elements.combination.modifier;

import api.listener.events.weapon.UnitModifierHandledEvent;
import api.mod.StarLoader;
import org.schema.common.config.ConfigurationElement;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.beam.BeamCollectionManager;
import org.schema.game.common.controller.elements.beam.BeamElementManager;
import org.schema.game.common.controller.elements.beam.BeamUnit;
import org.schema.game.common.controller.elements.combination.BeamCombiSettings;
import org.schema.game.common.controller.elements.combination.modifier.tagMod.BasicModifier;
import org.schema.game.common.controller.elements.combination.modifier.tagMod.formula.SetFomula;
import org.schema.game.common.controller.elements.weapon.WeaponElementManager;

@Deprecated
public class BeamUnitModifier<E extends BeamUnit<?, ?, ?>> extends Modifier<E, BeamCombiSettings> {
    @ConfigurationElement(
            name = "HitSpeed"
    )
    public BasicModifier hitSpeed;
    @ConfigurationElement(
            name = "AdditiveDamage"
    )
    public BasicModifier additiveDamage;
    @ConfigurationElement(
            name = "PowerPerHit"
    )
    public BasicModifier powerPerHit;
    @ConfigurationElement(
            name = "PowerConsumptionCharging"
    )
    public BasicModifier powerConsumptionCharging;
    @ConfigurationElement(
            name = "Distance"
    )
    public BasicModifier distance;
    @ConfigurationElement(
            name = "CoolDown"
    )
    public BasicModifier coolDown;
    @ConfigurationElement(
            name = "BurstTime"
    )
    public BasicModifier burstTime;
    @ConfigurationElement(
            name = "InitialTicks"
    )
    public BasicModifier initialTicks;
    @ConfigurationElement(
            name = "LatchOn"
    )
    public BasicModifier latchModifier;
    @ConfigurationElement(
            name = "CheckLatchConnection"
    )
    public BasicModifier checkLatchConnectionModifier;
    @ConfigurationElement(
            name = "Penetration"
    )
    public BasicModifier penetrationModifier;
    @ConfigurationElement(
            name = "AcidDamagePercentage"
    )
    public BasicModifier acidModifier;
    @ConfigurationElement(
            name = "FriendlyFire"
    )
    public BasicModifier friendlyFireModifier;
    @ConfigurationElement(
            name = "Aimable"
    )
    public BasicModifier aimableModifier;
    @ConfigurationElement(
            name = "ChargeTime"
    )
    public BasicModifier chargeTimeMod;
    @ConfigurationElement(
            name = "MinEffectiveValue"
    )
    public BasicModifier minEffectiveValueMod;
    @ConfigurationElement(
            name = "MinEffectiveRange"
    )
    public BasicModifier minEffectiveRangeMod;
    @ConfigurationElement(
            name = "MaxEffectiveValue"
    )
    public BasicModifier maxEffectiveValueMod;
    @ConfigurationElement(
            name = "MaxEffectiveRange"
    )
    public BasicModifier maxEffectiveRangeMod;
    @ConfigurationElement(
            name = "PowerConsumptionResting"
    )
    public BasicModifier restingPowerConsumptionMod;
    @ConfigurationElement(
            name = "PossibleZoom"
    )
    public BasicModifier possibleZoomMod;
    public float outputPowerConsumption;
    public float outputTickRate;
    public float outputDamagePerHit;
    public float outputDistance;
    public float outputCoolDown;
    public float outputBurstTime;
    public float outputInitialTicks;
    public float outputAcidPercentage;
    public boolean outputFriendlyFire;
    public boolean outputAimable;
    public boolean outputPenetration;
    public boolean outputLatchMode;
    public boolean outputCheckLatchConnection;
    public float outputMinEffectiveValue;
    public float outputMinEffectiveRange;
    public float outputMaxEffectiveValue;
    public float outputMaxEffectiveRange;

    public BeamUnitModifier() {
    }

    public void handle(BeamUnit var1, ControlBlockElementCollectionManager var2, float var3) {
        assert var1 != null;

        assert this.hitSpeed != null;

        this.outputTickRate = this.hitSpeed.getOuput(var1.getTickRate(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputDamagePerHit = this.powerPerHit.getOuput(var1.getBaseBeamPower(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputDamagePerHit += this.additiveDamage.getOuput(WeaponElementManager.ADDITIVE_DAMAGE, var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputPowerConsumption = this.powerConsumptionCharging.getOuput(var1.getBasePowerConsumption() * var1.getExtraConsume(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        float var4 = this.distance.getOuput(var1.getDistance(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputDistance = this.distance.formulas instanceof SetFomula ? var4 * ((GameStateInterface)var1.getSegmentController().getState()).getGameState().getWeaponRangeReference() : var4;
        this.outputFriendlyFire = (int)this.friendlyFireModifier.getOuput(var1.isFriendlyFire() ? 1.0F : 0.0F, var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3) != 0;
        this.outputAimable = (int)this.aimableModifier.getOuput(var1.isAimable() ? 1.0F : 0.0F, var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3) != 0;
        this.outputPenetration = (int)this.penetrationModifier.getOuput(var1.isPenetrating() ? 1.0F : 0.0F, var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3) != 0;
        this.outputLatchMode = (int)this.latchModifier.getOuput(var1.isLatchOn() ? 1.0F : 0.0F, var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3) != 0;
        this.outputCheckLatchConnection = (int)this.checkLatchConnectionModifier.getOuput(var1.isCheckLatchConnection() ? 1.0F : 0.0F, var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3) != 0;
        this.outputAcidPercentage = this.acidModifier.getOuput(var1.getAcidDamagePercentage(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputCoolDown = this.coolDown.getOuput(var1.getCoolDownSec(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputBurstTime = this.burstTime.getOuput(var1.getBurstTime(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputInitialTicks = this.initialTicks.getOuput(var1.getInitialTicks(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputMinEffectiveValue = this.minEffectiveValueMod.getOuput(var1.getMinEffectiveValue(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputMinEffectiveRange = this.minEffectiveRangeMod.getOuput(var1.getMinEffectiveRange(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputMaxEffectiveValue = this.maxEffectiveValueMod.getOuput(var1.getMaxEffectiveValue(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
        this.outputMaxEffectiveRange = this.maxEffectiveRangeMod.getOuput(var1.getMaxEffectiveRange(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);

        //INSERTED CODE @...
        UnitModifierHandledEvent event = new UnitModifierHandledEvent(this, var1, var2, var3);
        StarLoader.fireEvent(event, var1.getSegmentController().isOnServer());
        ///
    }

    public double calculateReload(BeamUnit var1, ControlBlockElementCollectionManager<?, ?, ?> var2, float var3) {
        return (double)this.coolDown.getOuput(var1.getReloadTimeMs(), var1.size(), var1.getCombiBonus(var2.getTotalSize()), var1.getEffectBonus(), var3);
    }

    public double calculatePowerConsumption(double var1, BeamUnit var3, ControlBlockElementCollectionManager var4, float var5) {
        return var3.isPowerCharging(var3.getSegmentController().getState().getUpdateTime()) ? (double)this.powerConsumptionCharging.getOuput((float)(var1 * (double)var3.getExtraConsume()), var3.size(), var3.getCombiBonus(var4.getTotalSize()), var3.getEffectBonus(), var5) : (double)this.restingPowerConsumptionMod.getOuput((float)(var1 * (double)var3.getExtraConsume()), var3.size(), var3.getCombiBonus(var4.getTotalSize()), var3.getEffectBonus(), var5);
    }

    public void calcCombiSettings(BeamCombiSettings var1, ControlBlockElementCollectionManager<?, ?, ?> var2, ControlBlockElementCollectionManager<?, ?, ?> var3, float var4) {
        var1.chargeTime = this.chargeTimeMod.getOuput(((BeamCollectionManager)var2).getChargeTime(), var2.getTotalSize(), var3.getTotalSize(), 0, var4);
        var1.possibleZoom = this.possibleZoomMod.getOuput(((BeamCollectionManager)var2).getPossibleZoomRaw(), var2.getTotalSize(), var3.getTotalSize(), 0, var4);
        var1.burstTime = this.burstTime.getOuput(((BeamElementManager)((BeamCollectionManager)var2).getElementManager()).getBurstTime(), var2.getTotalSize(), var3.getTotalSize(), 0, var4);
    }
}
