//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.schema.game.common.data.world;

import api.listener.events.world.GalaxyInstantiateEvent;
import api.listener.events.world.SystemPreGenerationEvent;
import api.listener.events.world.sector.SectorUnloadEvent;
import api.listener.events.world.sector.SegmentControllerUnloadEvent;
import api.mod.StarLoader;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.dynamics.InternalTickCallback;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2LongOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArrayFIFOQueue;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.objects.*;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.DebugControlManager;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.Starter;
import org.schema.game.common.controller.*;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.data.DebugServerPhysicalObject;
import org.schema.game.common.data.EntityFileTools;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.physics.PairCachingGhostObjectAlignable;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.game.common.data.physics.RigidBodySegmentController;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionRelation.RType;
import org.schema.game.common.data.player.faction.config.FactionSystemOwnerBonusConfig;
import org.schema.game.common.data.world.Sector.SectorMode;
import org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType;
import org.schema.game.common.data.world.space.PlanetCore;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GalaxyTmpVars;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.simulation.npc.geo.FactionResourceRequestContainer;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.remote.RemoteSerializableObject;
import org.schema.schine.physics.Physics;
import org.schema.schine.physics.PhysicsState;
import org.schema.schine.resource.DiskWritable;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import java.io.*;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Deprecated
public class Universe implements TagSerializable {
    public static final int SECTOR_GENERATION_LENGTH = 5;
    public static final float SECTOR_MARGIN = 300.0F;
    private static final int MAX_PHYSICS_REPOSITORY_SIZE = 30;
    public static boolean resetMinableAndVulnerable;
    private static Random random;
    public final Object2LongOpenHashMap<String> writeMap = new Object2LongOpenHashMap();
    public final ObjectArrayFIFOQueue<Vector3i> ftlDirty = new ObjectArrayFIFOQueue();
    public final LongArrayFIFOQueue tradeNodesDirty = new LongArrayFIFOQueue();
    private final Object2ObjectOpenHashMap<Vector3i, VoidSystem> starSystemMap = new Object2ObjectOpenHashMap();
    private final Object2ObjectOpenHashMap<Vector3i, Galaxy> galaxyMap = new Object2ObjectOpenHashMap();
    private final Int2ObjectOpenHashMap<Sector> sectors;
    private final Object2ObjectOpenHashMap<Vector3i, Sector> sectorPositions;
    private final GameServerState state;
    private final ObjectArrayFIFOQueue<SendableSegmentController> toClear = new ObjectArrayFIFOQueue();
    private final List<Physics> physicsRepository = new ObjectArrayList();
    private final Vector3i where = new Vector3i();
    private final Vector3f otherSecCenter = new Vector3f();
    private final Vector3f nearVector = new Vector3f();
    private final Transform out = new Transform();
    private final Transform ttmp = new Transform();
    private final Matrix3f rot = new Matrix3f();
    private final Vector3f bb = new Vector3f();
    private final Vector3f dist = new Vector3f();
    private final TransformaleObjectTmpVars v = new TransformaleObjectTmpVars();
    private final Vector3i tmpStellar = new Vector3i();
    private final Vector3i tmpGalaxy = new Vector3i();
    private final GalaxyManager galaxyManager;
    Vector3f tmpObjectPos = new Vector3f();
    Vector3i belogingVector = new Vector3i();
    Transform std = new Transform();
    Vector3i otherSecAbs = new Vector3i();
    Random systemRandom = new Random();
    private Int2ObjectOpenHashMap<VirtualEntityAttachment> virtualMap = new Int2ObjectOpenHashMap();
    private String name;
    private ObjectOpenHashSet<Sector> inactiveWrittenSectors = new ObjectOpenHashSet();
    private ObjectOpenHashSet<Sector> entityCleaningSectors = new ObjectOpenHashSet();
    private long lastPing;
    private long seed;
    private List<Universe.DeferredLoad> deffereedLoads = new ObjectArrayList();
    public final Object2LongOpenHashMap<Vector3i> attackSector = new Object2LongOpenHashMap();
    private long lastTime;
    private long lastAttackCheck;

    public Universe(GameServerState var1) {
        if (GameServerState.ENTITY_DATABASE_PATH == null) {
            random = new Random();
            System.err.println("NO UNIVERSE PATH GIVEN. RETURN");
            this.sectors = new Int2ObjectOpenHashMap();
            this.sectorPositions = new Object2ObjectOpenHashMap();
            this.galaxyManager = new GalaxyManager(var1);
            this.state = var1;
        } else {
            this.sectors = new Int2ObjectOpenHashMap();
            this.sectorPositions = new Object2ObjectOpenHashMap();
            this.galaxyManager = new GalaxyManager(var1);
            this.state = var1;
            VirtualEntityAttachment.universe = this;
            this.setSeed(0L);
            FileExt var41;
            if ((var41 = new FileExt(GameServerState.ENTITY_DATABASE_PATH + File.separator + ".seed")).exists()) {
                label309: {
                    System.err.println("[INIT] Seed File: " + var41.getAbsolutePath());
                    DataInputStream var2 = null;
                    boolean var28 = false;

                    label302: {
                        label301: {
                            try {
                                var28 = true;
                                var2 = new DataInputStream(new FileInputStream(var41));
                                this.setSeed(var2.readLong());
                                var28 = false;
                                break label302;
                            } catch (FileNotFoundException var38) {
                                var38.printStackTrace();
                                var28 = false;
                            } catch (IOException var39) {
                                var39.printStackTrace();
                                var41.delete();
                                var28 = false;
                                break label301;
                            } finally {
                                if (var28) {
                                    if (var2 != null) {
                                        try {
                                            var2.close();
                                        } catch (IOException var29) {
                                            var29.printStackTrace();
                                        }
                                    }

                                }
                            }

                            if (var2 != null) {
                                try {
                                    var2.close();
                                } catch (IOException var34) {
                                    var34.printStackTrace();
                                }
                            }
                            break label309;
                        }

                        if (var2 != null) {
                            try {
                                var2.close();
                            } catch (IOException var33) {
                                var33.printStackTrace();
                            }
                        }
                        break label309;
                    }

                    try {
                        var2.close();
                    } catch (IOException var35) {
                        var35.printStackTrace();
                    }
                }
            }

            if (!var41.exists()) {
                label310: {
                    System.err.println("[INIT] Not found Seed File: " + var41.getAbsolutePath() + " " + GameServerState.ENTITY_DATABASE_PATH + "; Path exists? " + (new FileExt(GameServerState.ENTITY_DATABASE_PATH)).exists() + "; dir? " + (new FileExt(GameServerState.ENTITY_DATABASE_PATH)).isDirectory());
                    this.setSeed(System.currentTimeMillis());
                    DataOutputStream var42 = null;
                    boolean var15 = false;

                    label290: {
                        try {
                            var15 = true;

                            assert var41.getParentFile().exists() : var41.getAbsolutePath();

                            assert var41.getParentFile().isDirectory() : var41.getAbsolutePath();

                            (var42 = new DataOutputStream(new FileOutputStream(var41))).writeLong(this.getSeed());
                            var42.flush();
                            var15 = false;
                            break label290;
                        } catch (IOException var36) {
                            var36.printStackTrace();
                            var15 = false;
                        } finally {
                            if (var15) {
                                if (var42 != null) {
                                    try {
                                        var42.close();
                                    } catch (IOException var30) {
                                        var30.printStackTrace();
                                    }
                                }

                            }
                        }

                        if (var42 != null) {
                            try {
                                var42.close();
                            } catch (IOException var31) {
                                var31.printStackTrace();
                            }
                        }
                        break label310;
                    }

                    try {
                        var42.close();
                    } catch (IOException var32) {
                        var32.printStackTrace();
                    }
                }
            }

            random = new Random(this.getSeed());
        }
    }

    public static void calcSecPos(StateInterface var0, Vector3i var1, Vector3i var2, long var3, long var5, Transform var7) {
        calcSecPos(var0, var1, var2, var3, var5, var7, false);
    }

    private static void calcSecPos(StateInterface var0, Vector3i var1, Vector3i var2, long var3, long var5, Transform var7, boolean var8) {
        Vector3i var12 = StellarSystem.getPosFromSector(var2, new Vector3i());
        Vector3i var4;
        (var4 = new Vector3i()).sub(var2, var1);
        var12.scale(16);
        var12.add(8, 8, 8);
        var12.sub(var1);
        Vector3f var10 = new Vector3f();
        Vector3f var11 = new Vector3f();
        var10.set((float)var4.x * ((GameStateInterface)var0).getSectorSize(), (float)var4.y * ((GameStateInterface)var0).getSectorSize(), (float)var4.z * ((GameStateInterface)var0).getSectorSize());
        var11.set((float)var12.x * ((GameStateInterface)var0).getSectorSize(), (float)var12.y * ((GameStateInterface)var0).getSectorSize(), (float)var12.z * ((GameStateInterface)var0).getSectorSize());
        var7.setIdentity();
        if (var11.lengthSquared() > 0.0F) {
            var7.origin.add(var11);
            var7.basis.rotX(0.0F);
            Vector3f var9;
            (var9 = new Vector3f()).sub(var10, var11);
            var7.origin.add(var9);
            var7.basis.transform(var7.origin);
        } else {
            var7.basis.rotX(0.0F);
            var7.origin.set(var10);
            var7.basis.transform(var7.origin);
        }
    }

    public static void calcSecPosInv(StateInterface var0, Vector3i var1, Vector3i var2, long var3, long var5, Transform var7) {
        calcSecPos(var0, var1, var2, var3, var5, var7, true);
    }

    public static void calcSunPosInnerStarSystem(StateInterface var0, Vector3i var1, StellarSystem var2, long var3, Transform var5) {
        float var6 = ((GameStateInterface)var0).getGameState().getRotationProgession();
        var5.basis.rotX(6.2831855F * var6);
        var5.origin.set((float)((var2.getPos().x << 4) + 8 - var1.x), (float)((var2.getPos().y << 4) + 8 - var1.y), (float)((var2.getPos().z << 4) + 8 - var1.z));
        var5.origin.scale(((GameStateInterface)var0).getSectorSize());
        var5.basis.transform(var5.origin);
    }

    public static Random getRandom() {
        return random;
    }

    public static Sendable loadUntouchedAsteroid(GameServerState var0, DatabaseEntry var1, Sector var2) {
        assert EntityType.ASTEROID.dbTypeId == 3;

        assert var1.type == EntityType.ASTEROID.dbTypeId;

        FloatingRock var3;
        (var3 = new FloatingRock(var0)).setId(var0.getNextFreeObjectId());
        var3.setSectorId(var2.getId());
        var3.initialize();
        var3.getMinPos().set(var1.minPos);
        var3.getMaxPos().set(var1.maxPos);
        var3.setTouched(var1.touched, false);
        var3.setRealName(var1.realName);
        var3.setFactionId(var1.faction);
        var3.setLastModifier(var1.lastModifier);
        var3.setSpawner(var1.spawner);
        var3.setUniqueIdentifier(var1.uid);
        var3.setCreatorId(var1.creatorID);
        var3.setTracked(var1.tracked);
        var3.dbId = var1.dbId;
        if (var0.isPhysicalAsteroids()) {
            var3.setMass(0.1F);
        } else {
            var3.setMass(0.0F);
        }

        var3.getRemoteTransformable().getInitialTransform().setIdentity();
        var3.getRemoteTransformable().getInitialTransform().origin.set(var1.pos);
        var3.getRemoteTransformable().getWorldTransform().setIdentity();
        var3.getRemoteTransformable().getWorldTransform().origin.set(var1.pos);
        if (var1.seed == 0L) {
            var3.setSeed(getRandom().nextLong());
        } else {
            var3.setSeed(var1.seed);
        }

        var0.getController().getSynchController().addNewSynchronizedObjectQueued(var3);
        return var3;
    }

    public static SimpleTransformableSendableObject<?> loadEntity(GameServerState var0, EntityType var1, File var2, Sector var3, long var4) throws IOException, EntityNotFountException {
        Tag var6;
        long var7;
        long var9;
        if (var1 == EntityType.SHIP) {
            var6 = var0.getController().readEntity(var2.getName(), "");
            Ship var25;
            (var25 = new Ship(var0)).setId(var0.getNextFreeObjectId());
            var25.setSectorId(var3.getId());
            var25.dbId = var4;
            var25.initialize();
            var7 = System.currentTimeMillis();
            var25.fromTagStructure(var6);
            var9 = System.currentTimeMillis() - var7;
            synchronized(var0.getUniverse().writeMap) {
                var25.setLastWrite(var0.getUniverse().writeMap.getLong(var25.getUniqueIdentifier()));
            }

            if (var9 > 10L) {
                System.err.println("[SERVER][LOADENTITY] WARNING: SHIP IN " + var3 + " FROM TAG: " + var2.getName() + " took long: " + var9 + "ms");
            }

            if (resetMinableAndVulnerable && !var3.isTutorialSector()) {
                var25.setMinable(true);
                var25.setVulnerable(true);
            }

            var0.getController().getSynchController().addNewSynchronizedObjectQueued(var25);
            Starter.modManager.onSegmentControllerSpawn(var25);
            return var25;
        } else if (var1 != EntityType.ASTEROID && var1 != EntityType.ASTEROID_MANAGED) {
            if (var1 == EntityType.SHOP) {
                var6 = var0.getController().readEntity(var2.getName(), "");
                ShopSpaceStation var24;
                (var24 = new ShopSpaceStation(var0)).setId(var0.getNextFreeObjectId());
                var24.setSectorId(var3.getId());
                var24.dbId = var4;
                var24.initialize();
                var7 = System.currentTimeMillis();
                var24.fromTagStructure(var6);
                if ((var9 = System.currentTimeMillis() - var7) > 10L) {
                    System.err.println("[SERVER][LOADENTITY] WARNING: SHOP IN " + var3 + " FROM TAG: " + var2.getName() + " took long: " + var9 + "ms");
                }

                synchronized(var0.getUniverse().writeMap) {
                    var24.setLastWrite(var0.getUniverse().writeMap.getLong(var24.getUniqueIdentifier()));
                }

                if (resetMinableAndVulnerable && !var3.isTutorialSector()) {
                    var24.setMinable(true);
                    var24.setVulnerable(true);
                }

                var0.getController().getSynchController().addNewSynchronizedObjectQueued(var24);
                Starter.modManager.onSegmentControllerSpawn(var24);
                return var24;
            } else if (var1 == EntityType.SPACE_STATION) {
                var6 = var0.getController().readEntity(var2.getName(), "");
                SpaceStation var23;
                (var23 = new SpaceStation(var0)).setId(var0.getNextFreeObjectId());
                var23.setSectorId(var3.getId());
                var23.dbId = var4;
                var23.initialize();
                var7 = System.currentTimeMillis();
                var23.fromTagStructure(var6);
                if ((var9 = System.currentTimeMillis() - var7) > 10L) {
                    System.err.println("[SERVER][LOADENTITY] WARNING: STATION IN " + var3 + " FROM TAG: " + var2.getName() + " took long: " + var9 + "ms");
                }

                synchronized(var0.getUniverse().writeMap) {
                    var23.setLastWrite(var0.getUniverse().writeMap.getLong(var23.getUniqueIdentifier()));
                }

                if (resetMinableAndVulnerable && !var3.isTutorialSector()) {
                    var23.setMinable(true);
                    var23.setVulnerable(true);
                }

                var0.getController().getSynchController().addNewSynchronizedObjectQueued(var23);
                Starter.modManager.onSegmentControllerSpawn(var23);
                return var23;
            } else if (var1 == EntityType.PLANET_SEGMENT) {
                var6 = var0.getController().readEntity(var2.getName(), "");
                Planet var22;
                (var22 = new Planet(var0)).setId(var0.getNextFreeObjectId());
                var22.setSectorId(var3.getId());
                var22.dbId = var4;
                var22.initialize();
                var7 = System.currentTimeMillis();
                var22.fromTagStructure(var6);
                if (!var22.getPlanetCoreUID().equals("none")) {
                    var22.setPlanetCore(var3.getPlanetCore());
                    if (var22.getCore() == null) {
                        try {
                            System.err.println("[UNIVERSE][PLANET] LOADING PLANET CORE: " + var22.getPlanetCoreUID());
                            Tag var26 = var0.getController().readEntity(var22.getPlanetCoreUID(), ".ent");
                            PlanetCore var10;
                            (var10 = new PlanetCore(var0)).setId(var0.getNextFreeObjectId());
                            var10.setSectorId(var3.getId());
                            var10.initialize();
                            var10.fromTagStructure(var26);
                            var0.getController().getSynchController().addNewSynchronizedObjectQueued(var10);
                            var22.setPlanetCore(var10);
                            var3.setPlanetCore(var10);
                        } catch (EntityNotFountException var15) {
                            var15.printStackTrace();
                            var22.setPlanetCoreUID("none");
                        }
                    } else {
                        System.err.println("[UNIVERSE][PLANET] PLANET CORE ALREADY LOADED: " + var22.getCore());
                    }
                } else {
                    System.err.println("[UNIVERSE][PLANET] NO PLANET CORE");
                }

                if ((var9 = System.currentTimeMillis() - var7) > 10L) {
                    System.err.println("[SERVER][LOADENTITY] WARNING: PLANET IN " + var3 + " FROM TAG: " + var2.getName() + " took long: " + var9 + "ms");
                }

                synchronized(var0.getUniverse().writeMap) {
                    var22.setLastWrite(var0.getUniverse().writeMap.getLong(var22.getUniqueIdentifier()));
                }

                if (resetMinableAndVulnerable && !var3.isTutorialSector()) {
                    var22.setMinable(true);
                    var22.setVulnerable(true);
                }

                var0.getController().getSynchController().addNewSynchronizedObjectQueued(var22);
                Starter.modManager.onSegmentControllerSpawn(var22);
                return var22;
            } else if (var1 == EntityType.PLANET_ICO) {
                var6 = var0.getController().readEntity(var2.getName(), "");
                PlanetIco var21;
                (var21 = new PlanetIco(var0)).setId(var0.getNextFreeObjectId());
                var21.setSectorId(var3.getId());
                var21.dbId = var4;
                var21.initialize();
                var7 = System.currentTimeMillis();
                var21.fromTagStructure(var6);
                if ((var9 = System.currentTimeMillis() - var7) > 10L) {
                    System.err.println("[SERVER][LOADENTITY] WARNING: PLANET IN " + var3 + " FROM TAG: " + var2.getName() + " took long: " + var9 + "ms");
                }

                synchronized(var0.getUniverse().writeMap) {
                    var21.setLastWrite(var0.getUniverse().writeMap.getLong(var21.getUniqueIdentifier()));
                }

                if (resetMinableAndVulnerable && !var3.isTutorialSector()) {
                    var21.setMinable(true);
                    var21.setVulnerable(true);
                }

                var0.getController().getSynchController().addNewSynchronizedObjectQueued(var21);
                Starter.modManager.onSegmentControllerSpawn(var21);
                return var21;
            } else if (var1 == EntityType.DEATH_STAR) {
                var6 = var0.getController().readEntity(var2.getName(), "");
                TeamDeathStar var20;
                (var20 = new TeamDeathStar(var0)).setId(var0.getNextFreeObjectId());
                var20.setSectorId(var3.getId());
                var20.initialize();
                var20.fromTagStructure(var6);
                synchronized(var0.getUniverse().writeMap) {
                    var20.setLastWrite(var0.getUniverse().writeMap.getLong(var20.getUniqueIdentifier()));
                }

                var0.getController().getSynchController().addNewSynchronizedObjectQueued(var20);
                Starter.modManager.onSegmentControllerSpawn(var20);
                return var20;
            } else {
                System.err.println("[LOADENTITY] Exception: no loading routine for type: " + var1.name());
                return null;
            }
        } else {
            var6 = var0.getController().readEntity(var2.getName(), "");
            Object var10000 = var1 == EntityType.ASTEROID ? new FloatingRock(var0) : new FloatingRockManaged(var0);
            Object var19 = var10000;
            ((FloatingRock)var10000).setId(var0.getNextFreeObjectId());
            ((FloatingRock)var19).setSectorId(var3.getId());
            ((FloatingRock)var19).dbId = var4;
            ((FloatingRock)var19).initialize();
            var7 = System.currentTimeMillis();
            ((FloatingRock)var19).fromTagStructure(var6);
            if ((var9 = System.currentTimeMillis() - var7) > 10L) {
                System.err.println("[SERVER][LOADENTITY] WARNING: ROCK IN " + var3 + " FROM TAG: " + var2.getName() + " took long: " + var9 + "ms");
            }

            synchronized(var0.getUniverse().writeMap) {
                ((FloatingRock)var19).setLastWrite(var0.getUniverse().writeMap.getLong(((FloatingRock)var19).getUniqueIdentifier()));
            }

            if (resetMinableAndVulnerable && !var3.isTutorialSector()) {
                ((FloatingRock)var19).setMinable(true);
                ((FloatingRock)var19).setVulnerable(true);
            }

            var0.getController().getSynchController().addNewSynchronizedObjectQueued((Sendable)var19);
            Starter.modManager.onSegmentControllerSpawn((SegmentController)var19);
            return (SimpleTransformableSendableObject)var19;
        }
    }

    private Sector addNewSector(Vector3i var1) throws IOException {
        Sector var2;
        (var2 = new Sector(this.state)).pos = new Vector3i(var1);
        var2.setSeed(this.getSectorSeed(var1));
        var2.setRandom(new Random(var2.getSeed()));
        var2.setChangedForDb(true);

        try {
            this.state.getDatabaseIndex().getTableManager().getSectorTable().updateOrInsertSector(var2);
        } catch (SQLException var3) {
            var3.printStackTrace();
        }

        this.addSector(var2);
        var2.populate(this.state);
        return var2;
    }

    public long getSectorSeed(Vector3i var1) {
        return this.seed + var1.code();
    }

    public void addSector(Sector var1) throws IOException {
        assert var1 != null;

        this.sectors.put(var1.getId(), var1);
        var1.onAddedSector();
        this.sectorPositions.put(var1.pos, var1);
    }

    public void addToClear(SendableSegmentController var1) {
        synchronized(this.toClear) {
            this.toClear.enqueue(var1);
        }
    }

    public void addToFreePhysics(Physics var1, Sector var2) {
        boolean var3 = false;
        ((PhysicsExt)var1).setState((PhysicsState)null);
        ((PhysicsExt)var1).getDynamicsWorld().setInternalTickCallback((InternalTickCallback)null, (Object)null);
        synchronized(this.getPhysicsRepository()) {
            if (!(var3 = this.getPhysicsRepository().size() > 30) && !this.getPhysicsRepository().contains(var1)) {
                this.getPhysicsRepository().add(var1);
                var1.softClean();
            }
        }

        if (var3) {
            System.out.println("[SERVER][UNIVERSE] Cleaned up Physics, because repository is full");
            var1.cleanUp();
            var2.setPhysics((Physics)null);
            var2.entityUids.clear();
        }

    }

    public boolean existsSector(int var1) {
        return (Sector)this.sectors.get(var1) != null;
    }

    public void fromTagStructure(Tag var1) {
    }

    public Tag toTagStructure() {
        return null;
    }

    public boolean getIsSectorActive(int var1) {
        Sector var2;
        return (var2 = (Sector)this.sectors.get(var1)) != null && var2.isActive();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String var1) {
        this.name = var1;
    }

    public Physics getPhysicsInstance(PhysicsState var1) {
        synchronized(this.getPhysicsRepository()) {
            if (this.getPhysicsRepository().size() > 0) {
                PhysicsExt var3;
                (var3 = (PhysicsExt)this.getPhysicsRepository().remove(0)).setState(var1);
                return var3;
            }
        }

        return new PhysicsExt(var1);
    }

    public List<Physics> getPhysicsRepository() {
        return this.physicsRepository;
    }

    public Sector getSector(int var1) {
        return (Sector)this.sectors.get(var1);
    }

    public Sector getSector(Vector3i var1) throws IOException {
        return this.getSector(var1, true);
    }

    public Sector getSector(Vector3i var1, boolean var2) throws IOException {
        try {
            if (!this.sectorPositions.containsKey(var1)) {
                this.loadOrGenerateSector(var1);
            }

            Sector var3;
            if (!(var3 = (Sector)this.sectorPositions.get(var1)).isActive() && (this.entityCleaningSectors.contains(var3) || this.inactiveWrittenSectors.contains(var3))) {
                this.loadOrGenerateSector(var1);
            }

            if (!this.sectors.containsKey(var3.getId())) {
                assert var3 != null;

                this.sectors.put(var3.getId(), var3);
            }

            assert var3 != null : var1 + " - " + this.sectorPositions;

            if (var2) {
                var3.setActive(true);
            }

            return var3;
        } catch (SQLException var4) {
            var4.printStackTrace();
            throw new IOException(var4);
        }
    }

    public Vector3i getSectorBelonging(SimpleTransformableSendableObject var1) {
        if (var1.isHidden()) {
            return this.getSector(var1.getSectorId()).pos;
        } else if (var1 instanceof SegmentController && ((SegmentController)var1).getDockingController().isDocked()) {
            return this.getSector(var1.getSectorId()).pos;
        } else if (var1 instanceof SegmentController && ((SegmentController)var1).railController.isDockedAndExecuted()) {
            return this.getSector(((SegmentController)var1).railController.getRoot().getSectorId()).pos;
        } else if (var1 instanceof AbstractCharacter && ((PairCachingGhostObjectAlignable)((AbstractCharacter)var1).getPhysicsDataContainer().getObject()).getAttached() != null) {
            return this.getSector(var1.getSectorId()).pos;
        } else {
            Sector var2;
            Vector3i var3 = (var2 = this.getSector(var1.getSectorId())).pos;
            int var4 = -1;
            this.nearVector.set(var1.getWorldTransform().origin);
            StellarSystem.isStarSystem(var2.pos);

            for(int var6 = 0; var6 < Element.DIRECTIONSi.length; ++var6) {
                this.std.setIdentity();
                this.otherSecAbs.set(var3);
                this.otherSecAbs.add(Element.DIRECTIONSi[var6]);
                Sector var5;
                if ((var5 = (Sector)this.sectorPositions.get(this.otherSecAbs)) != null) {
                    SimpleTransformableSendableObject.calcWorldTransformRelative(var1.getSectorId(), var3, var5.getId(), this.std, this.state, true, this.out, this.v);
                    this.dist.sub(var1.getWorldTransform().origin, this.out.origin);
                    if (this.dist.lengthSquared() < this.nearVector.lengthSquared()) {
                        this.nearVector.set(this.dist);
                        var4 = var6;
                    }
                } else {
                    this.v.dir.sub(this.otherSecAbs, var3);
                    this.v.otherSecCenter.set((float)this.v.dir.x * this.state.getSectorSize(), (float)this.v.dir.y * this.state.getSectorSize(), (float)this.v.dir.z * this.state.getSectorSize());
                    this.v.t.set(this.std);
                    this.v.t.origin.add(this.v.otherSecCenter);
                    this.out.set(this.v.t);
                    this.dist.sub(var1.getWorldTransform().origin, this.out.origin);
                    if (this.dist.lengthSquared() < this.nearVector.lengthSquared()) {
                        this.nearVector.set(this.dist);
                        var4 = var6;
                    }
                }
            }

            if (var4 >= 0) {
                this.belogingVector.set(var3);
                this.belogingVector.add(Element.DIRECTIONSi[var4]);
                this.state.getController().queueSectorSwitch(var1, this.belogingVector, 0, false);
                return this.belogingVector;
            } else {
                return var3;
            }
        }
    }

    public Collection<Sector> getSectorSet() {
        return this.sectors.values();
    }

    public Sector getSectorWithoutLoading(Vector3i var1) {
        return (Sector)this.sectorPositions.get(var1);
    }

    public Universe.SystemOwnershipType getSystemOwnerShipType(StellarSystem var1, int var2) {
        if (var1.getOwnerFaction() == 0) {
            return Universe.SystemOwnershipType.NONE;
        } else if (var2 != 0 && var1.getOwnerFaction() == var2) {
            return Universe.SystemOwnershipType.BY_SELF;
        } else {
            RType var3 = this.state.getFactionManager().getRelation(var2, var1.getOwnerFaction());
            switch(var3) {
                case ENEMY:
                    return Universe.SystemOwnershipType.BY_ENEMY;
                case FRIEND:
                    return Universe.SystemOwnershipType.BY_ALLY;
                case NEUTRAL:
                    return Universe.SystemOwnershipType.BY_NEUTRAL;
                default:
                    return Universe.SystemOwnershipType.NONE;
            }
        }
    }

    public Universe.SystemOwnershipType getSystemOwnerShipType(int var1, int var2) {
        Sector var4;
        if ((var4 = this.getSector(var1)) != null) {
            try {
                StellarSystem var5 = this.getStellarSystemFromSecPos(var4.pos);
                return this.getSystemOwnerShipType(var5, var2);
            } catch (IOException var3) {
                var3.printStackTrace();
            }
        }

        return Universe.SystemOwnershipType.NONE;
    }

    public StellarSystem getStellarSystemFromSecPos(Vector3i var1) throws IOException {
        Vector3i var2 = VoidSystem.getContainingSystem(var1, this.tmpStellar);
        VoidSystem var3;
        if ((var3 = (VoidSystem)this.starSystemMap.get(var2)) == null) {
            Vector3i var4 = Galaxy.getContainingGalaxyFromSystemPos(var2, this.tmpGalaxy);
            Galaxy var5 = this.getGalaxy(var4);
            var3 = this.loadOrGenerateVoidSystem(var2, var5);
            this.starSystemMap.put(var3.getPos(), var3);
        }

        assert VoidSystem.getContainingSystem(var1, new Vector3i()).equals(var3.getPos());

        return var3.getInternal(var1);
    }

    public Galaxy getGalaxyFromSystemPos(Vector3i var1) {
        var1 = Galaxy.getContainingGalaxyFromSystemPos(var1, this.tmpGalaxy);
        return this.getGalaxy(var1);
    }

    public Galaxy getGalaxy(Vector3i var1) {
        Galaxy var2;
        if ((var2 = (Galaxy)this.galaxyMap.get(var1)) == null) {
            var2 = new Galaxy(this.seed + (long)var1.hashCode(), new Vector3i(var1));

            ///INSERTED CODE
            GalaxyInstantiateEvent e = new GalaxyInstantiateEvent(var2, var1);
            StarLoader.fireEvent(GalaxyInstantiateEvent.class, e, true);
            var2 = e.getGalaxy();
            ///

            var2.generate();
            var2.initializeGalaxyOnServer(this.state);
            this.galaxyMap.put(new Vector3i(var1), var2);
        }

        return var2;
    }

    public VoidSystem getStellarSystemFromStellarPosIfLoaded(Vector3i var1) throws IOException {
        return (VoidSystem)this.starSystemMap.get(var1);
    }

    public StellarSystem overwriteSystem(Vector3i var1, SectorGenerationInterface var2, boolean var3) throws IOException {
        VoidSystem var4 = this.getStellarSystemFromStellarPosIfLoaded(var1);
        Vector3i var5 = Galaxy.getContainingGalaxyFromSystemPos(var1, this.tmpGalaxy);
        Galaxy var6 = this.getGalaxy(var5);
        if (var4 == null) {
            var4 = this.loadOrGenerateVoidSystem(var1, var6, var2, var3);

            assert var4.getPos().equals(var1);

            this.starSystemMap.put(var4.getPos(), var4);
        } else {
            this.generateSystem(var4, var6, var2, var3);
        }

        return var4;
    }

    public StellarSystem getStellarSystemFromStellarPos(Vector3i var1) throws IOException {
        VoidSystem var2;
        if ((var2 = this.getStellarSystemFromStellarPosIfLoaded(var1)) == null) {
            Vector3i var3 = Galaxy.getContainingGalaxyFromSystemPos(var1, this.tmpGalaxy);
            Galaxy var4 = this.getGalaxy(var3);
            var2 = this.loadOrGenerateVoidSystem(var1, var4);

            assert var2.getPos().equals(var1);

            this.starSystemMap.put(var2.getPos(), var2);
        }

        return var2;
    }

    public boolean isEmpty() {
        return this.sectors.isEmpty();
    }

    public boolean isSectorActive(Vector3i var1) {
        Sector var2;
        return (var2 = (Sector)this.sectorPositions.get(var1)) != null && var2.isActive();
    }

    public boolean isSectorLoaded(Vector3i var1) {
        return this.sectorPositions.containsKey(var1);
    }

    public void loadOrGenerateSector(Vector3i var1) throws IOException, SQLException {
        Vector3i var2 = new Vector3i(var1);
        System.out.println("[SERVER][UNIVERSE] LOADING SECTOR... " + var2);
        long var4 = System.currentTimeMillis();
        Sector var3 = new Sector(this.state);
        long var6 = System.currentTimeMillis() - var4;
        var4 = System.currentTimeMillis();
        boolean var8 = this.state.getDatabaseIndex().getTableManager().getSectorTable().loadSector(var1, var3);
        long var9 = System.currentTimeMillis() - var4;
        long var11 = 0L;
        if (!var8) {
            System.currentTimeMillis();
            (var3 = this.addNewSector(var2)).setNew();
            var3.setLastReplenished(System.currentTimeMillis());
            var3.loadUIDs(this.state);
            System.currentTimeMillis();
        } else {
            var3.setSeed(this.seed + var1.code());
            var3.setRandom(new Random(var3.getSeed()));
            var4 = System.currentTimeMillis();
            var3.loadUIDs(this.state);
            this.addSector(var3);
            var11 = System.currentTimeMillis() - var4;
        }

        var4 = System.currentTimeMillis();
        var3.setActive(true);
        var3.loadEntities(this.state);
        if (var3.isTutorialSector()) {
            var3.mode(SectorMode.LOCK_NO_ENTER, true);
            var3.mode(SectorMode.LOCK_NO_EXIT, true);
        }

        long var13 = System.currentTimeMillis() - var4;
        if (var6 > 10L || var9 > 10L || var11 > 10L || var13 > 10L) {
            System.out.println("[SERVER][UNIVERSE] WARNING: LOADING SECTOR TOOK SOME TIME... " + var2 + ": STATS: inst " + var6 + "ms, loadDB " + var9 + "ms, add " + var11 + "ms, entity " + var13 + "ms");
        }

    }

    private VoidSystem loadOrGenerateVoidSystem(Vector3i var1, Galaxy var2) throws IOException {
        Object var3;
        if (Galaxy.USE_GALAXY) {
            var3 = new SectorGenerationDefault();
        } else {
            var3 = new SectorGenerationNoGalaxies();
        }

        return this.loadOrGenerateVoidSystem(var1, var2, (SectorGenerationInterface)var3, false);
    }

    private VoidSystem loadOrGenerateVoidSystem(Vector3i var1, Galaxy var2, SectorGenerationInterface var3, boolean var4) throws IOException {
        VoidSystem var5 = new VoidSystem();
        if (this.state.getDatabaseIndex().getTableManager().getSystemTable().loadSystem(this.state, var1, var5)) {
            assert var5.getDBId() >= 0L;
        } else {
            var5.getPos().set(var1);
            this.generateSystem(var5, var2, var3, var4);
        }

        return var5;
    }

    public void generateSystem(VoidSystem var1, Galaxy var2, SectorGenerationInterface var3, boolean var4) throws IOException {
        this.systemRandom.setSeed(this.seed + (long)var1.getPos().hashCode());

        ///INSERTED CODE
        SystemPreGenerationEvent e = new SystemPreGenerationEvent(var1,var2,var3,var4);
        StarLoader.fireEvent(SystemPreGenerationEvent.class, e, false);
        var3 = e.getGenerator();
        ///

        var1.generate(this.systemRandom, var2, this.state, var3);

        try {
            long var5 = this.state.getDatabaseIndex().getTableManager().getSystemTable().updateOrInsertSystemIfChanged(var1, var4);
            if (var1.getDBId() < 0L) {
                var1.setDBId(var5);
            }

        } catch (SQLException var7) {
            var7.printStackTrace();
        }
    }

    private void pingSectorBlock(Vector3i var1) throws IOException {
        for(int var2 = var1.x - 1; var2 <= var1.x + 1; ++var2) {
            for(int var3 = var1.y - 1; var3 <= var1.y + 1; ++var3) {
                for(int var4 = var1.z - 1; var4 <= var1.z + 1; ++var4) {
                    this.where.set(var2, var3, var4);
                    this.getSector(this.where).ping();
                }
            }
        }

    }

    public void pingSectors() throws IOException {
        Iterator var1 = this.state.getPlayerStatesByName().values().iterator();

        while(var1.hasNext()) {
            PlayerState var2 = (PlayerState)var1.next();
            this.pingSectorBlock(var2.getCurrentSector());
        }

    }

    public void resetAllSectors() {
        try {
            Sector var1 = this.getSector(new Vector3i(Sector.DEFAULT_SECTOR));
            Sector var2;
            (var2 = new Sector(this.state)).setId(var1.getId());
            var2.pos = new Vector3i(Sector.DEFAULT_SECTOR);
            var2.populate(this.state);
            this.addSector(var2);
            var1.cleanUp();
        } catch (IOException var3) {
            var3.printStackTrace();
        }
    }

    private void updateSector(Sector var1, long var2, int var4, Timer var5, Int2LongOpenHashMap var6) throws IOException {
        long var7 = System.currentTimeMillis();
        Iterator var9;
        if (EngineSettings.N_TRANSMIT_RAW_DEBUG_POSITIONS.isOn()) {
            var9 = var1.getPhysics().getDynamicsWorld().getCollisionObjectArray().iterator();

            while(var9.hasNext()) {
                CollisionObject var10;
                DebugServerPhysicalObject var11;
                if ((var10 = (CollisionObject)var9.next()) instanceof RigidBodySegmentController) {
                    (var11 = new DebugServerPhysicalObject()).setObject((RigidBodySegmentController)var10, var1.getId());
                    this.state.getGameState().getNetworkObject().debugPhysical.add(new RemoteSerializableObject(var11, true));
                } else if (var10 instanceof PairCachingGhostObjectAlignable) {
                    (var11 = new DebugServerPhysicalObject()).setObject(((PairCachingGhostObjectAlignable)var10).getObj());
                    this.state.getGameState().getNetworkObject().debugPhysical.add(new RemoteSerializableObject(var11, true));
                }
            }
        }

        System.currentTimeMillis();
        if (var1.isActive() && var4 >= 0 && var2 - var1.getLastPing() > (long)(var4 * 1000)) {
            boolean var14 = false;
            var9 = this.state.getPlayerStatesByName().values().iterator();

            while(var9.hasNext()) {
                if (((PlayerState)var9.next()).getCurrentSectorId() == var1.getId()) {
                    var14 = true;
                    var1.ping();
                }
            }

            if (!var14) {
                var1.setActive(false);
            }
        }

        System.currentTimeMillis();
        System.currentTimeMillis();
        System.currentTimeMillis();
        System.currentTimeMillis();
        var1.update(var5);
        System.currentTimeMillis();
        System.currentTimeMillis();
        if (!var1.isActive() && var1.hasSectorRemoveTimeout(var2) && var1.isSectorWrittenToDisk() && !this.entityCleaningSectors.contains(var1)) {
            this.inactiveWrittenSectors.add(var1);
        }

        System.currentTimeMillis();
        long var12;
        if ((var12 = System.currentTimeMillis() - var7 + var6.get(var1.getId())) > 50L && this.state.getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(var1.getRemoteSector().getId())) {
            var1.getRemoteSector().announceLag(var12);
        }

    }

    public void update(Timer var1, Int2LongOpenHashMap var2) throws IOException {
        this.lastTime = var1.currentTime;
        long var3;
        if (var1.currentTime - this.lastPing > 500L) {
            var3 = System.currentTimeMillis();
            this.pingSectors();
            long var5;
            if ((var5 = System.currentTimeMillis() - var3) > 10L) {
                System.err.println("[UNIVERSE] WARNING: Sector Ping took " + var5);
            }

            this.lastPing = System.currentTimeMillis();
        }

        int var26;
        for(var26 = 0; var26 < this.deffereedLoads.size(); ++var26) {
            Universe.DeferredLoad var4;
            if ((var4 = (Universe.DeferredLoad)this.deffereedLoads.get(var26)).sector.isActive()) {
                --var4.delay;
                if (var4.delay <= 0) {
                    try {
                        var4.sector.loadEntitiy(this.state, var4.uid);
                    } catch (SQLException var20) {
                        var20.printStackTrace();
                    }

                    this.deffereedLoads.remove(var26--);
                }
            } else {
                this.deffereedLoads.remove(var26--);
            }
        }

        if (var1.currentTime - this.lastAttackCheck > 300000L) {
            LongIterator var29 = this.attackSector.values().iterator();

            while(var29.hasNext()) {
                if (var1.currentTime - var29.nextLong() > 1800000L) {
                    var29.remove();
                }
            }

            this.lastAttackCheck = var1.currentTime;
        }

        Iterator var30 = this.galaxyMap.values().iterator();

        while(var30.hasNext()) {
            ((Galaxy)var30.next()).updateLocal(var1.currentTime);
        }

        var3 = System.currentTimeMillis();
        int var31 = (Integer)ServerConfig.SECTOR_INACTIVE_TIMEOUT.getCurrentState();
        long var6 = System.currentTimeMillis();
        GameServerState.totalSectorCountTmp = 0;
        GameServerState.activeSectorCountTmp = 0;
        Iterator var8 = this.getSectorSet().iterator();

        while(var8.hasNext()) {
            Sector var9 = (Sector)var8.next();
            this.updateSector(var9, var6, var31, var1, var2);
        }

        GameServerState.totalSectorCount = GameServerState.totalSectorCountTmp;
        GameServerState.activeSectorCount = GameServerState.activeSectorCountTmp;
        long var35;
        if ((var35 = System.currentTimeMillis() - var3) > 30L) {
            System.err.println("[UNIVERSE] WARNING: Sector UPDATE took " + var35 + "; sectors updated: " + this.getSectorSet().size());
        }

        if (this.ftlDirty.size() > 0) {
            synchronized(this.ftlDirty) {
                while(!this.ftlDirty.isEmpty()) {
                    Vector3i var11 = (Vector3i)this.ftlDirty.dequeue();
                    this.getGalaxyManager().sendDirectFtlUpdateOnServer(var11);
                }
            }
        }

        if (this.tradeNodesDirty.size() > 0) {
            synchronized(this.tradeNodesDirty) {
                while(!this.tradeNodesDirty.isEmpty()) {
                    long var36 = this.tradeNodesDirty.dequeueLong();
                    this.getGalaxyManager().sendDirectTradeUpdateOnServer(var36);
                }
            }
        }

        long var10 = System.currentTimeMillis();
        if (!this.toClear.isEmpty()) {
            synchronized(this.toClear) {
                if (!this.toClear.isEmpty()) {
                    this.toClear.dequeue();
                }
            }
        }

        int var12 = this.inactiveWrittenSectors.size();
        Iterator var23 = this.inactiveWrittenSectors.iterator();

        Sector var13;
        ObjectArrayList var33;
        while(var23.hasNext()) {
            Sector var25 = (Sector)var23.next();
            var13 = (Sector)this.sectorPositions.remove(var25.pos);
            Sector sector = (Sector)this.sectors.remove(var25.getId());
            System.err.println("[SECTOR][CLEANUP] removing entities and " + var25 + ": " + var13 + "; " + sector);

            assert var13 != null && sector != null;

            var33 = var25.updateEntities();
            long var15 = System.currentTimeMillis();
            //INSERTED CODE
            SectorUnloadEvent event = new SectorUnloadEvent(var33, var25);
            StarLoader.fireEvent(event, true);
            ///

            SimpleTransformableSendableObject var18;
            for(Iterator var17 = var33.iterator(); var17.hasNext(); var18.setMarkedForDeleteVolatile(true)) {
                if (!(var18 = (SimpleTransformableSendableObject)var17.next()).isWrittenForUnload()) {
                    System.err.println("[SECTOR][CLEANUP] Warning: written flag was not set for " + var18);
                }
                if (var18 instanceof SendableSegmentController) {
                    SendableSegmentController var34 = (SendableSegmentController)var18;
                    //INSERTED CODE
                    SegmentControllerUnloadEvent event2 = new SegmentControllerUnloadEvent(var34);
                    StarLoader.fireEvent(event2, true);
                    ///
                    var34.onClear();
                    var34.getSegmentBuffer().size();
                    var34.getSegmentBuffer().clear(true);
                    if (var34 instanceof ManagedSegmentController) {
                        ((ManagedSegmentController)var34).getManagerContainer().clear();
                    }

                    var34.getSegmentProvider().releaseFileHandles();
                }
            }

            long var40;
            if ((var40 = System.currentTimeMillis() - var15) > 30L) {
                System.err.println("[SERVER] WARNING: clearing written objects of sector " + var25 + " took " + var40 + " ms");
            }

            this.entityCleaningSectors.add(var25);
            var26 = var25.getId();
            var25 = null;
            Iterator var28 = this.state.getPlayerStatesByName().values().iterator();

            while(var28.hasNext()) {
                PlayerState var32;
                if ((var32 = (PlayerState)var28.next()).getCurrentSectorId() == var26) {
                    if (var25 == null) {
                        var25 = this.getSector(var32.getCurrentSector());
                    }

                    if (!var25.pos.equals(var32.getCurrentSector())) {
                        throw new IllegalArgumentException("Sector invalid for player: " + var32 + " has " + var32.getCurrentSectorId() + " -> " + var32.getCurrentSector() + "; new sector: " + var25);
                    }

                    var32.setCurrentSectorId(var25.getId());
                }
            }
        }

        this.inactiveWrittenSectors.clear();
        int var24 = this.entityCleaningSectors.size();
        ObjectIterator var27 = this.entityCleaningSectors.iterator();

        while(true) {
            while(var27.hasNext()) {
                var13 = (Sector)var27.next();
                boolean var38 = false;
                Iterator var39 = (var33 = var13.updateEntities()).iterator();

                SimpleTransformableSendableObject var16;
                while(var39.hasNext()) {
                    var16 = (SimpleTransformableSendableObject)var39.next();
                    if (this.state.getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(var16.getId())) {
                        var38 = true;
                        break;
                    }
                }

                if (!var38) {
                    var13.cleanUp();
                    var13.getRemoteSector().setMarkedForDeleteVolatile(true);
                    var13.terminated = true;
                    var27.remove();
                } else {
                    System.err.println("[SERVER] waiting for sector " + var13 + " entities to be cleaned up " + var33);
                    var39 = var33.iterator();

                    while(var39.hasNext()) {
                        if (!(var16 = (SimpleTransformableSendableObject)var39.next()).isMarkedForDeleteVolatile()) {
                            System.err.println("[SERVER] not cleaned up: " + var16 + "; deleteSent: " + var16.isMarkedForDeleteVolatileSent());
                        }
                    }
                }
            }

            long var37;
            if ((var37 = System.currentTimeMillis() - var10) > 5L) {
                System.err.println("[UNIVERSE] WARNING SECTOR CLEANUP TIME: " + var37 + "; QUEUE: " + var24 + "; InactSectors: " + var12);
            }

            this.galaxyManager.updateServer();
            DebugControlManager.requestPhysicsCheck = false;
            return;
        }
    }

    public void updateProximitySectorInformation(Vector3i var1) throws IOException {
        Vector3i[] var2;
        (var2 = new Vector3i[8])[0] = new Vector3i(var1.x + 8, var1.y + 8, var1.z + 8);
        var2[1] = new Vector3i(var1.x - 8, var1.y + 8, var1.z + 8);
        var2[2] = new Vector3i(var1.x + 8, var1.y - 8, var1.z + 8);
        var2[3] = new Vector3i(var1.x - 8, var1.y - 8, var1.z + 8);
        var2[4] = new Vector3i(var1.x + 8, var1.y + 8, var1.z - 8);
        var2[5] = new Vector3i(var1.x - 8, var1.y + 8, var1.z - 8);
        var2[6] = new Vector3i(var1.x + 8, var1.y - 8, var1.z - 8);
        var2[7] = new Vector3i(var1.x - 8, var1.y - 8, var1.z - 8);

        for(int var3 = 0; var3 < var2.length; ++var3) {
            this.getStellarSystemFromSecPos(var2[var3]);
        }

    }

    public static void write(DiskWritable var0, String var1) throws IOException {
        EntityFileTools.write(GameServerState.fileLocks, var0, GameServerState.ENTITY_DATABASE_PATH, var1);
    }

    private void writeAdditionalEntities() throws IOException {
        if (this.state != null && this.state.getGameState() != null) {
            System.err.println("[SERVER] WRITING ADDITIONAL ENTITIES");
            write(this.state.getGameState().getFactionManager(), "FACTIONS.fac");
            this.state.getCatalogManager().writeToDisk();
            Iterator var1 = this.galaxyMap.values().iterator();

            while(var1.hasNext()) {
                Galaxy var2 = (Galaxy)var1.next();
                EntityFileTools.write(GameServerState.fileLocks, var2.getNpcFactionManager(), GameServerState.ENTITY_DATABASE_PATH, var2.getNpcFactionManager().getFileNameTag());
            }

            write(this.state.getChannelRouter(), "chatchannels.tag");
            write(this.state.getGameState().getTradeManager(), "TRADING.tag");
            System.err.println("[SERVER] WRITING ADDITIONAL ENTITIES DONE");
        }

    }

    private void writeSimulationState() throws IOException {
        this.state.getSimulationManager().writeToDatabase();
        write(this.state.getSimulationManager(), this.state.getSimulationManager().getUniqueIdentifier() + ".sim");
    }

    public void writeStarSystems() throws IOException, SQLException {
        System.currentTimeMillis();
        Iterator var1 = this.starSystemMap.values().iterator();

        while(var1.hasNext()) {
            StellarSystem var2;
            if ((var2 = (StellarSystem)var1.next()).isChanged()) {
                this.state.getDatabaseIndex().getTableManager().getSystemTable().updateOrInsertSystemIfChanged(var2, false);
            }
        }

    }

    public void writeToDatabase(boolean var1, boolean var2) throws IOException, SQLException {
        this.writeAdditionalEntities();
        Iterator var3 = this.sectors.values().iterator();

        while(true) {
            Sector var4;
            do {
                if (!var3.hasNext()) {
                    this.writeStarSystems();
                    this.writeSimulationState();

                    try {
                        this.state.getMetaObjectManager().save();
                    } catch (Exception var5) {
                        var5.printStackTrace();
                    }

                    if (var1) {
                        try {
                            this.state.getThreadPoolLogins().shutdown();
                            System.out.println("[SERVER] WAITING FOR LOGIN THREAD POOL TO TERMINATE; Active: " + this.state.getThreadPoolLogins().getActiveCount());
                            this.state.getThreadPoolLogins().awaitTermination(5L, TimeUnit.SECONDS);
                            System.out.println("[SERVER] SERVER LOGIN THREAD POOL TERMINATED");
                            System.out.println("[SERVER] KILLING ACTIVE EXPLOSION THREADS; Active: " + this.state.getTheadPoolExplosions().getActiveCount());
                            this.state.getTheadPoolExplosions().shutdownNow();
                            this.state.getThreadQueue().shutdown();
                            System.out.println("[SERVER] WAITING FOR WRITING QUEUE TO FINISH; Active: " + this.state.getThreadQueue().getActiveCount());

                            while(this.state.getThreadQueue().getActiveCount() > 0) {
                                Thread.sleep(300L);
                            }

                            System.out.println("[SERVER] SERVER THREAD QUEUE TERMINATED");
                            return;
                        } catch (InterruptedException var6) {
                            var6.printStackTrace();
                        }
                    }

                    return;
                }
            } while(!(var4 = (Sector)var3.next()).isActive() && var4.isSectorWrittenToDisk() && !var1);

            if (var1) {
                var4.writeToDisk(1, var2, true, this);
            } else if (var2) {
                var4.writeToDisk(2, true, false, this);
            } else {
                var4.writeToDisk(0, false, false, this);
            }
        }
    }

    public long getSeed() {
        return this.seed;
    }

    public void setSeed(long var1) {
        this.seed = var1;
    }

    public GalaxyManager getGalaxyManager() {
        return this.galaxyManager;
    }

    public void onShutdown() {
        if (this.galaxyManager != null) {
            this.galaxyManager.shutdown();
        }

    }

    public FactionResourceRequestContainer updateSystemResourcesWithDatabaseValues(Vector3i var1, Galaxy var2, FactionResourceRequestContainer var3, GalaxyTmpVars var4) {
        VoidSystem var5;
        if ((var5 = (VoidSystem)this.starSystemMap.get(var1)) == null) {
            if (!this.state.getDatabaseIndex().getTableManager().getSystemTable().updateSystemResources(var1, var3)) {
                var3 = var2.getSystemResources(var1, var3, var4);
            }
        } else {
            var5.updateSystemResources(var3);
        }

        return var3;
    }

    public void onRemovedSectorSynched(Sector var1) {
        this.state.getController().onSectorRemovedSynch(var1);
    }

    public void onAddedSectorSynched(Sector var1) {
        Iterator var2 = this.state.getFactionManager().getFactionCollection().iterator();

        while(var2.hasNext()) {
            ((Faction)var2.next()).onAddedSectorSynched(var1);
        }

        this.state.getController().onSectorAddedSynch(var1);
    }

    public ObjectCollection<Galaxy> getGalaxies() {
        return this.galaxyMap.values();
    }

    public void scheduleDefferedLoad(Sector var1, EntityUID var2, int var3) {
        Universe.DeferredLoad var4 = new Universe.DeferredLoad(var1, var2, var3);
        this.deffereedLoads.add(var4);
    }

    public void attackInSector(Vector3i var1) {
        this.attackSector.put(var1, this.lastTime);
    }

    static {
        resetMinableAndVulnerable = ServerConfig.HOST_NAME_TO_ANNOUNCE_TO_SERVER_LIST.getCurrentState().toString().startsWith("play.star-made.org");
    }

    class DeferredLoad {
        Sector sector;
        EntityUID uid;
        int delay;

        public DeferredLoad(Sector var2, EntityUID var3, int var4) {
            this.sector = var2;
            this.uid = var3;
            this.delay = var4;
        }
    }

    public static enum SystemOwnershipType {
        NONE,
        BY_ALLY,
        BY_ENEMY,
        BY_SELF,
        BY_NEUTRAL;

        private SystemOwnershipType() {
        }

        public final int getMiningBonusMult() {
            switch(this) {
                case BY_ALLY:
                    return (int)FactionSystemOwnerBonusConfig.MINING_BONUS_OTHERS;
                case BY_ENEMY:
                    return (int)FactionSystemOwnerBonusConfig.MINING_BONUS_OTHERS;
                case BY_NEUTRAL:
                    return (int)FactionSystemOwnerBonusConfig.MINING_BONUS_OTHERS;
                case BY_SELF:
                    return (int)FactionSystemOwnerBonusConfig.MINING_BONUS_OWNER;
                case NONE:
                    return (int)FactionSystemOwnerBonusConfig.MINING_BONUS_UNOWNED;
                default:
                    return 1;
            }
        }
    }
}
