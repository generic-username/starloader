//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.schema.game.common.data.world;

import api.listener.events.world.PlanetTypeSelectEvent;
import api.mod.StarLoader;
import com.bulletphysics.linearmath.Transform;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.forms.BoundingBox;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.util.Random;


@Deprecated
public class SectorInformation {
    public static Vector3f tmpAxis = new Vector3f();
    public static Vector3i tmpTT = new Vector3i();
    public static Vector3i tmp = new Vector3i();
    public static Vector3i tmpOut = new Vector3i();
    public static Vector3i tmpOutGal = new Vector3i();
    public static int[] orbits = new int[8];
    public static Transform orbitRot = new Transform();

    public SectorInformation() {
    }

    public static boolean generateOrbits(GameServerState var0, Galaxy var1, int var2, int var3, int var4, StellarSystem var5, int var6, Random var7, SectorGenerationInterface var8) {
        Vector3i var9 = Galaxy.getRelPosInGalaxyFromAbsSystem(var5.getPos(), tmpOut);
        var1.getSystemOrbits(var9, orbits);
        var1.getSystemType(var9);
        Vector3i var10 = var1.getSunPositionOffset(var9, tmpTT);
        var1.getAxisMatrix(var9, orbitRot.basis);
        Vector3f var15 = var1.getSystemAxis(var9, tmpAxis);
        tmp.set(var2, var3, var4);
        Vector3i var10000 = tmp;
        Vector3f var17 = new Vector3f((float)var10.x * 6.25F, (float)var10.y * 6.25F, (float)var10.z * 6.25F);
        float var11 = (float)VoidSystem.localCoordinate(var2);
        float var12 = (float)VoidSystem.localCoordinate(var3);
        float var13 = (float)VoidSystem.localCoordinate(var4);
        Vector3f var14 = new Vector3f(-50.0F + var11 * 6.25F, -50.0F + var12 * 6.25F, -50.0F + var13 * 6.25F);
        Vector3f var18 = new Vector3f(-50.0F + var11 * 6.25F + 6.25F, -50.0F + var12 * 6.25F + 6.25F, -50.0F + var13 * 6.25F + 6.25F);
        BoundingBox var19 = new BoundingBox(var14, var18);
        if (BoundingBox.intersectsPlane(var17, var15, var19)) {
            for(int var16 = 0; var16 < orbits.length; ++var16) {
                int var20;
                if ((var20 = orbits[var16]) > 0) {
                    var13 = (float)(var16 + 1) * 6.25F + 3.125F;
                    (var14 = new Vector3f()).sub(var19.getCenter(new Vector3f()), var17);
                    var14.normalize();
                    var14.scale(var13);
                    var14.add(var17);
                    if (var19.isInside(var14)) {
                        if (!Galaxy.isPlanetOrbit(var20)) {
                            var8.onAsteroidBelt(var0, var2, var3, var4, var5, var6, var1, var7);
                            return true;
                        }

                        if (var5.isOrbitTakenByGeneration(var16)) {
                            if (var8.orbitTakenByGeneration(var0, var2, var3, var4, var5, var6, var1, var7)) {
                                return true;
                            }
                        } else {
                            if (var5.getOrbitTakenIteration(var16) >= var5.getOrbitWait(var16)) {
                                var5.setOrbitTakenByGeneration(var16);
                                var8.definitePlanet(var0, var2, var3, var4, var5, var6, var1, var7);
                                return true;
                            }

                            var5.incrementOrbit(var16);
                            if (var8.onOrbitButNoPlanet(var0, var2, var3, var4, var5, var6, var1, var7)) {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    public static void generate(GameServerState var0, Galaxy var1, int var2, int var3, int var4, StellarSystem var5, int var6, Random var7, SectorGenerationInterface var8) {
        if (var5.getSectorType(var6) != SectorInformation.SectorType.SUN && var5.getSectorType(var6) != SectorInformation.SectorType.DOUBLE_STAR && var5.getSectorType(var6) != SectorInformation.SectorType.GIANT) {
            if (!var8.staticSectorGeneration(var0, var2, var3, var4, var5, var6, var1, var7)) {
                var8.generate(var0, var2, var3, var4, var5, var6, var1, var7);
                boolean var10000 = Galaxy.USE_GALAXY;
            }
        }
    }

    public static boolean isPlanetSpotTaken(int var0, int var1, int var2, StellarSystem var3) {
        for(int var4 = var0 - 1; var4 < var0 + 2; ++var4) {
            for(int var5 = var1 - 1; var5 < var1 + 2; ++var5) {
                for(int var6 = var2 - 1; var6 < var2 + 2; ++var6) {
                    if (var3.getSectorType(var3.getIndex(Math.max(0, Math.min(15, var4)), Math.max(0, Math.min(15, var5)), Math.max(0, Math.min(15, var6)))) == SectorInformation.SectorType.PLANET) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public static void generatePlanet(int var0, int var1, int var2, StellarSystem var3, int var4, Random var5) {
        ///INSERTED CODE
        int originalX = var0;
        ///
        for(var0 = var5.nextInt(SectorInformation.PlanetType.values().length);
            !SectorInformation.PlanetType.values()[var0].allowedSpawn;
            var0 = var5.nextInt(SectorInformation.PlanetType.values().length)) { //...do nothing? Why are there even brackets here?
        }

        ///INSERTED CODE
        PlanetType type = SectorInformation.PlanetType.values()[var0];

        PlanetTypeSelectEvent e = new PlanetTypeSelectEvent(originalX,var1,var2, var3,var4, type);
        StarLoader.fireEvent(PlanetTypeSelectEvent.class, e, false);
        type = e.getPlanetType();

        var3.setPlanetType(var4, type);
        ///
    }

    public static enum SectorType {
        SPACE_STATION(0),
        ASTEROID(8),
        PLANET(0),
        MAIN(0),
        SUN(0),
        BLACK_HOLE(0),
        VOID(0),
        LOW_ASTEROID(2),
        GIANT(0),
        DOUBLE_STAR(2);

        private final int asteroidCountMax;

        private SectorType(int var3) {
            this.asteroidCountMax = var3;
        }

        public final int getAsteroidCountMax() {
            return this.asteroidCountMax;
        }
    }

    public static enum PlanetType {
        MARS(true, "planet-mars-diff", "planet-earth-clouds", "planet-earth-normal", "planet-earth-specular", new Vector4f(1.0F, 0.2F, 0.2F, 0.8F), new Vector4f(1.0F, 0.2F, 0.2F, 1.0F)),
        EARTH(true, "planet-earth-diff", "planet-earth-clouds", "planet-earth-normal", "planet-earth-specular", new Vector4f(0.39215F, 0.584313F, 0.92941F, 0.8F), new Vector4f(0.39215F, 0.584313F, 0.92941F, 1.0F)),
        DESERT(true, "planet-desert-diff", "planet-earth-clouds", "planet-earth-normal", "planet-earth-specular", new Vector4f(0.61F, 0.8F, 1.0F, 1.0F), new Vector4f(0.61F, 0.8F, 1.0F, 1.0F)),
        PURPLE(true, "planet-purple-diff", "planet-earth-clouds", "planet-earth-normal", "planet-earth-specular", new Vector4f(0.2781F, 0.7411F, 0.2F, 1.0F), new Vector4f(0.2781F, 0.7411F, 0.2F, 1.0F)),
        ICE(true, "planet-ice-diff", "planet-earth-clouds", "planet-earth-normal", "planet-earth-specular", new Vector4f(0.8F, 0.8F, 0.8F, 0.3F), new Vector4f(0.8F, 0.8F, 0.8F, 0.9F));

        public final String diff;
        public final String clouds;
        public final String normal;
        public final String specular;
        public final Vector4f atmosphere;
        public final Vector4f atmosphereInner;
        public boolean allowedSpawn;

        private PlanetType(boolean var3, String var4, String var5, String var6, String var7, Vector4f var8, Vector4f var9) {
            this.diff = var4;
            this.clouds = var5;
            this.normal = var6;
            this.specular = var7;
            this.allowedSpawn = var3;
            this.atmosphere = var8;
            this.atmosphereInner = var9;
        }
    }
}
