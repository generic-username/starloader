//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.schema.game.common.data.player;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import org.schema.common.FastMath;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.HitType;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.controller.elements.beam.BeamCommand;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.beam.BeamReloadCallback;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.weapon.GrappleBeam;
import org.schema.game.common.data.element.meta.weapon.SniperRifle;
import org.schema.game.common.data.element.meta.weapon.TorchBeam;
import org.schema.game.common.data.element.meta.weapon.Weapon;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.CharacterBlockActivation;
import org.schema.game.network.objects.NetworkPlayerHumanCharacter;
import org.schema.game.network.objects.remote.RemoteCharacterBlockActivation;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.NetworkGravity;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.Tag.Type;

import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import java.util.ArrayList;
import java.util.Iterator;


//Not ported due to no hooks
public class PlayerCharacter extends AbstractCharacter<PlayerState> implements PlayerControllable {
    public static final float headUpScale = 0.685F;
    public static final float shoulderUpScale = 0.385F;
    private final ArrayList<PlayerState> attachedPlayers = new ArrayList();
    public LongArrayList waitingForToSpawn = new LongArrayList();
    public int spawnOnObjectId;
    public Vector3f spawnOnObjectLocalPos;
    boolean wasMouseDown = false;
    Vector3f shootingDirTemp = new Vector3f(0.0F, 1.0F, 0.0F);
    float yaw = 0.0F;
    private float characterWidth = 0.2F;
    private float characterHeight = 1.13F;
    private float characterMargin = 0.1F;
    private float characterHeightOffset = 0.2F;
    private NetworkPlayerHumanCharacter networkPlayerCharacterObject;
    private int clientOwnerId;
    private PlayerState ownerState;
    private BeamReloadCallback beamReloadCallback = new BeamReloadCallback() {
        public void setShotReloading(long var1) {
        }

        public boolean canUse(long var1, boolean var3) {
            return true;
        }

        public boolean isInitializing(long var1) {
            return false;
        }

        public long getNextShoot() {
            return 0L;
        }

        public long getCurrentReloadTime() {
            return 0L;
        }

        public boolean consumePower(float var1) {
            return true;
        }

        public boolean canConsumePower(float var1) {
            return true;
        }

        public double getPower() {
            return 1.0D;
        }

        public boolean isUsingPowerReactors() {
            return true;
        }

        public void flagBeamFiredWithoutTimeout() {
        }
    };
    private Vector3f up = new Vector3f(0.0F, 1.0F, 0.0F);
    private Vector3f down = new Vector3f(0.0F, -1.0F, 0.0F);
    private float speed = 4.0F;
    private Vector3i blockDim = new Vector3i(1, 1, 1);

    public PlayerCharacter(StateInterface var1) {
        super(var1);
    }

    public EntityType getType() {
        return EntityType.ASTRONAUT;
    }

    public void handleKeyEvent(ControllerStateUnit var1, KeyboardMappings var2) {
    }

    public void cleanUpOnEntityDelete() {
        super.cleanUpOnEntityDelete();
        System.err.println("[DELETE] Cleaning up playerCharacter for playerState " + this.getOwnerState() + " on " + this.getState() + "; " + this.getUniqueIdentifier() + "; " + this);
        GameClientState var1;
        if (this.getState() instanceof GameClientState && (var1 = (GameClientState)this.getState()).getCharacter() != null && var1.getCharacter().getId() == this.getId()) {
            var1.getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PLAYERCHARACTER_0, 0.0F);
            var1.setCharacter((PlayerCharacter)null);
            var1.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().exitShip(true);
            System.err.println("[DELETE] Deactivating character control manager for: " + this + "; (setting spawn screen for attached player)");
            var1.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().setActive(false);
            var1.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().setActive(false);
            var1.getGlobalGameControlManager().getIngameControlManager().getAutoRoamController().setActive(true);
            var1.setCurrentPlayerObject((SimpleTransformableSendableObject)null);
        }

    }

    public void fromTagStructure(Tag var1) {
        assert "PlayerCharacter".equals(this.getClass().getSimpleName());

        Tag[] var2 = (Tag[])var1.getValue();
        this.setId((Integer)var2[0].getValue());
        super.fromTagStructure(var2[3]);
    }

    public Tag toTagStructure() {
        Tag var1 = new Tag(Type.INT, "id", this.getId());
        Tag var2 = new Tag(Type.FLOAT, "speed", this.getSpeed());
        Tag var3 = new Tag(Type.FLOAT, "stepHeight", this.stepHeight);
        return new Tag(Type.STRUCT, "PlayerCharacter", new Tag[]{var1, var2, var3, super.toTagStructure(), FinishTag.INST});
    }

    public String toNiceString() {
        return this.getAttachedPlayers().isEmpty() ? "ghost <bug: playerCharacter without host>" : ((PlayerState)this.getAttachedPlayers().get(0)).getName();
    }

    public String getRealName() {
        return this.ownerState != null ? this.ownerState.getName() : "(unknown)";
    }

    public int getClientOwnerId() {
        return this.clientOwnerId;
    }

    public void setClientOwnerId(int var1) {
        this.clientOwnerId = var1;
    }

    public void handleMouseEvent(ControllerStateUnit var1, MouseEvent var2) {
    }

    public void handleControl(Timer var1, ControllerStateInterface var2) {
        if (var2 instanceof ControllerStateUnit) {
            ControllerStateUnit var7;
            (var7 = (ControllerStateUnit)var2).playerState.getShopsInDistance().addAll(this.getShopsInDistance());
            if ((Boolean)var7.playerState.getNetworkObject().activeControllerMask.get(0).get() && var7.isUnitInPlayerSector()) {
                if (!this.wasMouseDown) {
                    this.handleMovingInput(var1, var7);
                    byte var3 = 1;
                    byte var4 = 0;
                    if (var7.playerState.getNetworkObject().mouseSwitched.get()) {
                        var4 = 1;
                        var3 = 0;
                    }

                    this.handle(var7, var1, var7.playerState.isMouseButtonDown(var3));
                    Inventory var5 = var7.playerState.getInventory((Vector3i)null);
                    int var6 = var7.playerState.getSelectedBuildSlot();
                    if (!var5.isSlotEmpty(var6) && var5.getType(var6) < 0) {
                        int var8 = var5.getMeta(var6);
                        MetaObject var9;
                        if ((var9 = ((MetaObjectState)this.getState()).getMetaObjectManager().getObject(var8)) != null) {
                            var9.onMouseAction(this, var7, var4, var3, var1);
                        }
                    }

                } else {
                    this.wasMouseDown = var7.playerState.isMouseButtonDown(0) || var7.playerState.isMouseButtonDown(1);
                }
            } else {
                this.wasMouseDown = var7.playerState.isMouseButtonDown(0) || var7.playerState.isMouseButtonDown(1);
            }
        }
    }

    public void onAttachPlayer(PlayerState var1, Sendable var2, Vector3i var3, Vector3i var4) {
        this.lastAttach = System.currentTimeMillis();
        this.setHidden(false);
        if (this.isClientOwnObject()) {
            this.getNetworkObject().hidden.set(false);
        }

        this.onPhysicsRemove();
        Transform var5 = new Transform(this.getWorldTransform());
        if (var2 != null) {
            if (this.getSectorId() != ((SimpleTransformableSendableObject)var2).getSectorId()) {
                if (!this.isOnServer()) {
                    ((GameClientState)this.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PLAYERCHARACTER_1, new Object[]{this.getSectorId(), ((SimpleTransformableSendableObject)var2).getSectorId()}), 0.0F);
                } else {
                    var1.sendServerMessage(new ServerMessage(new Object[]{301, this.getSectorId(), ((SimpleTransformableSendableObject)var2).getSectorId()}, 3, var1.getId()));
                }

                System.err.println("[PLAYERCHARACTER][DETACHED][" + this.getState() + "] Exception while SETTING SPAWNING POINT FOR " + this + ": (from " + var2 + ") SECTOR DIFF: " + this.getSectorId() + " / " + ((SimpleTransformableSendableObject)var2).getSectorId() + "; playerState sector: " + var1.getId());
                this.setSectorId(((SimpleTransformableSendableObject)var2).getSectorId());
            }

            var5.set(((SimpleTransformableSendableObject)var2).getWorldTransform());
            if (this.isClientOwnObject() && var2 instanceof SegmentController && this.getGravity().source == null) {
                this.scheduleGravity(new Vector3f(0.0F, 0.0F, 0.0F), ((SegmentController)var2).railController.getRoot());
            }
        } else if (var1.getControllerState().getLastTransform() != null) {
            System.err.println(this.getState() + "[PLAYERCHARACTER] USING LAST TRANSFORM ON ATTACH PLAYER " + var1.getControllerState().getLastTransform().origin);
            var5.set(var1.getControllerState().getLastTransform());
        }

        if (var3 != null) {
            Vector3f var6 = new Vector3f((float)var3.x, (float)var3.y, (float)var3.z);
            if (var4 != null) {
                var6.x += (float)var4.x;
                var6.y += (float)var4.y;
                var6.z += (float)var4.z;
            }

            if (var2 != null && var2 instanceof SegmentController) {
                var6.x -= 16.0F;
                var6.y -= 16.0F;
                var6.z -= 16.0F;
            }

            var5.basis.transform(var6);
            var5.origin.add(var6);
        }

        this.getInitialTransform().set(var5);
        this.initPhysics();
        if (!this.isOnServer() && ((GameClientState)this.getState()).getPlayer() == var1) {
            this.getState().needsNotify(this);
            if (!((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().isActive()) {
                ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().setActive(true);
            }

            ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getPlayerCharacterManager().setActive(true);
        }

        if (var1.isClientOwnPlayer()) {
            ((GameClientState)this.getState()).getController().queueBackgroundAudio("0022_ambience loop - space wind omnious light warbling tones (loop)", 0.35F);
        }
        //INSERTED CODE

        ///

        this.flagGravityUpdate = true;
    }

    public void onDetachPlayer(PlayerState var1, boolean var2, Vector3i var3) {
        if (!this.isHidden()) {
            this.setHidden(var2);
            if (this.isClientOwnObject()) {
                this.getNetworkObject().hidden.set(var2);
            }

            this.onPhysicsRemove();
        }

        if (!this.isOnServer() && ((GameClientState)this.getState()).getPlayer() == var1) {
            this.getState().needsNotify(this);
            ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getPlayerCharacterManager().setActive(false);
        }

        if (var1.isClientOwnPlayer()) {
            Controller.getAudioManager().stopBackgroundMusic();
        }

    }

    public boolean hasSpectatorPlayers() {
        Iterator var1 = this.attachedPlayers.iterator();

        do {
            if (!var1.hasNext()) {
                return false;
            }
        } while(!((PlayerState)var1.next()).isSpectator());

        return true;
    }

    public void handle(ControllerStateUnit var1, Timer var2, boolean var3) {
        if ((Boolean)var1.playerState.getNetworkObject().activeControllerMask.get(0).get() && var1.isUnitInPlayerSector()) {
            if (var3) {
                boolean var4 = false;
                if (!this.getOwnerState().getInventory().isSlotEmpty(this.getOwnerState().getSelectedBuildSlot()) && this.getOwnerState().getInventory().getType(this.getOwnerState().getSelectedBuildSlot()) < 0) {
                    int var5 = this.getOwnerState().getInventory().getMeta(this.getOwnerState().getSelectedBuildSlot());
                    MetaObject var6;
                    if ((var6 = ((MetaObjectState)this.getState()).getMetaObjectManager().getObject(var5)) != null && var6 instanceof Weapon) {
                        var4 = true;
                    }
                }

                if (!var4) {
                    this.shootFabricatorBeam(var1);
                }
            }

        }
    }

    public void shootFabricatorBeam(ControllerStateUnit var1) {
        this.shootBeam(var1, 0, 0.0F, (MetaObject)null, false, true);
    }

    private void shootBeam(ControllerStateUnit var1, int var2, float var3, MetaObject var4, boolean var5, boolean var6) {
        Vector3f var9 = new Vector3f();
        Vector3f var10 = new Vector3f();
        if (this.needsYaw()) {
            var1.playerState.getForward(this.shootingDirTemp);
            this.shootingDirTemp.scale(2.0F);
        }

        Vector3f var8;
        if (var2 == 0) {
            var8 = new Vector3f(-0.2F, 0.18F, 0.2F);
        } else {
            var8 = new Vector3f(0.0F, 0.32F, 0.0F);
        }

        Transform var7 = new Transform();
        if (this.isOnServer()) {
            var7.set(this.getWorldTransform());
        } else {
            var7.set(this.getWorldTransformOnClient());
        }

        var7.transform(var9);
        Vector3f var11 = new Vector3f(var8);
        this.getYawRotation().transform(var11);
        var9.add(var11);
        var10.set(this.getShoulderWorldTransform().origin);
        var10.add(this.shootingDirTemp);
        BeamCommand var12;
        (var12 = new BeamCommand()).currentTime = System.currentTimeMillis();
        var12.beamType = var2;
        var12.identifier = 0L;
        var12.relativePos.set(var8);
        var12.reloadCallback = this.beamReloadCallback;
        var12.from.set(var9);
        var12.to.set(var10);
        var12.originMetaObject = var4;
        var12.playerState = this.getOwnerState();
        var12.beamTimeout = 0.1F;
        var12.tickRate = 0.7F;
        var12.controllerPos = null;
        var12.beamPower = var3;
        var12.hitType = var4 instanceof Weapon ? ((Weapon)var4).getHitType() : HitType.GENERAL;
        var12.cooldownSec = 0.0F;
        var12.bursttime = -1.0F;
        var12.initialTicks = 0.0F;
        var12.powerConsumedByTick = 0.0F;
        var12.powerConsumedExtraByTick = 0.0F;
        var12.weaponId = var4 != null ? (long)var4.getId() : -100100L;
        var12.handheld = true;
        var12.minEffectiveRange = 1.0F;
        var12.minEffectiveValue = 1.0F;
        var12.maxEffectiveRange = 1.0F;
        var12.maxEffectiveValue = 1.0F;
        var12.latchOn = false;
        var12.checkLatchConnection = false;
        var12.firendlyFire = true;
        var12.penetrating = false;
        var12.acidDamagePercent = 0.0F;
        this.getHandler().addBeam(var12);
    }

    public void shootHealingBeam(ControllerStateUnit var1, float var2, MetaObject var3, boolean var4, boolean var5) {
        this.shootBeam(var1, 1, var2, var3, var4, var5);
    }

    public void shootSniperBeam(ControllerStateUnit var1, float var2, float var3, float var4, MetaObject var5, boolean var6, boolean var7) {
        this.shootWeaponBeam(var1, var2, var3, var4, var5, var6, var7, 4, ((SniperRifle)var5).reloadCallback);
    }

    public void shootGrappleBeam(ControllerStateUnit var1, float var2, float var3, float var4, MetaObject var5, boolean var6, boolean var7) {
        this.shootWeaponBeam(var1, var2, var3, var4, var5, var6, var7, 5, ((GrappleBeam)var5).reloadCallback);
    }

    public void shootTorchBeam(ControllerStateUnit var1, float var2, float var3, float var4, MetaObject var5, boolean var6, boolean var7) {
        this.shootWeaponBeam(var1, var2, var3, var4, var5, var6, var7, 6, ((TorchBeam)var5).reloadCallback);
    }

    public void shootWeaponBeam(ControllerStateUnit var1, float var2, float var3, float var4, MetaObject var5, boolean var6, boolean var7, int var8, BeamReloadCallback var9) {
        Vector3f var14 = new Vector3f();
        Vector3f var15 = new Vector3f();
        if (this.needsYaw()) {
            var1.playerState.getForward(this.shootingDirTemp);
            this.shootingDirTemp.scale(var4);
        }

        Vector3f var10 = new Vector3f(0.0F, 0.32F, 0.0F);
        Transform var11;
        (var11 = new Transform()).set(this.getShoulderWorldTransform());
        var11.transform(var14);
        Vector3f var12 = new Vector3f(var10);
        this.getYawRotation().transform(var12);
        var14.add(var12);
        var15.set(this.getShoulderWorldTransform().origin);
        var15.add(this.shootingDirTemp);
        BeamCommand var13;
        (var13 = new BeamCommand()).currentTime = System.currentTimeMillis();
        var13.beamType = var8;
        var13.identifier = 0L;
        var13.relativePos.set(var10);
        var13.reloadCallback = var9;
        var13.from.set(var14);
        var13.to.set(var15);
        var13.originMetaObject = var5;
        var13.playerState = this.getOwnerState();
        var13.beamTimeout = 0.1F;
        var13.tickRate = 1.55F;
        var13.hitType = var5 instanceof Weapon ? ((Weapon)var5).getHitType() : HitType.GENERAL;
        var13.controllerPos = null;
        var13.beamPower = var2;
        var13.cooldownSec = var3;
        var13.bursttime = 0.1F;
        var13.initialTicks = 1.0F;
        var13.powerConsumedByTick = 0.0F;
        var13.powerConsumedExtraByTick = 0.0F;
        var13.weaponId = (long)var5.getId();
        var13.handheld = true;
        var13.ignoreShields = var5 instanceof Weapon && ((Weapon)var5).isIgnoringShields();
        var13.ignoreArmor = var5 instanceof Weapon && ((Weapon)var5).isIgnoringArmor();
        var13.minEffectiveRange = 1.0F;
        var13.minEffectiveValue = 1.0F;
        var13.maxEffectiveRange = 1.0F;
        var13.maxEffectiveValue = 1.0F;
        var13.latchOn = false;
        var13.firendlyFire = true;
        var13.penetrating = false;
        var13.acidDamagePercent = 0.0F;
        this.getHandler().addBeam(var13);
    }

    public void shootMarkerBeam(ControllerStateUnit var1, float var2, MetaObject var3, boolean var4, boolean var5) {
        this.shootBeam(var1, 3, var2, var3, var4, var5);
    }

    public void shootTransporterMarkerBeam(ControllerStateUnit var1, float var2, MetaObject var3, boolean var4, boolean var5) {
        this.shootBeam(var1, 7, var2, var3, var4, var5);
    }

    public void shootPowerSupplyBeam(ControllerStateUnit var1, float var2, MetaObject var3, boolean var4, boolean var5) {
        this.shootBeam(var1, 2, var2, var3, var4, var5);
    }

    protected Matrix3f getYawRotation() {
        Vector3f var1 = this.getOwnerState().getForward(new Vector3f());
        Vector3f var2 = this.getOwnerState().getUp(new Vector3f());
        if (this.needsYaw()) {
            Transform var3;
            (var3 = new Transform()).setIdentity();
            GlUtil.setRightVector(this.getCharacterController().upAxisDirection[0], var3);
            GlUtil.setUpVector(this.getCharacterController().upAxisDirection[1], var3);
            GlUtil.setForwardVector(this.getCharacterController().upAxisDirection[2], var3);
            var3.inverse();
            var3.transform(var1);
            var3.transform(var2);
            if (var1.epsilonEquals(this.up, 0.01F)) {
                this.yaw = FastMath.atan2Fast(-var2.x, -var2.z);
            } else if (var1.epsilonEquals(this.down, 0.01F)) {
                this.yaw = FastMath.atan2Fast(var2.x, var2.z);
            } else {
                this.yaw = FastMath.atan2Fast(var1.x, var1.z);
            }
        }

        Matrix3f var4;
        (var4 = new Matrix3f()).setIdentity();
        var4.rotY(this.yaw);
        return var4;
    }

    private boolean needsYaw() {
        return !KeyboardMappings.FREE_CAM.isDownOrSticky(this.getState());
    }

    public void handleMovingInput(Timer var1, ControllerStateUnit var2) {
        if (!this.getOwnerState().isSitting()) {
            if (this.isClientOwnObject() && !this.checkClintSpawnSanity(((GameClientState)this.getState()).getCurrentSectorEntities())) {
                ((GameClientState)this.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PLAYERCHARACTER_3, new Object[]{this.waitingForToSpawn.size()}), "W", 0.0F);
                this.waitingForToSpawn.clear();
            } else {
                Vector3f var3 = var2.playerState.getUp(new Vector3f());
                Vector3f var4 = var2.playerState.getForward(new Vector3f());
                Vector3f var5 = var2.playerState.getRight(new Vector3f());
                Vector3f var6;
                Vector3f var7;
                Vector3f var8;
                if (this.getGravity().isGravityOn()) {
                    var6 = new Vector3f();
                    GlUtil.project(this.getCharacterController().upAxisDirection[1], var5, var6);
                    var6.normalize();
                    (var7 = new Vector3f(this.getCharacterController().upAxisDirection[1])).negate();
                    var8 = new Vector3f();
                    GlUtil.project(this.getCharacterController().upAxisDirection[1], var4, var8);
                    var8.normalize();
                    if (var4.epsilonEquals(this.getCharacterController().upAxisDirection[1], 0.01F)) {
                        var8.cross(var6, var4);
                        var8.normalize();
                    } else if (var4.epsilonEquals(var7, 0.01F)) {
                        var8.cross(var4, var6);
                        var8.normalize();
                    }

                    var5.set(var6);
                    var3.set(this.getCharacterController().upAxisDirection[1]);
                    var4.set(var8);
                }

                (var6 = new Vector3f(var3)).scale(-1.0F);
                var7 = new Vector3f(var5);
                var5.scale(-1.0F);
                (var8 = new Vector3f(var4)).scale(-1.0F);
                Vector3f var9;
                (var9 = new Vector3f()).set(0.0F, 0.0F, 0.0F);
                if (this.isClientOwnObject() && !this.getGravity().isGravityOn() && var2.playerState.isKeyDownOrSticky(KeyboardMappings.GRAPPLING_HOOK)) {
                    this.grappleToNearest();
                }

                if (var2.playerState.isKeyDownOrSticky(KeyboardMappings.JUMP)) {
                    if (this.getGravity().isGravityOn()) {
                        this.getCharacterController().jump();
                    }
                } else {
                    this.getCharacterController().breakJump(var1);
                }

                if (this.getGravity().isGravityOn()) {
                    if (var2.playerState.isKeyDownOrSticky(KeyboardMappings.FORWARD)) {
                        var9.add(var4);
                    }

                    if (var2.playerState.isKeyDownOrSticky(KeyboardMappings.BACKWARDS)) {
                        var9.add(var8);
                    }

                    if (var2.playerState.isKeyDownOrSticky(KeyboardMappings.STRAFE_LEFT)) {
                        var9.add(var7);
                    }

                    if (var2.playerState.isKeyDownOrSticky(KeyboardMappings.STRAFE_RIGHT)) {
                        var9.add(var5);
                    }

                    this.getOwnerState().handleJoystickDir(var9, var4, var5, var3);
                } else {
                    if (var2.playerState.isKeyDownOrSticky(KeyboardMappings.FORWARD)) {
                        var9.add(var4);
                    }

                    if (var2.playerState.isKeyDownOrSticky(KeyboardMappings.BACKWARDS)) {
                        var9.add(var8);
                    }

                    if (var2.playerState.isKeyDownOrSticky(KeyboardMappings.STRAFE_LEFT)) {
                        var9.add(var7);
                    }

                    if (var2.playerState.isKeyDownOrSticky(KeyboardMappings.STRAFE_RIGHT)) {
                        var9.add(var5);
                    }

                    if (var2.playerState.isKeyDownOrSticky(KeyboardMappings.UP)) {
                        var9.add(var3);
                    }

                    if (var2.playerState.isKeyDownOrSticky(KeyboardMappings.DOWN)) {
                        var9.add(var6);
                    }

                    this.getOwnerState().handleJoystickDir(var9, var4, var5, var3);
                }

                if (this.getCharacterController().getGravity() > 0.0F) {
                    GlUtil.getForwardVector(var4, this.getWorldTransform());
                    GlUtil.getRightVector(var5, this.getWorldTransform());
                    var3 = GlUtil.getUpVector(var3, this.getWorldTransform());
                    var9 = GlUtil.projectOntoPlane(var9, var9, var3);
                }

                if (var2.playerState.isKeyDownOrSticky(KeyboardMappings.WALK)) {
                    var9.scale(0.5F);
                }

                var9.scale(1.0F / var9.length());
                var9.scale(var1.getDelta() * this.getSpeed());
                if (var9.length() > 0.0F) {
                    this.characterController.setWalkDirectionStacked(var9);
                }

            }
        }
    }

    private void grappleToNearest() {
        if (System.currentTimeMillis() - this.lastAlignRequest > 500L) {
            System.err.println("[CLIENT] CHARACTER ALIGN PRESSED ON " + this.getState() + "; " + ((PlayerState)this.getAttachedPlayers().get(0)).getForward(new Vector3f()));
            SegmentPiece var1;
            if ((var1 = this.getNearestPiece(true)) != null) {
                SegmentController var3 = var1.getSegment().getSegmentController();
                this.scheduleGravity(new Vector3f(0.0F, 0.0F, 0.0F), var3);
                ((GameClientState)this.getState()).getController().popupInfoTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PLAYERCHARACTER_4, new Object[]{var3.toNiceString()}), 0.0F);
            } else if (this.getGravity().source != null && this.getGravity().getAcceleration().lengthSquared() == 0.0F) {
                SimpleTransformableSendableObject var2 = this.getGravity().source;
                if ((Float)EngineSettings.G_MUST_CONFIRM_DETACHEMENT_AT_SPEED.getCurrentState() >= 0.0F && (double)var2.getSpeedPercentServerLimitCurrent() >= (double)(Float)EngineSettings.G_MUST_CONFIRM_DETACHEMENT_AT_SPEED.getCurrentState() * 0.01D) {
                    (new PlayerGameOkCancelInput("CONFIRM_Exit", (GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PLAYERCHARACTER_5, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PLAYERCHARACTER_6, new Object[]{StringTools.formatPointZero(var2.getSpeedCurrent())})) {
                        public boolean isOccluded() {
                            return false;
                        }

                        public void onDeactivate() {
                        }

                        public void pressedOK() {
                            if (PlayerCharacter.this.getGravity().source != null && PlayerCharacter.this.getGravity().getAcceleration().lengthSquared() == 0.0F) {
                                PlayerCharacter.this.scheduleGravity(new Vector3f(0.0F, 0.0F, 0.0F), (SimpleTransformableSendableObject)null);
                                this.getState().getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PLAYERCHARACTER_7, 0.0F);
                                this.deactivate();
                            }

                        }
                    }).activate();
                } else {
                    this.scheduleGravity(new Vector3f(0.0F, 0.0F, 0.0F), (SimpleTransformableSendableObject)null);
                    ((GameClientState)this.getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PLAYERCHARACTER_8, 0.0F);
                }
            }

            this.lastAlignRequest = System.currentTimeMillis();
        }

    }

    public void destroyPersistent() {
        assert this.isOnServer();

        assert "ENTITY_PLAYERCHARACTER_".equals(this.getType().dbPrefix);

        String var1 = this.getType().dbPrefix + this.getOwnerState().getName();
        (new FileExt(GameServerState.ENTITY_DATABASE_PATH + var1 + ".ent")).delete();
    }

    public float getCharacterHeightOffset() {
        return this.characterHeightOffset;
    }

    public float getCharacterHeight() {
        return this.characterHeight;
    }

    public float getCharacterWidth() {
        return this.characterWidth;
    }

    public Vector3i getBlockDim() {
        return this.blockDim;
    }

    public Transform getHeadWorldTransform() {
        Transform var1 = new Transform(super.getWorldTransform());
        Vector3f var2;
        (var2 = GlUtil.getUpVector(new Vector3f(), var1)).scale(0.685F);
        var1.origin.add(var2);
        return var1;
    }

    public ArrayList<PlayerState> getAttachedPlayers() {
        return this.attachedPlayers;
    }

    public PlayerState getOwnerState() {
        if (this.ownerState == null) {
            label141: {
                synchronized(this.getState().getLocalAndRemoteObjectContainer().getLocalObjects()){}

                Throwable var10000;
                label130: {
                    boolean var10001;
                    Iterator var2;
                    try {
                        var2 = this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();
                    } catch (Throwable var9) {
                        var10000 = var9;
                        var10001 = false;
                        break label130;
                    }

                    while(true) {
                        try {
                            if (!var2.hasNext()) {
                                break label141;
                            }

                            Sendable var3;
                            if ((var3 = (Sendable)var2.next()) instanceof PlayerState) {
                                PlayerState var11 = (PlayerState)var3;
                                if (this.clientOwnerId == var11.getClientId()) {
                                    this.ownerState = var11;
                                    return this.ownerState;
                                }
                            }
                        } catch (Throwable var8) {
                            var10000 = var8;
                            var10001 = false;
                            break;
                        }
                    }
                }

                Throwable var10 = var10000;
                try {
                    throw var10;
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        }

        if (this.ownerState == null) {
            System.err.println(this.getState() + " Exception: no owner state found for " + this);
        }

        return this.ownerState;
    }

    public Transform getShoulderWorldTransform() {
        Transform var1;
        Vector3f var2;
        if (this.isOnServer()) {
            var1 = new Transform(super.getWorldTransform());
            (var2 = GlUtil.getUpVector(new Vector3f(), var1)).scale(0.385F);
            var1.origin.add(var2);
            return var1;
        } else {
            var1 = new Transform(super.getWorldTransformOnClient());
            (var2 = GlUtil.getUpVector(new Vector3f(), var1)).scale(0.385F);
            var1.origin.add(var2);
            return var1;
        }
    }

    public NetworkPlayerHumanCharacter getNetworkObject() {
        return this.networkPlayerCharacterObject;
    }

    public void initFromNetworkObject(NetworkObject var1) {
        super.initFromNetworkObject(var1);
        this.setClientOwnerId(this.getNetworkObject().clientOwnerId.get());
        if (((NetworkGravity)this.getNetworkObject().gravity.get()).gravityReceived) {
            this.receivedGravity = new NetworkGravity((NetworkGravity)this.getNetworkObject().gravity.get());
            if (this.receivedGravity.gravityIdReceive != 0) {
                this.getGravity().grappleStart = System.currentTimeMillis();
            }

            ((NetworkGravity)this.getNetworkObject().gravity.get()).gravityReceived = false;
        }

        this.spawnOnObjectId = this.getNetworkObject().spawnOnObjectId.getInt();
        this.spawnOnObjectLocalPos = this.getNetworkObject().spawnOnObjectLocalPos.getVector();
    }

    public void initPhysics() {
        if (this.isClientOwnObject() && this.spawnOnObjectId != 0) {
            Sendable var1;
            if ((var1 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(this.spawnOnObjectId)) != null && var1 instanceof SimpleTransformableSendableObject) {
                SimpleTransformableSendableObject var3 = (SimpleTransformableSendableObject)var1;
                Vector3f var2 = new Vector3f(this.spawnOnObjectLocalPos);
                var3.getWorldTransform().transform(var2);
                this.getInitialTransform().origin.set(var2);
                System.err.println("[CLIENT][SPAWN] Spawned on object: " + var3 + "; " + var3.getWorldTransform().origin + ", local " + this.spawnOnObjectLocalPos + " -> " + var2);
            }

            this.spawnOnObjectId = 0;
        }

        super.initPhysics();
    }

    protected float getCharacterMargin() {
        return this.characterMargin;
    }

    public void updateLocal(Timer var1) {
        super.updateLocal(var1);
        if (!this.isOnServer()) {
            if (this.getClientOwnerId() == ((GameClientState)this.getState()).getId()) {
                this.getRemoteTransformable().useSmoother = false;
                this.getRemoteTransformable().setSendFromClient(true);
                return;
            }

            this.getRemoteTransformable().useSmoother = true;
            this.getRemoteTransformable().setSendFromClient(false);
        }

    }

    public boolean isClientOwnObject() {
        return !this.isOnServer() && this.getAttachedPlayers().contains(((GameClientState)this.getState()).getPlayer());
    }

    public void updateToFullNetworkObject() {
        super.updateToFullNetworkObject();
        this.getNetworkObject().clientOwnerId.set(this.clientOwnerId, true);
        ((NetworkGravity)this.getNetworkObject().gravity.get()).gravity.set(this.getGravity().getAcceleration());
        ((NetworkGravity)this.getNetworkObject().gravity.get()).gravityId = this.getGravity().source != null ? this.getGravity().source.getId() : -1;
        this.getNetworkObject().gravity.setChanged(true);
        if (this.spawnOnObjectId != 0 && this.spawnOnObjectLocalPos != null) {
            this.getNetworkObject().spawnOnObjectId.set(this.spawnOnObjectId);
            this.getNetworkObject().spawnOnObjectLocalPos.set(this.spawnOnObjectLocalPos);
        }

    }

    public float getSpeed() {
        return this.speed;
    }

    public void transformBeam(BeamState var1) {
        var1.from.set(0.0F, 0.0F, 0.0F);
        (new Transform(this.getWorldTransform())).transform(var1.from);
        Vector3f var2 = new Vector3f(var1.relativePos);
        this.getYawRotation().transform(var2);
        var1.from.add(var2);
        if (!var1.latchOn) {
            var1.to.set(this.getShoulderWorldTransform().origin);
            var1.to.add(this.shootingDirTemp);
        }

    }

    public void newNetworkObject() {
        this.networkPlayerCharacterObject = new NetworkPlayerHumanCharacter(this.getState());
    }

    public void activateGravity(final SegmentPiece var1) {
        final Vector3f var2;
        if (!this.getGravity().isGravityOn()) {
            var2 = new Vector3f(0.0F, -9.89F, 0.0F);
            switch(var1.getOrientation()) {
                case 0:
                    var2.set(0.0F, 0.0F, -9.89F);
                    break;
                case 1:
                    var2.set(0.0F, 0.0F, 9.89F);
                    break;
                case 2:
                    var2.set(0.0F, 9.89F, 0.0F);
                    break;
                case 3:
                    var2.set(0.0F, -9.89F, 0.0F);
                    break;
                case 4:
                    var2.set(9.89F, 0.0F, 0.0F);
                    break;
                case 5:
                    var2.set(-9.89F, 0.0F, 0.0F);
            }

            this.scheduleGravity(var2, var1.getSegment().getSegmentController());
            if (!this.isOnServer()) {
                ((GameClientState)this.getState()).getController().popupGameTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PLAYERCHARACTER_9, new Object[]{var1.getSegment().getSegmentController().toNiceString()}), 0.0F);
                System.err.println("[CLIENT][ACTIVATE] Enter gravity of " + var1.getSegment().getSegmentController());
            } else {
                this.forcedCheckFlag = true;
                System.err.println("[SERVER][ACTIVATE] Enter gravity of " + var1.getSegment().getSegmentController());
            }
        } else {
            var2 = new Vector3f(0.0F, -9.89F, 0.0F);
            switch(var1.getOrientation()) {
                case 0:
                    var2.set(0.0F, 0.0F, -9.89F);
                    break;
                case 1:
                    var2.set(0.0F, 0.0F, 9.89F);
                    break;
                case 2:
                    var2.set(0.0F, 9.89F, 0.0F);
                    break;
                case 3:
                    var2.set(0.0F, -9.89F, 0.0F);
                    break;
                case 4:
                    var2.set(9.89F, 0.0F, 0.0F);
                    break;
                case 5:
                    var2.set(-9.89F, 0.0F, 0.0F);
            }

            System.err.println("[PLAYEREXTERNAL][GRAVITY] " + this + " ALREADY HAS GRAV: " + this.getGravity().getAcceleration() + "; " + var2);
            if (this.getGravity().source == var1.getSegment().getSegmentController() && this.getGravity().getAcceleration().equals(var2)) {
                if (!this.isOnServer()) {
                    if ((Float)EngineSettings.G_MUST_CONFIRM_DETACHEMENT_AT_SPEED.getCurrentState() >= 0.0F && (double)this.getGravity().source.getSpeedPercentServerLimitCurrent() >= (double)(Float)EngineSettings.G_MUST_CONFIRM_DETACHEMENT_AT_SPEED.getCurrentState() * 0.01D) {
                        (new PlayerGameOkCancelInput("CONFIRM_Exit", (GameClientState)this.getState(), "Exit", "Do you really want to do that.\nThe current object was flying at " + StringTools.formatPointZero(this.getGravity().source.getSpeedCurrent()) + " speed\n\n(this message can be customized or\nturned off in the game option\n'Popup Detach Warning' in the ingame options menu)") {
                            public boolean isOccluded() {
                                return false;
                            }

                            public void pressedOK() {
                                if (PlayerCharacter.this.getGravity().source != null && PlayerCharacter.this.getGravity().getAcceleration().equals(var2)) {
                                    PlayerCharacter.this.scheduleGravity(new Vector3f(0.0F, 0.0F, 0.0F), PlayerCharacter.this.getGravity().source);
                                    this.getState().getController().popupGameTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PLAYERCHARACTER_10, new Object[]{var1.getSegment().getSegmentController().toNiceString()}), 0.0F);
                                    System.err.println("[CLIENT][ACTIVATE] Exit gravity of " + var1.getSegment().getSegmentController().getId());
                                    this.deactivate();
                                }

                            }

                            public void onDeactivate() {
                            }
                        }).activate();
                    } else {
                        this.scheduleGravity(new Vector3f(0.0F, 0.0F, 0.0F), this.getGravity().source);
                        ((GameClientState)this.getState()).getController().popupGameTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PLAYERCHARACTER_11, new Object[]{var1.getSegment().getSegmentController().toNiceString()}), 0.0F);
                        System.err.println("[CLIENT][ACTIVATE] Exit gravity of " + var1.getSegment().getSegmentController().getId());
                    }
                } else {
                    this.scheduleGravity(new Vector3f(0.0F, 0.0F, 0.0F), this.getGravity().source);
                    this.forcedCheckFlag = true;
                    System.err.println("[SERVER][ACTIVATE] Exit gravity of " + var1.getSegment().getSegmentController().getId());
                }
            } else {
                this.scheduleGravity(var2, var1.getSegment().getSegmentController());
                if (!this.isOnServer()) {
                    ((GameClientState)this.getState()).getController().popupGameTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PLAYERCHARACTER_12, new Object[]{var1.getSegment().getSegmentController().toNiceString()}), 0.0F);
                    System.err.println("[CLIENT][ACTIVATE] Change to gravity of " + var1.getSegment().getSegmentController().getId());
                } else {
                    this.forcedCheckFlag = true;
                    System.err.println("[SERVER][ACTIVATE] Change to gravity of " + var1.getSegment().getSegmentController().getId());
                }
            }
        }
    }

    public boolean checkClintSpawnSanity(Int2ObjectOpenHashMap<SimpleTransformableSendableObject<?>> var1) {
        if (((GameClientState)this.getState()).getPlayer().hasSpawnWait) {
            Iterator var3 = this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

            while(var3.hasNext()) {
                Sendable var2;
                if ((var2 = (Sendable)var3.next()) instanceof SimpleTransformableSendableObject && ((SimpleTransformableSendableObject)var2).getSectorId() == this.getSectorId() && var2 instanceof SegmentController && !((SegmentController)var2).checkClientLoadedOverlap(this)) {
                    assert this.waitingForToSpawn.size() > 0;

                    return false;
                }
            }

            ((GameClientState)this.getState()).getPlayer().hasSpawnWait = false;
        }

        return true;
    }

    public void activateMedical(SegmentPiece var1) {
        CharacterBlockActivation var2;
        (var2 = new CharacterBlockActivation()).activate = true;
        var2.charId = this.getId();
        var2.location = var1.getAbsoluteIndexWithType4();
        var2.objectId = var1.getSegment().getSegmentController().getId();
        this.getNetworkObject().blockActivationsWithReaction.add(new RemoteCharacterBlockActivation(var2, this.getNetworkObject()));
    }

    public void onPlayerDetachedFromThis(PlayerState var1, PlayerControllable var2) {
        if (var2 != null && var2 instanceof SegmentController) {
            System.err.println("[PLAYERCHARACTER] " + this + " removed warp token, because character entered structure!");
            this.setWarpToken(false);
        }

    }
}
