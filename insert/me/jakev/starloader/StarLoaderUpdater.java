package me.jakev.starloader;

import com.google.gson.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * !!! IMPORTANT !!!
 * This class is never used, it is intended to be put in vanilla starmade
 */
public class StarLoaderUpdater {
    //Project ID on GitLab
    public static final int STARLOADER_ID = 16765877;

    //Private key to a dummy account to work around a gitlab bug where you cannot read pipelines without an account.
    //The private key can be made public and distributed, as it has no relation or permissions to any project.
    public static final String PRIVATE_TOKEN = "gjdT7wVFvJ1ZaiJB1Xbb";

    public static HttpURLConnection getGitLab(String request) throws IOException {
        return getRaw("https://gitlab.com/api/v4/projects/" + STARLOADER_ID + "/" + request);
    }

    public static HttpURLConnection getRaw(String request) throws IOException {
        URL url = new URL(request);
        HttpURLConnection openConnection = (HttpURLConnection) url.openConnection();
        openConnection.setRequestMethod("GET");
        openConnection.setRequestProperty("PRIVATE-TOKEN", PRIVATE_TOKEN);

        System.out.println("RCode: " + openConnection.getResponseCode());
        System.out.println(openConnection.getResponseMessage());
        return openConnection;
    }

    public static JsonArray getJsonArray(String raw) {
        return new JsonParser().parse(raw).getAsJsonArray();
    }

    public static String getCurrentInstalledJID() {
        try {
            Scanner scanner = new Scanner(new File("starloader_job_info.txt"));
            return scanner.nextLine();
        } catch (Exception e) {
            return "No_Installed_JID";
        }
    }

    public static void downloadURL(ImmutablePair<String, String> urlPair) throws IOException {
        System.err.println("Downloading starloader.jar");
        HttpURLConnection get = getRaw(urlPair.getRight());
        FileUtils.copyInputStreamToFile(get.getInputStream(), new File("starloader.jar"));
        FileWriter writer = new FileWriter("starloader_job_info.txt");
        writer.write(urlPair.getRight() + "\n");

        writer.write(urlPair.getLeft() + "\n");
        writer.write(urlPair.getRight() + "\n");
        writer.close();
    }

    public static ImmutablePair<String, String> getLatestReleaseURL() throws IOException {
        HttpURLConnection get = getGitLab("releases");
        JsonArray array = getJsonArray(IOUtils.toString(get.getInputStream()));
        JsonElement elem = array.get(0);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String elemStr = gson.toJson(elem);
        String url = elem.getAsJsonObject().get("assets").getAsJsonObject().get("links").getAsJsonArray().get(0).getAsJsonObject().get("url").getAsString();
        return new ImmutablePair<>(elemStr, url);
    }

    public static void main(String[] args) throws IOException, KeyManagementException, NoSuchAlgorithmException {
        checkAndDownloadAgent();
    }

    public static void runWithAgent(String[] args) throws IOException {
        System.err.println("Starting self with agent...");
        ArrayList<String> starArgs = new ArrayList<String>();
        starArgs.add(getJavaInstallation());
        //Carry over VM arguments for memory and stuff
        starArgs.addAll(getVMArguments());
        //Add -javaagent to load starloader
        starArgs.add("-javaagent:starloader.jar");
        starArgs.add("-jar");
        starArgs.add("StarMade.jar");
        //Carry over program arguments
        starArgs.addAll(Arrays.asList(args));
        System.err.println("!! EXECUTING NEW JAR !!");
        ProcessBuilder startSM = new ProcessBuilder(starArgs);
        try {
            startSM.inheritIO();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("InheritIO requires java 7 to run");
        }
        Process start = startSM.start();
        //Exit after done running starmade
        System.exit(0);
    }

    public static List<String> getVMArguments() {
        return ManagementFactory.getRuntimeMXBean().getInputArguments();
    }

    public static void checkAndDownloadAgent() throws IOException, NoSuchAlgorithmException, KeyManagementException {
        //Fixes some errors with java 7 not downloading properly
        SSLContext ssl = SSLContext.getInstance("TLSv1.2");
        ssl.init(null, null, new SecureRandom());
        System.setProperty("https.protocols", "TLSv1.2");
        //
        System.err.println("Looking for GitLab jobs...");
        ImmutablePair<String, String> latestReleaseURL = getLatestReleaseURL();
        String installedJob = getCurrentInstalledJID();
        System.err.println("Latest Release: " + latestReleaseURL.getLeft());
        System.err.println("Exact URL: " + latestReleaseURL.getRight());
        System.err.println("Installed URL: " + installedJob );
        if(!installedJob.equals(latestReleaseURL.getRight())){
            System.err.println("Not equal, downloading agent");
            downloadURL(latestReleaseURL);
            System.err.println("Done.");
        }
//        GitLabJob latestJob = getLatestJob();
//        int installedJob = getCurrentInstalledJID();
//        System.err.println("Latest GitLab job: " + latestJob.toString());
//        System.err.println("Installed JID: " + installedJob);
//        if (installedJob == -1 || latestJob.id != installedJob) {
//            downloadJob(latestJob);
//        }
    }

    public static String getJavaInstallation() {
        String javaHome = System.getProperty("java.home");
        File f = new File(javaHome);
        f = new File(f, "bin");
        f = new File(f, "java.exe");
        System.err.println("Getting java path... " + f + "    exists: " + f.exists());
        if (!f.exists()) {
            f = new File(javaHome);
            f = new File(f, "bin");
            f = new File(f, "java");
            System.err.println("*nix user detected");
            System.err.println("Getting java path... " + f + "    exists: " + f.exists());
        }
        return f.getPath();
    }
}