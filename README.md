# NOTICE OF DEPRECATION
This repo is no longer in use, as StarLoader has been merged directly into StarMade.

You can contribute to StarLoader here: [todo add link](http://star-made.org/)

All files under org.schema marked as deprecated mean that they have been fully ported to StarMade-Open


# StarLoader
## What is StarLoader?
StarLoader is a mod loader for StarMade. The "StarLoader" mod contains systems and hooks for modders to use.


## Info
Most information and documentation can be found on [The Wiki](https://gitlab.com/generic-username/starloader/-/wikis/home)

## StarLoader vs Star-API
Wrapper classes where moved to the Star-API project.
ModLoader classes, and some other helpers go here
Helper Classes/methods are allowed here if they are not a re-define or refactor of an existing method